import 'package:Novedades/global_widgets/global_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Locales extends GetWidget<GlobalController> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        width: Get.width * 0.80,
        height: 500,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "Novedades Carmita",
                style: TextStyle(color: Colors.black54),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Obx(() => ListView.separated(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (_, int posicion) {
                  return ListTile(
                    title: Center(
                      child: Text(
                        controller.locales[posicion].local,
                        style: TextStyle(color: Colors.blueAccent),
                      ),
                    ),
                    onTap: () {
                      controller.guardarLocal(controller.locales[posicion].id);
                    },
                  );
                },
                separatorBuilder: (_, index) => Divider(
                      color: Colors.black12,
                    ),
                itemCount: controller.locales.length))
          ],
        ),
      ),
    );
  }
}
