import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GlobalController extends GetxController {
  RxDouble sumaCantidadPagarCarrito = 0.0.obs;
  RxList<Local> locales = <Local>[].obs;
  var nombreLocal = "".obs;
  void onInit() async {
    super.onInit();
    cargarLocales();
  }

  Future<void> cargarLocales() async {
    locales.value = await API.instance.getLocales();
  }

  guardarLocal(idLocal) async {
    final storage = GetStorage();
    storage.write("local", idLocal.toString());
    print("LocalId $idLocal");
    // cargarProductosByLocal();
    Get.back();
    getLocalById(idLocal);
  }

  getLocalById(idLocal) async {
    await API.instance.getLocalById(idLocal).then((value) {
      nombreLocal.value = value["Local"];
    });
  }
}
