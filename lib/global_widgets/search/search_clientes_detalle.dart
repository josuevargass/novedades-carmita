import 'dart:convert';

import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/utils.dart';
import 'package:Novedades/utils/uber_map_theme.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ClienteDetalleSearch extends SearchDelegate {
  final storage = GetStorage();
  RxList<Cliente> clientes = <Cliente>[].obs;

  @override
  List<Widget> buildActions(BuildContext context) {
    // LAS ACCIONES DE NUESTRO APP BAR
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
            // buildSuggestions(context);
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // ÍCONO A LA IZQUIERDA DEL APP BAR
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          print("LEADING ICON");
          close(context, null);
        });
  }

  getClientes(termino) async {
    clientes.value = await API.instance.getCLientes(termino);
  }

  @override
  Widget buildResults(BuildContext context) {
    // CREA LOS RESULTADOS QUE VAMOS A MOSTRAR
    print("CLIENTES DETALLE: $clientes");
    return FutureBuilder(
      future: getClientes(query),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Obx(() => clientes.length != 0
            ? ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  color: Colors.black,
                ),
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () {
                      print(clientes[index].latitud);
                      var posicion;
                      if (clientes[index].latitud != "" ||
                          clientes[index].longitud != "") {
                        posicion = LatLng(double.parse(clientes[index].latitud),
                            double.parse(clientes[index].longitud));
                      }
                      alertaCliente(
                          clientes[index].id,
                          clientes[index].razonSocial,
                          clientes[index].identificacion,
                          clientes[index].correo,
                          posicion,
                          clientes[index].foto);
                    },
                    title: Text(
                      clientes[index].razonSocial,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          clientes[index].identificacion,
                        ),
                        Text(
                          clientes[index].correo,
                        )
                      ],
                    ),
                  );
                },
                itemCount: clientes.length,
              )
            : Center(
                child: Text("El cliente que buscas no existe..."),
              ));
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // SON LAS SUGERENCIAS QUE APARECEN CUANDO LAS PERSOANAS ESCRIBEN
    return Container();
  }
}
