import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class HistorialSearch extends SearchDelegate {
  final storage = GetStorage();
  RxList<dynamic> listadoHistorial = <dynamic>[].obs;

  HistorialSearch([this.listadoHistorial]);
  final DateFormat formatter = DateFormat('dd-MM-yyyy');

  @override
  List<Widget> buildActions(BuildContext context) {
    // LAS ACCIONES DE NUESTRO APP BAR
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
            // buildSuggestions(context);
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // ÍCONO A LA IZQUIERDA DEL APP BAR
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          print("LEADING ICON");
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // CREA LOS RESULTADOS QUE VAMOS A MOSTRAR
    final listaSugerida = (query.isEmpty)
        ? listadoHistorial
        : listadoHistorial
            .where((element) =>
                element["Id"].toLowerCase().startsWith(query.toLowerCase()))
            .toList();

    return Obx(() => listadoHistorial.length != 0
        ? Container(
            child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  // close(context, listadoHistorial[index]["Id"]);
                },
                child: ListTile(
                  title: Text(
                    "Cliente: ${listaSugerida[index]["Cliente"]["RazonSocial"]}",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "# Orden: ${listaSugerida[index]["Id"]}",
                      ),
                      Text(
                        "Fecha: ${listaSugerida[index]["Fecha"]}",
                      )
                    ],
                  ),
                ),
              );
            },
            itemCount: listaSugerida.length,
          ))
        : Container());
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // SON LAS SUGERENCIAS QUE APARECEN CUANDO LAS PERSOANAS ESCRIBEN

    final listaSugerida = (query.isEmpty)
        ? listadoHistorial
        : listadoHistorial
            .where((element) =>
                element["Id"].toLowerCase().startsWith(query.toLowerCase()))
            .toList();

    return Obx(() => listadoHistorial.length != 0
        ? Container(
            child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  // close(context, listadoHistorial[index]["Id"]);
                },
                child: ListTile(
                  title: Text(
                    "Cliente: ${listaSugerida[index]["Cliente"]["RazonSocial"]}",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "# Orden: ${listaSugerida[index]["Id"]}",
                      ),
                      Text(
                        "Fecha: ${listaSugerida[index]["Fecha"]}",
                      )
                    ],
                  ),
                ),
              );
            },
            itemCount: listaSugerida.length,
          ))
        : Container());
  }
}
