import 'dart:developer';

import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ProductoSearch extends SearchDelegate {
  final storage = GetStorage();
  final indexCarrito;
  var idLocal = "".obs;
  RxList<Productos> productos = <Productos>[].obs;

  ProductoSearch([this.indexCarrito]);

  String get searchFieldLabel => "Buscar producto";

  @override
  List<Widget> buildActions(BuildContext context) {
    // LAS ACCIONES DE NUESTRO APP BAR
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
            // buildSuggestions(context);
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // ÍCONO A LA IZQUIERDA DEL APP BAR
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          print("LEADING ICON");
          close(context, null);
        });
  }

  getProducto(idLocal, termino) async {
    idLocal.value = storage.read("local");
    print("Id local $idLocal");
    productos.value =
        await API.instance.getProductosByLocalTermino(idLocal, termino);
  }

  @override
  Widget buildResults(BuildContext context) {
    // CREA LOS RESULTADOS QUE VAMOS A MOSTRAR
    print(query);
    getProducto(idLocal, query);
    print(productos.length);
    return Obx(() => productos.length != 0
        ? ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
            itemBuilder: (context, index) {
              return GestureDetector(
                  onTap: () {
                    var existe = false;
                    var dataCarritos = storage.read("carritos");
                    inspect(dataCarritos[indexCarrito]);

                    List dataCarritoIndex = dataCarritos[indexCarrito];

                    dataCarritoIndex.forEach((item) {
                      if (item["idProducto"] == productos[index].id) {
                        print("Ya existe producto");
                        existe = true;

                        var data = {
                          "dataProducto": item,
                          "indexCarrito": indexCarrito
                        };
                        Get.toNamed(AppRoutes.EDITARPRODUCTO, arguments: data);
                      }
                    });

                    if (existe == false) {
                      print("NO existe producto");
                      if (indexCarrito != null) {
                        var data = {
                          "idProducto": productos[index].id,
                          "indexCarrito": indexCarrito
                        };
                        Get.toNamed(AppRoutes.DETALLEPRODUCTO, arguments: data);
                        print("INDEX CARRITO $indexCarrito");
                      } else {
                        Get.toNamed(AppRoutes.DETALLEPRODUCTO,
                            arguments: productos[index].id);
                        print("INDEX CARRITO $indexCarrito");
                      }
                    }

                    // print("INDEX CARRITO $indexCarrito");
                  },
                  child: ListTile(
                    title: Text(
                      productos[index].codigoInterno,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // Text(
                        //   clientes[index].descripcion,
                        // ),
                        Text(
                          productos[index].producto,
                        )
                      ],
                    ),
                    trailing: Container(
                      height: 80,
                      width: 80,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: NetworkImage(productos[index].photoUrl))),
                    ),
                  ));
            },
            itemCount: productos.length,
          )
        : Center(
            child: Container(
                child: Text("No se encuentra el producto que buscas")),
          ));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // SON LAS SUGERENCIAS QUE APARECEN CUANDO LAS PERSOANAS ESCRIBEN
    return Container();
  }
}
