import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ClienteSearch extends SearchDelegate {
  final storage = GetStorage();
  final indexCarrito;
  RxList<Cliente> clientes = <Cliente>[].obs;

  ClienteSearch([this.indexCarrito]);

  @override
  List<Widget> buildActions(BuildContext context) {
    // LAS ACCIONES DE NUESTRO APP BAR
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = '';
            // buildSuggestions(context);
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // ÍCONO A LA IZQUIERDA DEL APP BAR
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          print("LEADING ICON");
          close(context, null);
        });
  }

  getClientes(termino) async {
    clientes.value = await API.instance.getCLientes(termino);
  }

  @override
  Widget buildResults(BuildContext context) {
    // CREA LOS RESULTADOS QUE VAMOS A MOSTRAR
    print("CLIENTES: $clientes");
    getClientes(query);
    return Obx(() => clientes.length != 0
        ? ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  close(context, clientes[index].id);
                },
                child: ListTile(
                  title: Text(
                    clientes[index].razonSocial,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        clientes[index].identificacion,
                      ),
                      Text(
                        clientes[index].correo,
                      )
                    ],
                  ),
                ),
              );
            },
            itemCount: clientes.length,
          )
        : Center(
            child: Container(
              child: Text("El cliente que buscas no existe..."),
            ),
          ));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // SON LAS SUGERENCIAS QUE APARECEN CUANDO LAS PERSOANAS ESCRIBEN
    return Container();
  }
}
