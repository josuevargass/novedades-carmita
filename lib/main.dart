import 'package:Novedades/modules/Navegacion_tabs/tabs/tabs_binding.dart';
import 'package:Novedades/modules/splash/splash_binding.dart';
import 'package:Novedades/modules/splash/splash_page.dart';
import 'package:Novedades/routes/app_pages.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'global_widgets/global_controller.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await GetStorage.init();
  runApp(NovedadesCarmita());
}

class NovedadesCarmita extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Get.put(GlobalController());
    return GetMaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('es', 'ES'), // Arabic, no country code
      ],
      theme: ThemeData(
          primaryIconTheme: IconThemeData(
        color: Colors.white,
      )),
      debugShowCheckedModeBanner: false,
      title: 'Novedades Carmita',
      home: SplashPage(),
      initialBinding: SplashBinding(),
      getPages: AppPages.pages,
    );
  }
}
