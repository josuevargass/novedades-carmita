class AppRoutes {
  static const SPLASH = "splash";
  static const HOME = "home";
  static const HISTORIAL = "historial";
  static const SLIDE = "slide";
  static const TABS = "tabs";
  static const DETALLEPRODUCTO = "detalle_producto";
  static const DETALLEPEDIDO = "detalle_pedido";
  static const DETALLEPEDIDOPAGO = "detalle_pedido_pago";
  static const LOGIN = "login";
  static const CLIENTE = "cliente";
  static const EDITARCLIENTE = "editar_cliente";
  static const EDITARPRODUCTO = "editar_producto";
  static const CARRITO = "carrito";
  static const LOCAL = "local";
  static const MAPA = "mapa";
}
