import 'package:Novedades/modules/Navegacion_tabs/carrito/carrito_binding.dart';
import 'package:Novedades/modules/Navegacion_tabs/carrito/carrito_pages.dart';
import 'package:Novedades/modules/Navegacion_tabs/cliente/cliente_binding.dart';
import 'package:Novedades/modules/Navegacion_tabs/cliente/cliente_page.dart';
import 'package:Novedades/modules/auth/login/login_binding.dart';
import 'package:Novedades/modules/auth/login/login_page.dart';
import 'package:Novedades/modules/Navegacion_tabs/historial/historial_binding.dart';
import 'package:Novedades/modules/Navegacion_tabs/historial/historial_page..dart';
import 'package:Novedades/modules/Navegacion_tabs/inicio/inicio_binding.dart';
import 'package:Novedades/modules/Navegacion_tabs/inicio/inicio_page.dart';
import 'package:Novedades/modules/Navegacion_tabs/tabs/tabs_binding.dart';
import 'package:Novedades/modules/Navegacion_tabs/tabs/tabs_page.dart';
import 'package:Novedades/modules/detalle_pedido/detalle_pedido_binding.dart';
import 'package:Novedades/modules/detalle_pedido/detalle_pedido_page.dart';
import 'package:Novedades/modules/detalle_pedido_pago/detalle_pedido_pago_binding.dart';
import 'package:Novedades/modules/detalle_pedido_pago/detalle_pedido_pago_page.dart';
import 'package:Novedades/modules/detalle_producto/detalle_producto_binding.dart';
import 'package:Novedades/modules/detalle_producto/detalle_producto_page.dart';
import 'package:Novedades/modules/editar_cliente/editar_cliente_binding.dart';
import 'package:Novedades/modules/editar_cliente/editar_cliente_page.dart';
import 'package:Novedades/modules/editar_producto/editar_producto_binding.dart';
import 'package:Novedades/modules/editar_producto/editar_producto_page.dart';
import 'package:Novedades/modules/local/local_binding.dart';
import 'package:Novedades/modules/local/local_page.dart';
import 'package:Novedades/modules/mapa/mapa_binding.dart';
import 'package:Novedades/modules/mapa/mapa_page.dart';
import 'package:Novedades/modules/splash/splash_binding.dart';
import 'package:Novedades/modules/splash/splash_page.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:get/route_manager.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
        name: AppRoutes.SPLASH,
        page: () => SplashPage(),
        binding: SplashBinding(),
        transition: Transition.zoom,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.HOME,
        page: () => InicioPage(),
        binding: InicioBinding(),
        transition: Transition.zoom,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.HISTORIAL,
        page: () => HistorialPage(),
        binding: HistorialBinding(),
        transition: Transition.zoom,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.TABS,
        page: () => TabsPage(),
        binding: TabsBinding(),
        transition: Transition.zoom,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.DETALLEPRODUCTO,
        page: () => DetalleProductoPage(),
        binding: DetalleProductoBinding(),
        // transition: Transition.zoom,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.DETALLEPEDIDO,
        page: () => DetallePedidoPage(),
        binding: DetallePedidoBinding(),
        // transition: Transition.zoom,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.DETALLEPEDIDOPAGO,
        page: () => DetallePedidoPago(),
        binding: DetallePedidoPagoBinding(),
        // transition: Transition.zoom,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.LOGIN,
        page: () => LoginPage(),
        binding: LoginBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.CLIENTE,
        page: () => ClientePage(),
        binding: ClienteBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.EDITARCLIENTE,
        page: () => EditarClientePage(),
        binding: EditarClienteBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.CARRITO,
        page: () => CarritoPage(),
        binding: CarritoBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.EDITARPRODUCTO,
        page: () => EditarProductoPage(),
        binding: EditarProductoBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.LOCAL,
        page: () => LocalPage(),
        binding: LocalBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300)),
    GetPage(
        name: AppRoutes.MAPA,
        page: () => MapaPage(),
        binding: MapaBinding(),
        transition: Transition.fadeIn,
        transitionDuration: Duration(milliseconds: 300))
  ];
}
