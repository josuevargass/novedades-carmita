import 'dart:developer';

import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/environments/environment.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'dart:async';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;

class API {
  final storage = GetStorage();
  API.internal();
  static API instance = API.internal();

  //---------------PRODUCTOS
  //LISTA DE PRODUCTOS
  Future<List<Productos>> getProductos(idLocal) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
        Uri.parse(
            "https://api.binasystem.com/tendaGO/warehouses/$idLocal/products/search"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'app_token': ' $token',
        },
      );
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return (jsonResponse as List).map((e) => Productos.fromJson(e)).toList();
    } catch (e) {
      print("Error en productos: $e");
    }
  }

  //PRODUCTO POR ID
  Future<dynamic> getProductoById(idProducto) async {
    var token = storage.read("token");
    try {
      var response = await http.get(Uri.parse("$urlbase/products/$idProducto"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error producto por id: $e");
    }
  }

  //PRODUCTO POR CODIGO BARRA

  Future<dynamic> getProductoCodigoBarra(codigoBarra) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse("$urlbase/products;code=$codigoBarra"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error producto de codigo de barra: $e");
    }
  }

  //PRODUCTO POR ID Y TIPO UNIDAD
  Future<List<dynamic>> getProductoByIdByUnidad(idProducto) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse("$urlbase/products/$idProducto/unit-types?idEstado=Active"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error producto por id unidad: $e");
    }
  }

  // FOTO DEL PRODUCTO
  Future<dynamic> getFotoProducto(idProducto) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse("$urlbase/products/$idProducto/photo"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error foto de producto: $e");
    }
  }

  // DATOS PRODUCTO POR TIPO UNIDAD
  Future<dynamic> getDatosProductoByTipoUnidad(idProducto, tipoUnidad) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse("$urlbase/products/$idProducto/unitTypes;name=$tipoUnidad"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error productos por id tipo unidad: $e");
    }
  }

  Future<List<Productos>> getProductosByIdLocal(idLocal) async {
    print(idLocal);
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse(
              "https://api.binasystem.com/tendaGO/warehouses/$idLocal/products/search"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return (jsonResponse as List).map((e) => Productos.fromJson(e)).toList();
    } catch (e) {
      print("Error en productos por id local: $e");
    }
  }

  //------------LOCALES

  Future<dynamic> getLocalById(id) async {
    var token = storage.read("token");
    try {
      var response = await http
          .get(Uri.parse("$urlbase/warehouses/$id"), headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'app_token': ' $token',
      });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error locales: $e");
    }
  }

  //LISTA DE LOCALES
  Future<List<Local>> getLocales() async {
    var token = storage.read("token");
    try {
      var response = await http
          .get(Uri.parse("$urlbase/warehouses"), headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'app_token': ' $token',
      });
      var jsonResponse = convert.jsonDecode(response.body);
      print("respuesta: $jsonResponse");
      return (jsonResponse as List).map((e) => Local.fromJson(e)).toList();
    } catch (e) {
      print("Error locales: $e");
    }
  }

  Future<List<Productos>> getProductosByLocalTermino(idLocal, termino) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse("$urlbase/warehouses/$idLocal/products;search=$termino"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return (jsonResponse as List).map((e) => Productos.fromJson(e)).toList();
    } catch (e) {
      print("Error productos por local termino: $e");
      return [];
    }
  }

  //-------------------AUTH

  //LOGIN
  Future<dynamic> postlogin(data) async {
    try {
      print(data);
      var response = await http.post(
        Uri.parse(
          "$urlbase/user/login",
        ),
        body: data,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );
      var jsonResponse = convert.jsonDecode(response.body);
      // print("Data: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error login: $e");
    }
  }

  //------------------- CLIENTES

  Future<List<Cliente>> getCLientes(termino) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse("$urlbase/clients/search/$termino"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      print("respuesta: $jsonResponse");
      return (jsonResponse as List).map((e) => Cliente.fromJson(e)).toList();
    } catch (e) {
      print("Error clientes: $e");
      return [];
    }
  }

  Future<dynamic> getCLientesById(id) async {
    var token = storage.read("token");
    try {
      var response = await http
          .get(Uri.parse("$urlbase/clients/$id"), headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'app_token': ' $token',
      });
      var jsonResponse = convert.jsonDecode(response.body);
      // print("respuesta: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error clientes por id: $e");
    }
  }

  //------------------- METODO PAGOS
  Future<List<MetodoPagos>> getMetodosPagos() async {
    var token = storage.read("token");
    try {
      var response = await http
          .get(Uri.parse("$urlbase/paymentMethods"), headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'app_token': ' $token',
      });
      var jsonResponse = convert.jsonDecode(response.body);
      print("respuesta: $jsonResponse");
      return (jsonResponse as List)
          .map((e) => MetodoPagos.fromJson(e))
          .toList();
    } catch (e) {
      print("Error metodos de pago: $e");
    }
  }

  //------------------- PAISES
  Future<List<Pais>> getPaises() async {
    var token = storage.read("token");
    try {
      var response = await http
          .get(Uri.parse("$urlbase/countries"), headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'app_token': ' $token',
      });
      var jsonResponse = convert.jsonDecode(response.body);
      return (jsonResponse as List).map((e) => Pais.fromJson(e)).toList();
    } catch (e) {
      print("Error paises: $e");
    }
  }

  //------------------- PROVINCIA
  Future<List<Provincia>> getProvinciasByIdPais(idPais) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse("$urlbase/countries/$idPais/Provincias"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      return (jsonResponse as List).map((e) => Provincia.fromJson(e)).toList();
    } catch (e) {
      print("Error provincia: $e");
    }
  }

  //------------------- CIUDAD
  Future<List<Ciudad>> getCiudadByIdProvincia(idProvincia) async {
    var token = storage.read("token");
    try {
      var response = await http.get(
          Uri.parse("$urlbase/provinces/$idProvincia/Ciudades"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      print("CIUDADES: $jsonResponse");
      return (jsonResponse as List).map((e) => Ciudad.fromJson(e)).toList();
    } catch (e) {
      print("Error ciudad: $e");
    }
  }

  //------------------- SECTORES
  Future<List<Sector>> getSectores() async {
    var token = storage.read("token");
    try {
      var response = await http.get(Uri.parse("$urlbase/catalogs/sectors"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': ' $token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      print("CIUDADES: $jsonResponse");
      return (jsonResponse as List).map((e) => Sector.fromJson(e)).toList();
    } catch (e) {
      print("Error sectores: $e");
    }
  }

  //------------------- CREAR CLIENTE
  Future<dynamic> postCrearCliente(data) async {
    var token = storage.read("token");
    try {
      var response = await http.post(
          Uri.parse("http://api.binasystem.com/tendaGO/Clients"),
          body: data,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': '$token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      print("cliente: $jsonResponse");
      return jsonResponse;
    } catch (e) {
      print("Error ciudad: $e");
      mensajeError("Ocurrió un error al crear o editar el cliente");
    }
  }

  //------------------- HISTORIAL
  Future<List<dynamic>> getHistorial(
      idLocal, idCliente, fechaInicio, fechaFin) async {
    var token = storage.read("token");
    try {
      print("DATOS: ${idLocal}, ${idCliente}, ${fechaInicio}, ${fechaFin}");
      var response = await http.get(
          Uri.parse(
              "$urlbase/warehouses/$idLocal/outputs/search?idCliente=$idCliente&startDate=$fechaInicio&endDate=$fechaFin"),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': '$token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      inspect(jsonResponse);
      return jsonResponse;
    } catch (e) {
      print("Error en historial: $e");
    }
  }

  //------------------- POST COMPRA
  Future<dynamic> postPedidoCompra(data) async {
    var token = storage.read("token");
    try {
      var response = await http.post(Uri.parse("$urlbase/output/create"),
          body: data,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'app_token': '$token',
          });
      var jsonResponse = convert.jsonDecode(response.body);
      inspect(jsonResponse);
      return jsonResponse;
    } catch (e) {
      print("Error al solicitar nota de pedido: $e");
    }
  }

  //------------------- CERRAR SESIÓN
  cerrarSesion() async {
    storage.remove("token");
    Get.offAllNamed(AppRoutes.LOGIN);
  }
}
