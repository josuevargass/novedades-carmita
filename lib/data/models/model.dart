import 'dart:convert';

Productos productosFromJson(String str) => Productos.fromJson(json.decode(str));

String productosToJson(Productos data) => json.encode(data.toJson());

class Productos {
  Productos({
    this.id,
    this.codigoInterno,
    this.producto,
    this.descripcion,
    this.stock,
    this.stockMinimo,
    this.idEstado,
    this.codigoProductoPadre,
    this.pathArchivo,
    this.marca,
    this.idMarca,
    this.division,
    this.idDivision,
    this.linea,
    this.idLinea,
    this.categoria,
    this.idCategoria,
    this.productoPadre,
    this.idEmpresa,
    this.photoUrl,
  });

  num id;
  String codigoInterno;
  String producto;
  String descripcion;
  num stock;
  num stockMinimo;
  num idEstado;
  String codigoProductoPadre;
  String pathArchivo;
  String marca;
  num idMarca;
  String division;
  num idDivision;
  String linea;
  num idLinea;
  String categoria;
  num idCategoria;
  num productoPadre;
  num idEmpresa;
  String photoUrl;

  factory Productos.fromJson(Map<String, dynamic> json) => Productos(
        id: json["Id"] == null ? null : json["Id"],
        codigoInterno:
            json["CodigoInterno"] == null ? null : json["CodigoInterno"],
        producto: json["Producto"] == null ? null : json["Producto"],
        descripcion: json["Descripcion"] == null ? null : json["Descripcion"],
        stock: json["Stock"] == null ? null : json["Stock"],
        stockMinimo: json["StockMinimo"] == null ? null : json["StockMinimo"],
        idEstado: json["IdEstado"] == null ? null : json["IdEstado"],
        codigoProductoPadre: json["CodigoProductoPadre"] == null
            ? null
            : json["CodigoProductoPadre"],
        pathArchivo: json["PathArchivo"] == null ? null : json["PathArchivo"],
        marca: json["Marca"] == null ? null : json["Marca"],
        idMarca: json["IdMarca"] == null ? null : json["IdMarca"],
        division: json["Division"] == null ? null : json["Division"],
        idDivision: json["IdDivision"] == null ? null : json["IdDivision"],
        linea: json["Linea"] == null ? null : json["Linea"],
        idLinea: json["IdLinea"] == null ? null : json["IdLinea"],
        categoria: json["Categoria"] == null ? null : json["Categoria"],
        idCategoria: json["IdCategoria"] == null ? null : json["IdCategoria"],
        productoPadre:
            json["ProductoPadre"] == null ? null : json["ProductoPadre"],
        idEmpresa: json["IdEmpresa"] == null ? null : json["IdEmpresa"],
        photoUrl: json["PhotoUrl"] == null ? null : json["PhotoUrl"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "CodigoInterno": codigoInterno == null ? null : codigoInterno,
        "Producto": producto == null ? null : producto,
        "Descripcion": descripcion == null ? null : descripcion,
        "Stock": stock == null ? null : stock,
        "StockMinimo": stockMinimo == null ? null : stockMinimo,
        "IdEstado": idEstado == null ? null : idEstado,
        "CodigoProductoPadre":
            codigoProductoPadre == null ? null : codigoProductoPadre,
        "PathArchivo": pathArchivo == null ? null : pathArchivo,
        "Marca": marca == null ? null : marca,
        "IdMarca": idMarca == null ? null : idMarca,
        "Division": division == null ? null : division,
        "IdDivision": idDivision == null ? null : idDivision,
        "Linea": linea == null ? null : linea,
        "IdLinea": idLinea == null ? null : idLinea,
        "Categoria": categoria == null ? null : categoria,
        "IdCategoria": idCategoria == null ? null : idCategoria,
        "ProductoPadre": productoPadre == null ? null : productoPadre,
        "IdEmpresa": idEmpresa == null ? null : idEmpresa,
        "PhotoUrl": photoUrl == null ? null : photoUrl,
      };
}

// To parse this JSON data, do
//
// final local = localFromJson(jsonString);

Local localFromJson(String str) => Local.fromJson(json.decode(str));

String localToJson(Local data) => json.encode(data.toJson());

class Local {
  Local({
    this.idEmpresa,
    this.direccion,
    this.ipIngreso,
    this.usuarioIngreso,
    this.fechaIngreso,
    this.ipModificacion,
    this.usuarioModificacion,
    this.fechaModificacion,
    this.id,
    this.local,
    this.tipo,
    this.idEstado,
    this.defaultRuc,
  });

  int idEmpresa;
  String direccion;
  String ipIngreso;
  String usuarioIngreso;
  DateTime fechaIngreso;
  String ipModificacion;
  String usuarioModificacion;
  DateTime fechaModificacion;
  int id;
  String local;
  String tipo;
  int idEstado;
  String defaultRuc;

  factory Local.fromJson(Map<String, dynamic> json) => Local(
        idEmpresa: json["IdEmpresa"] == null ? null : json["IdEmpresa"],
        direccion: json["Direccion"] == null ? null : json["Direccion"],
        ipIngreso: json["IpIngreso"] == null ? null : json["IpIngreso"],
        usuarioIngreso:
            json["UsuarioIngreso"] == null ? null : json["UsuarioIngreso"],
        fechaIngreso: json["FechaIngreso"] == null
            ? null
            : DateTime.parse(json["FechaIngreso"]),
        ipModificacion:
            json["IpModificacion"] == null ? null : json["IpModificacion"],
        usuarioModificacion: json["UsuarioModificacion"] == null
            ? null
            : json["UsuarioModificacion"],
        fechaModificacion: json["FechaModificacion"] == null
            ? null
            : DateTime.parse(json["FechaModificacion"]),
        id: json["Id"] == null ? null : json["Id"],
        local: json["Local"] == null ? null : json["Local"],
        tipo: json["Tipo"] == null ? null : json["Tipo"],
        idEstado: json["IdEstado"] == null ? null : json["IdEstado"],
        defaultRuc: json["DefaultRUC"] == null ? null : json["DefaultRUC"],
      );

  Map<String, dynamic> toJson() => {
        "IdEmpresa": idEmpresa == null ? null : idEmpresa,
        "Direccion": direccion == null ? null : direccion,
        "IpIngreso": ipIngreso == null ? null : ipIngreso,
        "UsuarioIngreso": usuarioIngreso == null ? null : usuarioIngreso,
        "FechaIngreso":
            fechaIngreso == null ? null : fechaIngreso.toIso8601String(),
        "IpModificacion": ipModificacion == null ? null : ipModificacion,
        "UsuarioModificacion":
            usuarioModificacion == null ? null : usuarioModificacion,
        "FechaModificacion": fechaModificacion == null
            ? null
            : fechaModificacion.toIso8601String(),
        "Id": id == null ? null : id,
        "Local": local == null ? null : local,
        "Tipo": tipo == null ? null : tipo,
        "IdEstado": idEstado == null ? null : idEstado,
        "DefaultRUC": defaultRuc == null ? null : defaultRuc,
      };
}

MetodoPagos metodoPagosFromJson(String str) =>
    MetodoPagos.fromJson(json.decode(str));

String metodoPagosToJson(MetodoPagos data) => json.encode(data.toJson());

class MetodoPagos {
  MetodoPagos({
    this.id,
    this.idEmpresa,
    this.medioPago,
    this.ipIngreso,
    this.usuarioIngreso,
    this.fechaIngreso,
    this.ipModificacion,
    this.usuarioModificacion,
    this.fechaModificacion,
    this.idEstado,
  });

  int id;
  int idEmpresa;
  String medioPago;
  String ipIngreso;
  String usuarioIngreso;
  DateTime fechaIngreso;
  String ipModificacion;
  String usuarioModificacion;
  DateTime fechaModificacion;
  int idEstado;

  factory MetodoPagos.fromJson(Map<String, dynamic> json) => MetodoPagos(
        id: json["Id"] == null ? null : json["Id"],
        idEmpresa: json["IdEmpresa"] == null ? null : json["IdEmpresa"],
        medioPago: json["MedioPago"] == null ? null : json["MedioPago"],
        ipIngreso: json["IpIngreso"] == null ? null : json["IpIngreso"],
        usuarioIngreso:
            json["UsuarioIngreso"] == null ? null : json["UsuarioIngreso"],
        fechaIngreso: json["FechaIngreso"] == null
            ? null
            : DateTime.parse(json["FechaIngreso"]),
        ipModificacion:
            json["IpModificacion"] == null ? null : json["IpModificacion"],
        usuarioModificacion: json["UsuarioModificacion"] == null
            ? null
            : json["UsuarioModificacion"],
        fechaModificacion: json["FechaModificacion"] == null
            ? null
            : DateTime.parse(json["FechaModificacion"]),
        idEstado: json["IdEstado"] == null ? null : json["IdEstado"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "IdEmpresa": idEmpresa == null ? null : idEmpresa,
        "MedioPago": medioPago == null ? null : medioPago,
        "IpIngreso": ipIngreso == null ? null : ipIngreso,
        "UsuarioIngreso": usuarioIngreso == null ? null : usuarioIngreso,
        "FechaIngreso":
            fechaIngreso == null ? null : fechaIngreso.toIso8601String(),
        "IpModificacion": ipModificacion == null ? null : ipModificacion,
        "UsuarioModificacion":
            usuarioModificacion == null ? null : usuarioModificacion,
        "FechaModificacion": fechaModificacion == null
            ? null
            : fechaModificacion.toIso8601String(),
        "IdEstado": idEstado == null ? null : idEstado,
      };
}

Cliente clienteFromJson(String str) => Cliente.fromJson(json.decode(str));

String clienteToJson(Cliente data) => json.encode(data.toJson());

class Cliente {
  Cliente(
      {this.id,
      this.idEmpresa,
      this.tipoEntidad,
      this.tipoIdentificacion,
      this.razonSocial,
      this.identificacion,
      this.idPais,
      this.pais,
      this.idProvincia,
      this.provincia,
      this.idCiudad,
      this.ciudad,
      this.direccion,
      this.idSector,
      this.sector,
      this.telefono,
      this.celular,
      this.correo,
      this.observaciones,
      this.ipIngreso,
      this.usuarioIngreso,
      this.fechaIngreso,
      this.ipModificacion,
      this.usuarioModificacion,
      this.fechaModificacion,
      this.idEstado,
      this.esCliente,
      this.esProveedor,
      this.latitud,
      this.longitud,
      this.foto});

  int id;
  int idEmpresa;
  String tipoEntidad;
  String tipoIdentificacion;
  String razonSocial;
  String identificacion;
  int idPais;
  String pais;
  int idProvincia;
  String provincia;
  int idCiudad;
  String ciudad;
  String direccion;
  int idSector;
  String sector;
  String telefono;
  String celular;
  String correo;
  String observaciones;
  String ipIngreso;
  String usuarioIngreso;
  DateTime fechaIngreso;
  String ipModificacion;
  String usuarioModificacion;
  DateTime fechaModificacion;
  int idEstado;
  bool esCliente;
  bool esProveedor;
  String latitud;
  String longitud;
  String foto;

  factory Cliente.fromJson(Map<String, dynamic> json) => Cliente(
        id: json["Id"] == null ? null : json["Id"],
        idEmpresa: json["IdEmpresa"] == null ? null : json["IdEmpresa"],
        tipoEntidad: json["TipoEntidad"] == null ? null : json["TipoEntidad"],
        tipoIdentificacion: json["TipoIdentificacion"] == null
            ? null
            : json["TipoIdentificacion"],
        razonSocial: json["RazonSocial"] == null ? null : json["RazonSocial"],
        identificacion:
            json["Identificacion"] == null ? null : json["Identificacion"],
        idPais: json["IdPais"] == null ? null : json["IdPais"],
        pais: json["Pais"] == null ? null : json["Pais"],
        idProvincia: json["IdProvincia"] == null ? null : json["IdProvincia"],
        provincia: json["Provincia"] == null ? null : json["Provincia"],
        idCiudad: json["IdCiudad"] == null ? null : json["IdCiudad"],
        ciudad: json["Ciudad"] == null ? null : json["Ciudad"],
        direccion: json["Direccion"] == null ? null : json["Direccion"],
        idSector: json["IdSector"] == null ? null : json["IdSector"],
        sector: json["Sector"] == null ? null : json["Sector"],
        telefono: json["Telefono"] == null ? null : json["Telefono"],
        celular: json["Celular"] == null ? null : json["Celular"],
        correo: json["Correo"] == null ? null : json["Correo"],
        observaciones:
            json["Observaciones"] == null ? null : json["Observaciones"],
        ipIngreso: json["IpIngreso"] == null ? null : json["IpIngreso"],
        usuarioIngreso:
            json["UsuarioIngreso"] == null ? null : json["UsuarioIngreso"],
        fechaIngreso: json["FechaIngreso"] == null
            ? null
            : DateTime.parse(json["FechaIngreso"]),
        ipModificacion:
            json["IpModificacion"] == null ? null : json["IpModificacion"],
        usuarioModificacion: json["UsuarioModificacion"] == null
            ? null
            : json["UsuarioModificacion"],
        fechaModificacion: json["FechaModificacion"] == null
            ? null
            : DateTime.parse(json["FechaModificacion"]),
        idEstado: json["IdEstado"] == null ? null : json["IdEstado"],
        esCliente: json["EsCliente"] == null ? null : json["EsCliente"],
        esProveedor: json["EsProveedor"] == null ? null : json["EsProveedor"],
        latitud: json["Latitud"] == null ? null : json["Latitud"],
        longitud: json["Longitud"] == null ? null : json["Longitud"],
        foto: json["Foto"] == null ? null : json["Foto"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "IdEmpresa": idEmpresa == null ? null : idEmpresa,
        "TipoEntidad": tipoEntidad == null ? null : tipoEntidad,
        "TipoIdentificacion":
            tipoIdentificacion == null ? null : tipoIdentificacion,
        "RazonSocial": razonSocial == null ? null : razonSocial,
        "Identificacion": identificacion == null ? null : identificacion,
        "IdPais": idPais == null ? null : idPais,
        "Pais": pais == null ? null : pais,
        "IdProvincia": idProvincia == null ? null : idProvincia,
        "Provincia": provincia == null ? null : provincia,
        "IdCiudad": idCiudad == null ? null : idCiudad,
        "Ciudad": ciudad == null ? null : ciudad,
        "Direccion": direccion == null ? null : direccion,
        "IdSector": idSector == null ? null : idSector,
        "Sector": sector == null ? null : sector,
        "Telefono": telefono == null ? null : telefono,
        "Celular": celular == null ? null : celular,
        "Correo": correo == null ? null : correo,
        "Observaciones": observaciones == null ? null : observaciones,
        "IpIngreso": ipIngreso == null ? null : ipIngreso,
        "UsuarioIngreso": usuarioIngreso == null ? null : usuarioIngreso,
        "FechaIngreso":
            fechaIngreso == null ? null : fechaIngreso.toIso8601String(),
        "IpModificacion": ipModificacion == null ? null : ipModificacion,
        "UsuarioModificacion":
            usuarioModificacion == null ? null : usuarioModificacion,
        "FechaModificacion": fechaModificacion == null
            ? null
            : fechaModificacion.toIso8601String(),
        "IdEstado": idEstado == null ? null : idEstado,
        "EsCliente": esCliente == null ? null : esCliente,
        "EsProveedor": esProveedor == null ? null : esProveedor,
        "Latitud": latitud == null ? null : latitud,
        "Longitud": longitud == null ? null : longitud,
        "Foto": foto == null ? null : foto,
      };
}

Provincia provinciaFromJson(String str) => Provincia.fromJson(json.decode(str));

String provinciaToJson(Provincia data) => json.encode(data.toJson());

class Provincia {
  Provincia({
    this.id,
    this.idPais,
    this.provincia,
    this.codigo,
    this.ipIngreso,
    this.usuarioIngreso,
    this.fechaIngreso,
    this.ipModificacion,
    this.usuarioModificacion,
    this.fechaModificacion,
    this.idEstado,
  });

  int id;
  int idPais;
  String provincia;
  String codigo;
  String ipIngreso;
  String usuarioIngreso;
  DateTime fechaIngreso;
  String ipModificacion;
  String usuarioModificacion;
  DateTime fechaModificacion;
  int idEstado;

  factory Provincia.fromJson(Map<String, dynamic> json) => Provincia(
        id: json["Id"] == null ? null : json["Id"],
        idPais: json["IdPais"] == null ? null : json["IdPais"],
        provincia: json["Provincia"] == null ? null : json["Provincia"],
        codigo: json["Codigo"] == null ? null : json["Codigo"],
        ipIngreso: json["IpIngreso"] == null ? null : json["IpIngreso"],
        usuarioIngreso:
            json["UsuarioIngreso"] == null ? null : json["UsuarioIngreso"],
        fechaIngreso: json["FechaIngreso"] == null
            ? null
            : DateTime.parse(json["FechaIngreso"]),
        ipModificacion:
            json["IpModificacion"] == null ? null : json["IpModificacion"],
        usuarioModificacion: json["UsuarioModificacion"] == null
            ? null
            : json["UsuarioModificacion"],
        fechaModificacion: json["FechaModificacion"] == null
            ? null
            : DateTime.parse(json["FechaModificacion"]),
        idEstado: json["IdEstado"] == null ? null : json["IdEstado"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "IdPais": idPais == null ? null : idPais,
        "Provincia": provincia == null ? null : provincia,
        "Codigo": codigo == null ? null : codigo,
        "IpIngreso": ipIngreso == null ? null : ipIngreso,
        "UsuarioIngreso": usuarioIngreso == null ? null : usuarioIngreso,
        "FechaIngreso":
            fechaIngreso == null ? null : fechaIngreso.toIso8601String(),
        "IpModificacion": ipModificacion == null ? null : ipModificacion,
        "UsuarioModificacion":
            usuarioModificacion == null ? null : usuarioModificacion,
        "FechaModificacion": fechaModificacion == null
            ? null
            : fechaModificacion.toIso8601String(),
        "IdEstado": idEstado == null ? null : idEstado,
      };
}

Pais paisFromJson(String str) => Pais.fromJson(json.decode(str));

String paisToJson(Pais data) => json.encode(data.toJson());

class Pais {
  Pais({
    this.id,
    this.codigo,
    this.pais,
    this.nacionalidad,
    this.codigoNacionalidad,
    this.ipIngreso,
    this.usuarioIngreso,
    this.fechaIngreso,
    this.ipModificacion,
    this.usuarioModificacion,
    this.fechaModificacion,
    this.idEstado,
  });

  int id;
  String codigo;
  String pais;
  String nacionalidad;
  String codigoNacionalidad;
  String ipIngreso;
  String usuarioIngreso;
  DateTime fechaIngreso;
  String ipModificacion;
  String usuarioModificacion;
  DateTime fechaModificacion;
  int idEstado;

  factory Pais.fromJson(Map<String, dynamic> json) => Pais(
        id: json["Id"] == null ? null : json["Id"],
        codigo: json["Codigo"] == null ? null : json["Codigo"],
        pais: json["Pais"] == null ? null : json["Pais"],
        nacionalidad:
            json["Nacionalidad"] == null ? null : json["Nacionalidad"],
        codigoNacionalidad: json["CodigoNacionalidad"] == null
            ? null
            : json["CodigoNacionalidad"],
        ipIngreso: json["IpIngreso"] == null ? null : json["IpIngreso"],
        usuarioIngreso:
            json["UsuarioIngreso"] == null ? null : json["UsuarioIngreso"],
        fechaIngreso: json["FechaIngreso"] == null
            ? null
            : DateTime.parse(json["FechaIngreso"]),
        ipModificacion:
            json["IpModificacion"] == null ? null : json["IpModificacion"],
        usuarioModificacion: json["UsuarioModificacion"] == null
            ? null
            : json["UsuarioModificacion"],
        fechaModificacion: json["FechaModificacion"] == null
            ? null
            : DateTime.parse(json["FechaModificacion"]),
        idEstado: json["IdEstado"] == null ? null : json["IdEstado"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "Codigo": codigo == null ? null : codigo,
        "Pais": pais == null ? null : pais,
        "Nacionalidad": nacionalidad == null ? null : nacionalidad,
        "CodigoNacionalidad":
            codigoNacionalidad == null ? null : codigoNacionalidad,
        "IpIngreso": ipIngreso == null ? null : ipIngreso,
        "UsuarioIngreso": usuarioIngreso == null ? null : usuarioIngreso,
        "FechaIngreso":
            fechaIngreso == null ? null : fechaIngreso.toIso8601String(),
        "IpModificacion": ipModificacion == null ? null : ipModificacion,
        "UsuarioModificacion":
            usuarioModificacion == null ? null : usuarioModificacion,
        "FechaModificacion": fechaModificacion == null
            ? null
            : fechaModificacion.toIso8601String(),
        "IdEstado": idEstado == null ? null : idEstado,
      };
}

Ciudad ciudadFromJson(String str) => Ciudad.fromJson(json.decode(str));

String ciudadToJson(Ciudad data) => json.encode(data.toJson());

class Ciudad {
  Ciudad({
    this.id,
    this.idProvincia,
    this.ciudad,
    this.codigo,
    this.ipIngreso,
    this.usuarioIngreso,
    this.fechaIngreso,
    this.ipModificacion,
    this.usuarioModificacion,
    this.fechaModificacion,
    this.idEstado,
  });

  int id;
  int idProvincia;
  String ciudad;
  String codigo;
  String ipIngreso;
  String usuarioIngreso;
  DateTime fechaIngreso;
  String ipModificacion;
  String usuarioModificacion;
  DateTime fechaModificacion;
  int idEstado;

  factory Ciudad.fromJson(Map<String, dynamic> json) => Ciudad(
        id: json["Id"] == null ? null : json["Id"],
        idProvincia: json["IdProvincia"] == null ? null : json["IdProvincia"],
        ciudad: json["Ciudad"] == null ? null : json["Ciudad"],
        codigo: json["Codigo"] == null ? null : json["Codigo"],
        ipIngreso: json["IpIngreso"] == null ? null : json["IpIngreso"],
        usuarioIngreso:
            json["UsuarioIngreso"] == null ? null : json["UsuarioIngreso"],
        fechaIngreso: json["FechaIngreso"] == null
            ? null
            : DateTime.parse(json["FechaIngreso"]),
        ipModificacion:
            json["IpModificacion"] == null ? null : json["IpModificacion"],
        usuarioModificacion: json["UsuarioModificacion"] == null
            ? null
            : json["UsuarioModificacion"],
        fechaModificacion: json["FechaModificacion"] == null
            ? null
            : DateTime.parse(json["FechaModificacion"]),
        idEstado: json["IdEstado"] == null ? null : json["IdEstado"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "IdProvincia": idProvincia == null ? null : idProvincia,
        "Ciudad": ciudad == null ? null : ciudad,
        "Codigo": codigo == null ? null : codigo,
        "IpIngreso": ipIngreso == null ? null : ipIngreso,
        "UsuarioIngreso": usuarioIngreso == null ? null : usuarioIngreso,
        "FechaIngreso":
            fechaIngreso == null ? null : fechaIngreso.toIso8601String(),
        "IpModificacion": ipModificacion == null ? null : ipModificacion,
        "UsuarioModificacion":
            usuarioModificacion == null ? null : usuarioModificacion,
        "FechaModificacion": fechaModificacion == null
            ? null
            : fechaModificacion.toIso8601String(),
        "IdEstado": idEstado == null ? null : idEstado,
      };
}

Sector sectorFromJson(String str) => Sector.fromJson(json.decode(str));

String sectorToJson(Sector data) => json.encode(data.toJson());

class Sector {
  Sector({
    this.id,
    this.sector,
    this.ipIngreso,
    this.usuarioIngreso,
    this.fechaIngreso,
    this.ipModificacion,
    this.usuarioModificacion,
    this.fechaModificacion,
    this.idEstado,
  });

  int id;
  String sector;
  String ipIngreso;
  String usuarioIngreso;
  DateTime fechaIngreso;
  String ipModificacion;
  String usuarioModificacion;
  DateTime fechaModificacion;
  int idEstado;

  factory Sector.fromJson(Map<String, dynamic> json) => Sector(
        id: json["Id"] == null ? null : json["Id"],
        sector: json["Sector"] == null ? null : json["Sector"],
        ipIngreso: json["IpIngreso"] == null ? null : json["IpIngreso"],
        usuarioIngreso:
            json["UsuarioIngreso"] == null ? null : json["UsuarioIngreso"],
        fechaIngreso: json["FechaIngreso"] == null
            ? null
            : DateTime.parse(json["FechaIngreso"]),
        ipModificacion:
            json["IpModificacion"] == null ? null : json["IpModificacion"],
        usuarioModificacion: json["UsuarioModificacion"] == null
            ? null
            : json["UsuarioModificacion"],
        fechaModificacion: json["FechaModificacion"] == null
            ? null
            : DateTime.parse(json["FechaModificacion"]),
        idEstado: json["IdEstado"] == null ? null : json["IdEstado"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "Sector": sector == null ? null : sector,
        "IpIngreso": ipIngreso == null ? null : ipIngreso,
        "UsuarioIngreso": usuarioIngreso == null ? null : usuarioIngreso,
        "FechaIngreso":
            fechaIngreso == null ? null : fechaIngreso.toIso8601String(),
        "IpModificacion": ipModificacion == null ? null : ipModificacion,
        "UsuarioModificacion":
            usuarioModificacion == null ? null : usuarioModificacion,
        "FechaModificacion": fechaModificacion == null
            ? null
            : fechaModificacion.toIso8601String(),
        "IdEstado": idEstado == null ? null : idEstado,
      };
}
