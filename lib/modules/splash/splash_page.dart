import 'package:Novedades/modules/Navegacion_tabs/historial/historial_controller.dart';
import 'package:Novedades/modules/splash/splash_controller.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

class SplashPage extends GetWidget<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [fondo(), logo()],
      ),
    );
  }

  fondo() {
    return Container(
      decoration: BoxDecoration(color: Colors.black),
    );
  }

  logo() {
    return Center(
        child: ZoomIn(
            duration: Duration(milliseconds: 2000),
            child: Image.asset(
              "assets/img/logo.png",
              height: 200,
            )));
  }
}
