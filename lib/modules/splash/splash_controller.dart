import 'dart:async';

import 'package:Novedades/modules/Navegacion_tabs/tabs/tabs_page.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SplashController extends GetxController {
  @override
  final storage = GetStorage();
  void onInit() {
    super.onInit();
    irTabs();
  }

  irTabs() {
    final token = storage.read("token");
    Timer(Duration(seconds: 3), () async {
      if (token != null) {
        Get.offAllNamed(AppRoutes.TABS);
      } else {
        Get.offAllNamed(AppRoutes.LOGIN);
      }
    });
  }
}
