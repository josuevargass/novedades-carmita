import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

import '../../utils.dart';

class EditarClienteController extends GetxController {
  File file;
  final picker = ImagePicker();
  var pickedFile;
  var imagen64 = "".obs;

  final storage = GetStorage();
  var opcionPais = 1.obs;
  var opcionProvincia = 9.obs;
  var opcionCiudad = 75.obs;
  var opcionTipoIdentificacion = "Cédula".obs;
  var opcionSector = 1.obs;
  var foto = "".obs;

  Uint8List imagen;

  RxList<Pais> paises = <Pais>[].obs;
  RxList<Provincia> provincias = <Provincia>[].obs;
  RxList<Ciudad> ciudades = <Ciudad>[].obs;
  RxList<Sector> sectores = <Sector>[].obs;
  RxList<dynamic> tiposIdentificacion = <dynamic>["Cédula", "Ruc"].obs;

  TextEditingController razonsocialController = new TextEditingController();
  TextEditingController correoController = new TextEditingController();
  TextEditingController direccionController = new TextEditingController();
  TextEditingController telefonoController = new TextEditingController();
  TextEditingController celularController = new TextEditingController();
  TextEditingController identificacionController = new TextEditingController();
  TextEditingController fechaNacimientoController = new TextEditingController();
  final DateFormat formatter = DateFormat('yyyy-MM-dd');

  var idCliente;
  DateTime diaActual = DateTime.now();
  DateTime diaSeleccionado = DateTime(2021);

  Location location = new Location();
  LocationData locationData;
  PermissionStatus _permissionGranted;

  @override
  void onInit() {
    super.onInit();
    idCliente = Get.arguments;
    print("ID CLIENTE: $idCliente");
    permisos();
    cargarClienteById();
    cargarPaises();
    cargarProvincias();
    cargarCiudades();
    cargarSectores();
  }

  permisos() async {
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      print("IF");
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    locationData = await location.getLocation();
    print(locationData);
  }

  cargarClienteById() async {
    await API.instance.getCLientesById(idCliente).then((value) {
      razonsocialController.text = value["RazonSocial"];
      correoController.text = value["Correo"];
      direccionController.text = value["Direccion"];
      telefonoController.text = value["Correo"];
      celularController.text = value["Celular"];
      identificacionController.text = value["Identificacion"];
      foto.value = value["Foto"];
    });
  }

  cargarPaises() async {
    paises.value = await API.instance.getPaises();
  }

  cargarProvincias() async {
    provincias.value =
        await API.instance.getProvinciasByIdPais(opcionPais.value);
  }

  cargarCiudades() async {
    ciudades.value =
        await API.instance.getCiudadByIdProvincia(opcionProvincia.value);
  }

  cargarSectores() async {
    sectores.value = await API.instance.getSectores();
  }

  tomarFoto() async {
    pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 10);
    print("pick: $pickedFile");
    if (pickedFile != null) {
      file = File(pickedFile.path);
      imagen = file.readAsBytesSync();
      imagen64.value = base64Encode(imagen);
      mensajeSastifactorio("Se ha cargado la imagen");
    } else {
      mensajeAlerta("No has tomado foto");
    }
  }

  tomarUbicacion() async {
    Get.toNamed(AppRoutes.MAPA);
  }

  editar() async {
    var idLocal = storage.read("local");
    var body = json.encode({
      // "Pais": "string",
      "Id": idCliente,
      "IdEmpresa": idLocal,
      "TipoEntidad": "string",
      "RazonSocial": razonsocialController.text,
      "TipoIdentificacion": opcionTipoIdentificacion.value,
      "Identificacion": identificacionController.text,
      "Telefono": telefonoController.text,
      "Direccion": direccionController.text,
      "Celular": celularController.text,
      "Correo": correoController.text,
      "IdPais": opcionPais.value,
      "IdProvincia": opcionProvincia.value,
      "IdCiudad": opcionCiudad.value,
      // "Sector": "",
      "IdSector": opcionSector.value,
      "Observaciones": "string",
      "IpIngreso": "string",
      "UsuarioIngreso": "string",
      // "FechaIngreso": diaActual.toIso8601String(),
      "IpModificacion": "string",
      "UsuarioModificacion": "string",
      "FechaModificacion": diaActual.toIso8601String(),
      "IdEstado": 1,
      "EsCliente": true,
      "EsProveedor": true,
      // "Latitud": locationData.latitude,
      // "Longitud": locationData.longitude,
      "Foto": imagen64.value != null ? imagen64.value : ""
    });
    inspect(body);
    await API.instance.postCrearCliente(body).then((value) {
      print(value);
      razonsocialController.text = "";
      identificacionController.text = "";
      telefonoController.text = "";
      direccionController.text = "";
      celularController.text = "";
      correoController.text = "";
      mensajeSastifactorio("El cliente se ha editado");
    });
  }
}
