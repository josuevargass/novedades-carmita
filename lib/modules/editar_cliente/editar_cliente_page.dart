import 'package:Novedades/modules/Navegacion_tabs/cliente/cliente_controller.dart';
import 'package:Novedades/modules/editar_cliente/editar_cliente_controller.dart';
import 'package:Novedades/utils.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

class EditarClientePage extends GetWidget<EditarClienteController> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Editar cliente",
          // textAlign: TextAlign.start,
          style: TextStyle(fontSize: Get.width * 0.06),
        ),
        backgroundColor: HexColor("#B76AC8"),
        actions: [
          IconButton(
              iconSize: Get.width * 0.07,
              icon: Icon(Icons.save),
              onPressed: () {
                if (!_formKey.currentState.validate()) {
                } else {
                  print("else");
                  controller.editar();
                }
              }),
        ],
      ),
      body: FutureBuilder(
        initialData: controller.cargarPaises(),
        builder: (_, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child:
                  CircularProgressIndicator(backgroundColor: colorSecundario),
            );
          } else {
            return GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Form(
                key: _formKey,
                autovalidate: true,
                child: ListView(
                  children: [
                    CupertinoFormSection(
                      header: Text("Formulario de cliente"),
                      children: [
                        textRazonSocial(),
                        // selectTipoIdentificacion(),
                        selectPais(),
                        selectProvincia(),
                        selectCiudad(),
                        selectSector(),
                        textIdentificacion(),
                        textDireccion(),
                        textTelefono(),
                        textCelular(),
                        textCorreo(),
                        fecha(),
                        botonTomarFoto(),
                        botonMapa()
                      ],
                    )
                  ],
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget textRazonSocial() {
    return CupertinoTextFormFieldRow(
      prefix: Text(
        'Razón social *: ',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return "Razón social es requerido";
        }
        return null;
      },
      placeholder: "CONSUMIDOR FINAL",
      controller: controller.razonsocialController,
    );
  }

  Widget textIdentificacion() {
    return CupertinoTextFormFieldRow(
      prefix: Text(
        'Identificación *: ',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return "Identificación es requerido";
        }
        return null;
      },
      keyboardType: TextInputType.number,
      placeholder: "9999999999",
      controller: controller.identificacionController,
    );
  }

  Widget textDireccion() {
    return CupertinoTextFormFieldRow(
      prefix: Text(
        'Dirección: ',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      placeholder: "Dirección",
      controller: controller.direccionController,
    );
  }

  Widget textTelefono() {
    return CupertinoTextFormFieldRow(
      prefix: Text(
        'Teléfono: ',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      keyboardType: TextInputType.number,
      placeholder: "Teléfono",
      controller: controller.telefonoController,
    );
  }

  Widget textCelular() {
    return CupertinoTextFormFieldRow(
      prefix: Text(
        'Celular *: ',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return "Celular es requerido";
        }
        return null;
      },
      keyboardType: TextInputType.number,
      placeholder: "Celular",
      controller: controller.celularController,
    );
  }

  Widget textCorreo() {
    return CupertinoTextFormFieldRow(
      prefix: Text(
        'Correo *: ',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      validator: (value) {
        if (value.isEmpty) {
          return "Correo es requerido";
        }
        return null;
      },
      placeholder: "ejemplo@gmail.com",
      controller: controller.correoController,
    );
  }

  Widget textSector() {
    return CupertinoTextField(
      placeholder: "Sector",
      controller: controller.correoController,
    );
  }

  Widget botonCrearCliente() {
    return RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
          child: Text('Crear cliente'),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 0.0,
        color: colorSecundario,
        textColor: Colors.white,
        onPressed: () => {});
  }

  pais() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "País",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        selectPais()
      ],
    );
  }

  selectPais() {
    return Obx(() => DropdownButton(
          isExpanded: true,
          hint: Text('Selecciona un país'),
          value: controller.opcionPais.value,
          items: controller.paises.map((accountType) {
            return DropdownMenuItem(
              value: accountType.id,
              child: Text(accountType.pais),
            );
          }).toList(),
          onChanged: (opt) {
            controller.opcionPais.value = opt;
            print(controller.opcionPais.value);
            controller.cargarProvincias();
          },
        ));
  }

  provincia() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Provincias",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 10,
        ),
        selectProvincia()
      ],
    );
  }

  selectProvincia() {
    return Obx(() => DropdownButton(
          isExpanded: true,
          hint: Text('Selecciona una provincia'),
          value: controller.opcionProvincia.value,
          items: controller.provincias.map((accountType) {
            return DropdownMenuItem(
              value: accountType.id,
              child: Text(accountType.provincia),
            );
          }).toList(),
          onChanged: (opt) {
            controller.opcionProvincia.value = opt;
            print(controller.opcionProvincia.value);
            controller.cargarCiudades();
          },
        ));
  }

  ciudad() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Ciudades",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 10,
        ),
        selectCiudad()
      ],
    );
  }

  selectCiudad() {
    return Obx(() => DropdownButton(
          isExpanded: true,
          hint: Text('Selecciona una ciudad'),
          value: controller.opcionCiudad.value,
          items: controller.ciudades.map((accountType) {
            return DropdownMenuItem(
              value: accountType.id,
              child: Text(accountType.ciudad),
            );
          }).toList(),
          onChanged: (opt) {
            controller.opcionCiudad.value = opt;
            print(opt);
          },
        ));
  }

  sector() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Sector",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 10,
        ),
        selectSector()
      ],
    );
  }

  selectSector() {
    return Obx(() => DropdownButton(
          isExpanded: true,
          hint: Text('Selecciona un sector'),
          value: controller.opcionSector.value,
          items: controller.sectores.map((accountType) {
            return DropdownMenuItem(
              value: accountType.id,
              child: Text(accountType.sector),
            );
          }).toList(),
          onChanged: (opt) {
            controller.opcionSector.value = opt;
            print(opt);
          },
        ));
  }

  tipoIdentificacion() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Tipo de identificación",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 10,
        ),
        selectTipoIdentificacion()
      ],
    );
  }

  selectTipoIdentificacion() {
    return Obx(() => DropdownButton(
          isExpanded: true,
          hint: Text('Selecciona un tipo de identificación'),
          value: controller.opcionTipoIdentificacion.value,
          items: controller.tiposIdentificacion.map((accountType) {
            return DropdownMenuItem(
              value: accountType["key"],
              child: Text(accountType["valor"]),
            );
          }).toList(),
          onChanged: (opt) {
            controller.opcionTipoIdentificacion.value = opt;
            print(opt);
          },
        ));
  }

  fecha() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: CupertinoTextFormFieldRow(
        readOnly: true,
        prefix: Text(
          "Fecha de nacimiento: ",
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        placeholder: "27/02/1998",
        controller: controller.fechaNacimientoController,
        onTap: () {
          FocusScope.of(Get.context).requestFocus(new FocusNode());
          seleccionarFecha();
        },
      ),
    );
  }

  seleccionarFecha() async {
    DateTime picked = await showDatePicker(
        context: Get.context,
        locale: const Locale('es', 'ES'),
        initialDate: new DateTime(1960),
        firstDate: new DateTime(1960),
        lastDate: new DateTime(2025));

    if (picked != null) {
      controller.diaSeleccionado = picked;
      final String formatted = controller.formatter.format(picked);
      controller.fechaNacimientoController.text = formatted;
    }
  }

  botonTomarFoto() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: OutlinedButton(
          child: Icon(
            FontAwesomeIcons.camera,
            color: colorSecundario,
          ),
          onPressed: () {
            controller.tomarFoto();
          }),
    );
  }

  botonMapa() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: OutlinedButton(
          child: Icon(
            FontAwesomeIcons.searchLocation,
            color: colorSecundario,
          ),
          onPressed: () {
            controller.tomarUbicacion();
          }),
    );
  }
}
