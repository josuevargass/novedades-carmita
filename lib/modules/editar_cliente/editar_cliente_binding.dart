import 'package:Novedades/modules/detalle_producto/detalle_producto_controller.dart';
// import 'package:Novedades/modules/navegacion/carrito/carrito_controller.dart';
import 'package:get/get.dart';

import 'editar_cliente_controller.dart';

class EditarClienteBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EditarClienteController());
  }
}
