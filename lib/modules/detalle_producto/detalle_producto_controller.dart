import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/global_widgets/global_controller.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class DetalleProductoController extends GetxController {
  var contador = 1.obs;
  var idProducto;
  var titulo = "".obs;
  var descripcion = "".obs;
  var stock = 0.0.obs;
  var codigoInterno = "".obs;
  var photoUrl = "".obs;
  var precio = 0.0.obs;
  var tipoUnidad = "".obs;
  List<dynamic> carrito = [];
  List<dynamic> listaCarrito = [];
  List<dynamic> listaProductosByUnidad = [].obs;
  final storage = GetStorage();
  var ordenes = 0.obs;
  var cantidadOrdenes = [].obs;
  var carritos = [].obs;
  var indexCarrito = 0.obs;
  var cantidadMinima = 0.0.obs;
  var datosProductoTipoUnidad;
  var dataProducto;
  List productosPadres = [];
  RxBool existeIndexCarrito = false.obs;
  RxDouble sumaCantidadPagarProducto = 0.0.obs;
  RxDouble sumaCantidadPagarCarrito = 0.0.obs;
  var sumaCantidadProducto = 0.obs;
  var argumentos;
  var idTipoUnidad;

  @override
  void onInit() {
    super.onInit();
    getProducto();
    traerCarritos();
  }

  Future getProducto() async {
    var existeBarcode = storage.read("codigoBarra");
    print("Barcode existe? $existeBarcode");
    print("Argumento ${Get.arguments}");
    argumentos = Get.arguments;
    if (argumentos != null) {
      print("Tiene MAS DE 1 argumento ${Get.arguments}");
      idProducto = Get.arguments["idProducto"];
      indexCarrito.value = Get.arguments["indexCarrito"];
      print("Tiene dato idproducto 2 $idProducto");
      await API.instance.getProductoById(idProducto).then((value) {
        inspect(value);
        dataProducto = value;
        final data = value["PhotoUrl"];
        descripcion.value = value["Descripcion"];
        stock.value = value["Stock"];
        codigoInterno.value = value["CodigoInterno"];
        final a = data.toString().replaceAll("/Productos", "/Productos");
        final b = a.toString().replaceAll("http", "https");
        // print(b);
        photoUrl.value = b;
        cargarDatosUnidad();
      });
    } else {
      print("ENTRO BARDCODE");
      // indexCarrito.value = storage.read("indexCarrito");
      await API.instance.getProductoCodigoBarra(existeBarcode).then((value) {
        inspect(value);
        final data = value["PhotoUrl"];
        idProducto = value["Id"];
        descripcion.value = value["Descripcion"];
        stock.value = value["Stock"];
        codigoInterno.value = value["CodigoInterno"];
        final a = data.toString().replaceAll("/Productos", "/Productos");
        final b = a.toString().replaceAll("http", "https");
        // print(b);
        photoUrl.value = b;
        cargarDatosUnidad();
      });
    }
    // else if (Get.arguments != {} && existeBarcode == null) {
    //   print("Tiene 1 argumento");
    //   idProducto = Get.arguments;
    //   print("Tiene dato idproducto 1 $idProducto");
    //   await API.instance.getProductoById(idProducto).then((value) {
    //     final data = value["PhotoUrl"];
    //     descripcion.value = value["Descripcion"];
    //     stock.value = value["Stock"];
    //     codigoInterno.value = value["CodigoInterno"];
    //     final a = data.toString().replaceAll("/Productos", "/Productos");
    //     final b = a.toString().replaceAll("http", "https");
    //     // print(b);
    //     photoUrl.value = b;
    //     cargarDatosUnidad();
    //   });
    // }

    // else {
    //   print("ENTRO BARDCODE");
    //   indexCarrito.value = storage.read("indexCarrito");
    //   await API.instance.getProductoCodigoBarra(codigoBarra).then((value) {
    //     final data = value["PhotoUrl"];
    //     idProducto = value["Id"];
    //     descripcion.value = value["Descripcion"];
    //     stock.value = value["Stock"];
    //     codigoInterno.value = value["CodigoInterno"];
    //     final a = data.toString().replaceAll("/Productos", "/Productos");
    //     final b = a.toString().replaceAll("http", "https");
    //     // print(b);
    //     photoUrl.value = b;
    //     cargarDatosUnidad();
    //     getOrdenes();
    //   });
    // }
  }

  Future cargarDatosUnidad() async {
    print("id producto $idProducto");
    await API.instance.getProductoByIdByUnidad(idProducto).then((value) {
      // GUardamos en una lista todos los tipos de unidad x producto
      print("Value ${value}");
      listaProductosByUnidad = value;
      inspect(listaProductosByUnidad);
      print(value[0]["Producto"]);
      cantidadMinima.value = value[0]["CantidadMinima"];
      titulo.value = value[0]["Producto"];
      precio.value = value[0]["Precio"];
      tipoUnidad.value = value[0]["TipoUnidad"];
      datosProductoByTipoUnidad(tipoUnidad.value);
      getOrdenes();
    });
  }

  Future datosProductoByTipoUnidad(tipoUnidad) async {
    await API.instance
        .getDatosProductoByTipoUnidad(idProducto, tipoUnidad)
        .then((value) {
      print("DATOS TIPO UNIDAD $value");
      inspect(value);
      precio.value = value["Precio"];
      cantidadMinima.value = value["CantidadMinima"];
      idTipoUnidad = value["Id"];
      sumaCantidadPagarProducto.value =
          sumaCantidadProducto.value * precio.value;
    });
  }

  suma(posicion) {
    sumaCantidadProducto.value = 0;
    print("POSICION: $posicion");
    cantidadOrdenes[posicion]["pedido"]++;
    cantidadOrdenes.refresh();
    contador.value = contador.value + 1;
    print("SUMA: ${cantidadOrdenes}");

    for (var item in cantidadOrdenes) {
      print(item["pedido"]);
      sumaCantidadProducto.value = sumaCantidadProducto.value + item["pedido"];
    }

    print(
        "Total cantidad productos ${sumaCantidadProducto.value}"); // Total cantidad productos * precio

    sumaCantidadPagarProducto.value = sumaCantidadProducto.value * precio.value;
    sumarCantidadCarritoPagar();
    var dataListaCarrito = storage.read("carritos");
    if (dataListaCarrito == null) {
      print("Lista carritos NULL");
      listaCarrito = [];
    } else {
      listaCarrito = storage.read("carritos");
      // print(listaCarrito);
    }

    // Obtenemos el total a pagar por este producto
    // =
    // TotalPagarProducto

    // Falta TotalPagarCarrito
    // var index = indexCarrito;
    // for (var item in listaCarrito[index]) {
    //   //Carrito 1
    //   // Me imprime los productos que contiene mi carrito
    //   item[""] //debo obtener la cantidad de ordenes que se solicito del producto y hacer un ccontador de la cantiadad a llevar total de ese producto para * por su precio

    // }
  }

  restar(posicion) {
    if (cantidadOrdenes[posicion]["pedido"] == 0) {
      cantidadOrdenes[posicion]["pedido"] = 0;
      cantidadOrdenes.refresh();
    } else {
      sumaCantidadProducto.value = 0;
      print("POSICION: $posicion");
      cantidadOrdenes[posicion]["pedido"]--;
      cantidadOrdenes.refresh();
      contador.value = contador.value - 1;
      print("Resta: ${cantidadOrdenes}");

      for (var item in cantidadOrdenes) {
        print(item["pedido"]);
        sumaCantidadProducto.value =
            sumaCantidadProducto.value + item["pedido"];
      }

      print(
          "Cantidad productos${sumaCantidadProducto.value}"); // Total cantidad productos * precio

      sumaCantidadPagarProducto.value =
          sumaCantidadProducto.value * precio.value;
      sumarCantidadCarritoPagar();

      var dataListaCarrito = storage.read("carritos");
      if (dataListaCarrito == null) {
        print("Lista carritos NULL");
        listaCarrito = [];
      } else {
        listaCarrito = storage.read("carritos");
        print(listaCarrito);
      }
    }
  }

  sumarCantidadCarritoPagar() {
    sumaCantidadPagarCarrito.value = sumaCantidadPagarProducto.value;
    print("Index carrito actual ${indexCarrito.value}");
    if (listaCarrito.length != 0) {
      print(listaCarrito[indexCarrito.value]);

      if (listaCarrito[indexCarrito.value].length != 0) {
        print("TIENE DATOS EL CARRITO");

        for (var item in listaCarrito[indexCarrito.value]) {
          var precio = item["precio"];
          print("Precio $precio");
          var ordenes = item["ordenes"];
          print("Ordenes $ordenes");
          for (var item in ordenes) {
            print("Pedidos cant ${item["pedido"]}");
            sumaCantidadPagarCarrito.value =
                sumaCantidadPagarCarrito.value + (item["pedido"] * precio);
          }
        }
        print("Suma Cant. Carrito ${sumaCantidadPagarCarrito.value}");
      } else {
        print("NO TIENE DATOS EL CARRITO");
      }
    } else {
      print("No TIENE DATOS EL CARRITO");
    }
  }

  getOrdenes() {
    sumaCantidadProducto.value = 0;
    print("index carrito ${indexCarrito.value}");

    if (indexCarrito.value == null) {
      indexCarrito.value = 0;
    }

    cantidadOrdenes.value = [];
    var data = storage.read("ordenesCarrito");
    print("Todas las ordenes ${data}");
    print("Ordenes ${data[indexCarrito.value]}");
    ordenes.value = int.parse(data[indexCarrito.value]["ordenes"]);
    print("Cantidad ordenes ${ordenes.value}");
    for (var index = 0; index < ordenes.value; index++) {
      final pedido = {"pedido": contador.value};
      cantidadOrdenes.add(pedido);
    }

    print("Ordenes $cantidadOrdenes");

    for (var item in cantidadOrdenes) {
      print("ORDENES ${item["pedido"]}");
      sumaCantidadProducto.value = sumaCantidadProducto.value + item["pedido"];
    }

    sumaCantidadProducto.value = ordenes.value;
    sumaCantidadPagarProducto.value = sumaCantidadProducto.value * precio.value;

    sumarCantidadCarritoPagar();
  }

  // AGREGAR PRODUCTO AL CARRITO
  agregarProducto(index) {
    print("Posicion del carrito $index");

    if (index != null) {
      var cantidadProductos =
          double.parse(sumaCantidadProducto.value.toString());
      var codigoPadreProducto = dataProducto['ProductoPadre'];

      final producto = {
        "photoUrl": photoUrl.value,
        "titulo": titulo.value,
        "descripcion": descripcion.value,
        "codigoInterno": codigoInterno.value,
        "precio": precio.value,
        "cantidadTotal": sumaCantidadProducto.value,
        "ordenes": cantidadOrdenes,
        "idProducto": idProducto,
        "tipoUnidad": tipoUnidad.value,
        "IdTipoUnidad": idTipoUnidad,
        "codigoPadre": dataProducto["ProductoPadre"],
        "cantidadMinima": cantidadMinima.value
      };

      if (codigoPadreProducto != '' && codigoPadreProducto != null) {

        print("PRODUCTO en carrito ${index} + $producto");

        inspect(listaCarrito);
        listaCarrito[index].add(producto);
        storage.write("carritos", listaCarrito);

        // Get.offAllNamed(AppRoutes.TABS);
        // exitoMensajeSnackBar("Exito", "Se agrego el producto");

      } else {
        if (cantidadProductos >= cantidadMinima.value) {
          print("PRODUCTO en carrito ${index} + $producto");

          inspect(listaCarrito);
          listaCarrito[index].add(producto);
          storage.write("carritos", listaCarrito);

          Get.offAllNamed(AppRoutes.TABS);
          exitoMensajeSnackBar("Exito", "Se agrego el producto");
        } else {
          mensajeAlerta("No cumple la cantidad minima");
        }
      }
    } else {
      mensajeError("Ocurrio un error al agregar el producto");
    }
  }

  // nuevoCarrito() {
  //   final producto = {
  //     "photoUrl": photoUrl.value,
  //     "titulo": titulo.value,
  //     "descripcion": descripcion.value,
  //     "codigoInterno": codigoInterno.value,
  //     "precio": precio.value,
  //     "cantidadTotal": sumaCantidadProducto.value,
  //     "ordenes": cantidadOrdenes
  //   };

  //   inspect(producto);
  //   carrito.add(producto);
  //   inspect(carrito);
  //   listaCarrito.add(carrito.toList());
  //   inspect(listaCarrito);
  //   storage.write("carritos", listaCarrito);
  //   inspect(listaCarrito);

  //   var index = listaCarrito.length - 1;
  //   print("Nuevo carrito y index $index");
  //   posicionCarrito(index);
  //   mensajeSnackBar("Exito", "Se agrego el producto");
  //   Get.offAllNamed(AppRoutes.TABS);
  // }

  // TRAER TODOS LOS CARRITOS
  traerCarritos() {
    var dataListaCarrito = storage.read("carritos");
    if (dataListaCarrito == null) {
      print("Lista carritos NULL");
      listaCarrito = [];
    } else {
      listaCarrito = storage.read("carritos");
      if (listaCarrito.length == 1) {
        print("Solo tiene 1 carrito");
        // indexCarrito.value = 0;
      } else {
        print("Datos del carrito");
        // print("Index carrito ${indexCarrito.value}");
        inspect(listaCarrito);
      }
    }
  }

  // posicionCarrito(posicion) {
  //   print(posicion);
  //   storage.write("indexCarrito", posicion);
  //   indexCarrito.value = posicion;
  //   existeIndexCarrito.value = true;
  //   sumarCantidadCarritoPagar();
  //   print(" Index posicon carrito ${indexCarrito.value}");
  //   // Get.back();
  // }

  // getPosicionCarrito() {
  //   int index = storage.read("indexCarrito");
  //   print(index);
  //   if (index == null || index == -1) {
  //     print("Entro nulo");
  //     existeIndexCarrito.value = false;
  //     indexCarrito.value = index;
  //   } else {
  //     existeIndexCarrito.value = true;
  //     indexCarrito.value = index;
  //     print(" INDEX es ${indexCarrito.value}");
  //   }
  // }
}
