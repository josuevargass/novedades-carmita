import 'dart:convert';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  TextEditingController usuarioController = new TextEditingController();
  TextEditingController claveController = new TextEditingController();
  final storage = GetStorage();

  @override
  void onInit() {
    super.onInit();
  }

  loginEnviar() async {
    print(usuarioController.text);
    final data = json.encode(
        {"User": usuarioController.text, "Password": claveController.text});
    print(data);
    await API.instance.postlogin(data).then((value) {
      print(value);
      storage.write("token", value["Token"]);
      storage.write("usuario", value["InicioSesion"]);
      if (value["Token"] != null) {
        Get.offAllNamed(AppRoutes.LOCAL);
      } else {
        print("Acceso denegado");
      }
      if (value["ErrorCode"] == "400") {
        print("entro al 400");
        mensajeAlerta(value["UserMessage"]);
      }
    });
  }
}
