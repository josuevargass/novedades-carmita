import 'package:Novedades/global_widgets/fondo_login.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../../utils.dart';
import 'login_controller.dart';

class LoginPage extends GetWidget<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[FondoLogin(), _loginForm()],
          ),
        ),
      ),
    );
  }

  Widget _loginForm() {
    // final bloc = Provider.of(context);
    return Padding(
      padding: const EdgeInsets.only(top: 198.0),
      child: SingleChildScrollView(
        child: Container(
          width: Get.width * 1,
          height: Get.height * 1,
          padding: EdgeInsets.symmetric(vertical: 50.0),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(55.0)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey,
                    blurRadius: 1.0,
                    offset: Offset(0.0, 1.0),
                    spreadRadius: 1.0)
              ]),
          child: Column(
            children: [
              Text(
                "INICIAR SESIÓN",
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: Get.height * 0.02),
              ),
              SizedBox(
                height: 30.0,
              ),
              _email(),
              _password(),
              _crearBoton(),
              derechos()
            ],
          ),
        ),
      ),
    );
  }
}

Widget _email() {
  final controller = Get.put(LoginController());

  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.alternate_email, color: colorPrincipal),
          labelText: 'Correo electrónico'),
      controller: controller.usuarioController,
    ),
  );
}

Widget _password() {
  final controller = Get.put(LoginController());

  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20.0),
    child: TextField(
      obscureText: true,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          icon: Icon(Icons.lock_outline, color: colorPrincipal),
          labelText: 'Clave'),
      controller: controller.claveController,
    ),
  );
}

Widget _crearBoton() {
  final controller = Get.put(LoginController());
  return Padding(
    padding: const EdgeInsets.symmetric(vertical: 38.0),
    child: RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 120.0, vertical: 15.0),
          child: Text('Ingresar'),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 0.0,
        color: colorPrincipal,
        textColor: Colors.white,
        onPressed: () {
          controller.loginEnviar();
        }),
  );
}

Widget derechos() {
  return Center(
      child: Text(
    "POWER BINASYSTEM",
    style: TextStyle(color: Colors.grey),
  ));
}
