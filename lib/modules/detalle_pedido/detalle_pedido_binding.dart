import 'package:flutter/material.dart';
import 'package:get/instance_manager.dart';
import 'package:get/state_manager.dart';
import 'detalle_pedido_controller.dart';

class DetallePedidoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetallePedidoController());
  }
}
