import 'package:Novedades/global_widgets/search/search_clientes.dart';
import 'package:Novedades/modules/detalle_pedido/detalle_pedido_controller.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../utils.dart';

class DetallePedidoPage extends GetWidget<DetallePedidoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("#B76AC8"),
        title: Text("Cliente"),
        actions: [
          IconButton(
              icon: Icon(Icons.edit),
              // iconSize: Get.width * 0.07,
              onPressed: () {
                if (controller.idCliente.value != 0) {
                  Get.toNamed(AppRoutes.EDITARCLIENTE,
                      arguments: controller.idCliente.value);
                } else {
                  mensajeAlerta("Debes seleccionar un cliente");
                }
              }),
          IconButton(
              icon: Icon(Icons.add),
              // iconSize: Get.width * 0.07,
              onPressed: () {
                Get.toNamed(AppRoutes.CLIENTE);
              }),
          IconButton(
              icon: Icon(Icons.search),
              // iconSize: Get.width * 0.07,
              onPressed: () async {
                final data = await showSearch(
                    context: Get.context, delegate: ClienteSearch());
                print(data);
                if (data != null) {
                  controller.idCliente.value = data;
                  controller.cargarClienteById();
                } else {
                  controller.idCliente.value = 0;
                }
              })
        ],
      ),
      body: ListView(
        children: [
          titulo(),
          datosCliente(),
          SizedBox(height: 15),
          tituloObservacion(),
          observacion(),
          SizedBox(height: 15),
          tituloFecha(),
          fecha()
        ],
      ),
      bottomNavigationBar: cargarBotones(),
    );
  }

  titulo() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: Text(
        "Informacion del cliente",
        textAlign: TextAlign.center,
        style:
            TextStyle(fontSize: Get.width * 0.05, fontWeight: FontWeight.bold),
      ),
    );
  }

  datosCliente() {
    return Obx(() => controller.idCliente.value != 0
        ? Column(
            children: [
              Text(
                controller.razonSocial.value,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: Get.width * 0.04),
              ),
              SizedBox(height: Get.height * 0.02),
              Text(
                controller.identificacion.value,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: Get.width * 0.04),
              ),
            ],
          )
        : Padding(
            padding: const EdgeInsets.only(right: 20.0),
            child: Column(
              children: [
                GestureDetector(
                    child: Text(
                      "Sin cliente",
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: Get.width * 0.04),
                    ),
                    onTap: () async {
                      final data = await showSearch(
                          context: Get.context, delegate: ClienteSearch());
                      print(data);
                      if (data != null) {
                        controller.idCliente.value = data;
                        controller.cargarClienteById();
                      } else {
                        controller.idCliente.value = 0;
                      }
                    }),
                SizedBox(height: Get.height * 0.02),
                Text(
                  "9999999999",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: Get.width * 0.04),
                ),
              ],
            )));
  }

  tituloObservacion() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Text(
        "Observaciones: ",
        style:
            TextStyle(fontSize: Get.width * 0.04, fontWeight: FontWeight.bold),
        // textAlign: TextAlign.center,
      ),
    );
  }

  observacion() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Card(
          color: HexColor("F6F2F2"),
          child: Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
              controller: controller.observacionController,
              maxLines: 5,
              decoration: InputDecoration.collapsed(
                  hintText: "Ingresa alguna observacion"),
            ),
          )),
    );
  }

  tituloFecha() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Text(
        "Fecha: ",
        style:
            TextStyle(fontSize: Get.width * 0.04, fontWeight: FontWeight.bold),
        // textAlign: TextAlign.center,
      ),
    );
  }

  fecha() {
    return Container(
        height: Get.height / 3,
        child: CupertinoDatePicker(
          initialDateTime: DateTime.now(),
          onDateTimeChanged: (DateTime newdate) {
            print(newdate);
            controller.fecha = newdate;
          },
          use24hFormat: true,
          maximumDate: new DateTime(2030, 12, 30),
          minimumYear: 2010,
          maximumYear: 2018,
          minuteInterval: 1,
          mode: CupertinoDatePickerMode.dateAndTime,
        ));
  }

  cargarBotones() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [totalPagarCarrito(), cargarDatosFlotantes()],
    );
  }

  totalPagarCarrito() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Text(
        "Total: \$" + expresionRegular(controller.valorTotalCarrito),
        style:
            TextStyle(fontWeight: FontWeight.bold, fontSize: Get.width * 0.08),
      ),
    );
  }

  cargarDatosFlotantes() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [botonCancelar(), botonEnviar()],
    );
  }

  Widget botonCancelar() {
    return RaisedButton(
      color: colorPrincipal,
      onPressed: () {
        Get.back();
      },
      child: SizedBox(
        width: Get.width * 0.25,
        child: Text(
          "CANCELAR",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget botonEnviar() {
    return OutlineButton(
      textColor: colorPrincipal,
      onPressed: () {
        if (controller.fecha == "") {
          controller.fecha = DateTime.now();
        }
        final data = {
          "valorTotalCarrito": controller.valorTotalCarrito,
          "idCliente": controller.idCliente.value,
          "ruc": controller.identificacion.value,
          "fecha": controller.fecha,
          "observacion": controller.observacionController.text,
          "carrito": controller.carrito
        };
        print(data);
        if (controller.idCliente.value != 0) {
          Get.toNamed(AppRoutes.DETALLEPEDIDOPAGO, arguments: data);
        } else {
          mensajeAlerta("Debes seleccionar un cliente");
        }
      },
      borderSide: BorderSide(color: colorPrincipal),
      child: SizedBox(
        width: Get.width * 0.3,
        child: Text(
          "Continuar",
          textAlign: TextAlign.center,
          style: TextStyle(color: colorPrincipal),
        ),
      ),
    );
  }
}
