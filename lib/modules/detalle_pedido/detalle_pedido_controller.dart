import 'dart:developer';

import 'package:Novedades/data/services/api.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetallePedidoController extends GetxController {
  var idCliente = 0.obs;
  var razonSocial = "".obs;
  var identificacion = "".obs;
  double valorTotalCarrito;
  var carrito = [];
  DateTime fecha;

  TextEditingController observacionController = new TextEditingController();

  @override
  void onInit() {
    super.onInit();
    calculo();
    print(carrito);
  }

  calculo() {
    valorTotalCarrito = 0.0;
    carrito = Get.arguments;
    inspect(carrito);
    observacionController.text = carrito[0]["observacion"];
    carrito.forEach((element) {
      valorTotalCarrito = valorTotalCarrito +
          (element["carrito"]["precio"] * element["orden"]["pedido"]);
    });
  }

  cargarClienteById() async {
    if (idCliente.value != 0) {
      await API.instance.getCLientesById(idCliente.value).then((value) {
        print(value);
        razonSocial.value = value["RazonSocial"];
        identificacion.value = value["Identificacion"];
      });
    }
  }
}
