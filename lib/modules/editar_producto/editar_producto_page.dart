import 'package:Novedades/modules/editar_producto/editar_producto_controller.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:readmore/readmore.dart';

class EditarProductoPage extends GetWidget<EditarProductoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Stack(
            children: [fondoImagen(), botonAtras(), tipoUnidad()],
          ),
          descripcion(),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      bottomNavigationBar: new BottomAppBar(
        child: botonFlotante(),
      ),
    );
  }

  descripcion() {
    return Container(
      color: Colors.white,
      height: Get.height * 1,
      child: Obx(() => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 30,
              ),
              ListTile(
                title: Text(
                  controller.titulo.value,
                  textAlign: TextAlign.start,
                  style: titulosNegrita50,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ReadMoreText(
                      controller.descripcion.value,
                      trimLines: 3,
                      colorClickableText: Colors.blueAccent,
                      trimMode: TrimMode.Line,
                      trimCollapsedText: 'ver mas',
                      trimExpandedText: 'ver menos',
                      style: TextStyle(color: Colors.black),
                      moreStyle:
                          TextStyle(fontSize: 14, color: Colors.blueAccent),
                    ),
                    Row(
                      children: [
                        Text(
                          "Stock: ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: Get.width * 0.05),
                        ),
                        Text((controller.stock.value).toString(),
                            style: TextStyle(fontSize: Get.width * 0.045)),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          "Código interno: ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: Get.width * 0.05),
                        ),
                        Text(controller.codigoInterno.value,
                            style: TextStyle(fontSize: Get.width * 0.045))
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          "Precio: ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: Get.width * 0.05),
                        ),
                        Text((controller.precio.value).toString(),
                            style: TextStyle(fontSize: Get.width * 0.045))
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          "Cantidad Minima: ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: Get.width * 0.05),
                        ),
                        Text((controller.cantidadMinima.value).toString(),
                            style: TextStyle(fontSize: Get.width * 0.045))
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: Container(
                  color: Colors.black38,
                  height: Get.height * 0.001,
                  width: Get.width * 0.80,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              botonSumarRestar()
            ],
          )),
    );
  }

  Widget _botones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [botonCancelar(), botonAgregarProducto()],
    );
  }

  Widget botonAgregarProducto() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        RaisedButton(
          color: colorPrincipal,
          onPressed: () {
            controller.editarProducto(controller.indexCarrito.value);
          },
          child: SizedBox(
            width: Get.width * 0.4,
            child: Text(
              "EDITAR",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  Widget botonCancelar() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        RaisedButton(
          color: Colors.red,
          onPressed: () {
            Get.offAllNamed(AppRoutes.TABS);
            controller.storage.remove("codigoBarra");
          },
          child: SizedBox(
            width: Get.width * 0.4,
            child: Text(
              "CANCELAR",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  fondoImagen() {
    return Obx(() => controller.photoUrl.value != ""
        ? Hero(
            tag: controller.idProducto.toString(),
            child: Padding(
              padding: const EdgeInsets.only(top: 100),
              child: Image.network(
                "${controller.photoUrl.value}",
                width: Get.width * 1,
                height: Get.height * 0.30,
              ),
            ),
          )
        : Container());
  }

  botonAtras() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: GestureDetector(
        onTap: () {
          Get.offAllNamed(AppRoutes.TABS);
        },
        child: GestureDetector(
          child: Row(
            children: [
              Icon(
                Icons.shopping_cart,
                size: 30.0,
                color: colorPrincipal,
              ),
              Obx(() => controller.existeIndexCarrito.value
                  ? Text((controller.indexCarrito.value + 1).toString(),
                      style: TextStyle(
                        color: colorPrincipal,
                      )
                      // fontSize: Get.width * 0.02),
                      )
                  : Text((controller.indexCarrito.value + 1).toString(),
                      style: TextStyle(
                        color: colorPrincipal,
                      )
                      // fontSize: Get.width * 0.02),
                      )),
            ],
          ),
          onTap: () {
            // Get.bottomSheet(verCarritos());
          },
        ),
      ),
    );
  }

  botonFlotante() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [textos(), _botones()],
    );
  }

  Widget textos() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          children: [
            Text(
              "ACTUAL:",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: Get.width * 0.04,
                  fontWeight: FontWeight.bold),
            ),
            Obx(
              () => Text(
                "\$" +
                    expresionRegular(
                        controller.sumaCantidadPagarProducto.value),
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: Get.width * 0.04,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        Column(
          children: [
            Text(
              "CARRITO:",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: Get.width * 0.04,
                  fontWeight: FontWeight.bold),
            ),
            Obx(
              () => Text(
                "\$" +
                    expresionRegular(controller.sumaCantidadPagarCarrito.value),
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: Get.width * 0.04,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        Column(
          children: [
            Text(
              "CANTIDAD:",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: Get.width * 0.04,
                  fontWeight: FontWeight.bold),
            ),
            Obx(
              () => Text(
                (controller.sumaCantidadProducto.value).toString(),
                style: TextStyle(
                    color: Colors.blue,
                    fontSize: Get.width * 0.04,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        )
      ],
    );
  }

  botonSumarRestar() {
    return ListView(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      children: [
        Center(
          child: Text("Cantidad",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: Get.width * 0.045)),
        ),
        Obx(() => ListView.separated(
              shrinkWrap: true,
              separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
              itemBuilder: (context, index) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Nº. ${index + 1}",
                        style: TextStyle(fontSize: Get.width * 0.045)),
                    RawMaterialButton(
                      onPressed: () {
                        controller.restar(index);
                      },
                      elevation: 2.0,
                      fillColor: Colors.white,
                      child: Icon(
                        FontAwesomeIcons.minus,
                        size: 20.0,
                      ),
                      padding: EdgeInsets.all(15.0),
                      shape: CircleBorder(),
                    ),
                    Obx(() => Text(
                        controller.cantidadOrdenes[index]["pedido"].toString(),
                        style: TextStyle(fontSize: Get.width * 0.045))),
                    // Container(
                    //   width: Get.width * 0.12,
                    //   child: CupertinoTextField(
                    //       onChanged: (cantidad) {
                    //         controller.cantidadOrdenes.value[index]["pedido"] =
                    //             int.parse(cantidad);
                    //       },
                    //       controller: controller.cant,
                    //       style: TextStyle(fontSize: Get.width * 0.045)),
                    // ),
                    RawMaterialButton(
                      onPressed: () {
                        controller.suma(index);
                      },
                      elevation: 2.0,
                      fillColor: Colors.white,
                      child: Icon(
                        FontAwesomeIcons.plus,
                        size: 18.0,
                      ),
                      padding: EdgeInsets.all(15.0),
                      shape: CircleBorder(),
                    )
                  ],
                );
              },
              itemCount: controller.cantidadOrdenes.length,
            ))
      ],
    );
  }

  verCarritos() {
    controller.traerCarritos();
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        width: Get.width * 0.80,
        height: 500,
        child: Column(
          children: [
            Center(
              child: Icon(
                Icons.minimize,
                color: Colors.grey,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "Lista de carritos",
                style: TextStyle(color: Colors.black54),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            controller.listaCarrito.length != 0
                ? ListView.separated(
                    shrinkWrap: true,
                    physics: ClampingScrollPhysics(),
                    itemBuilder: (_, int posicion) {
                      return GestureDetector(
                        child: Center(child: Text("Carrito ${posicion + 1} ")),
                        onTap: () {
                          // controller.posicionCarrito(posicion);
                          Get.back();
                        },
                      );
                    },
                    separatorBuilder: (_, index) => Divider(
                          color: Colors.black12,
                        ),
                    itemCount: controller.listaCarrito.length)
                : Text(
                    "No hay carritos",
                    textAlign: TextAlign.center,
                  )
          ],
        ),
      ),
    );
  }

  tipoUnidad() {
    return Align(
      alignment: Alignment.topRight,
      child: GestureDetector(
        onTap: () {
          controller.cargarDatosUnidad();
        },
        child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              children: [
                Text(
                  "Tipo Unidad",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                mostrarTiposUnidad()
              ],
            )),
      ),
    );
  }

  mostrarTiposUnidad() {
    return Obx(() => DropdownButton(
          hint: Text('Selecciona una tipo'),
          value: controller.tipoUnidad.value,
          items: controller.listaProductosByUnidad.map((accountType) {
            return DropdownMenuItem(
              value: accountType["TipoUnidad"],
              child: Text(accountType["TipoUnidad"]),
            );
          }).toList(),
          onChanged: (opt) {
            controller.tipoUnidad.value = opt;
            print(opt);
            controller.datosProductoByTipoUnidad(opt);
          },
        ));
  }
}
