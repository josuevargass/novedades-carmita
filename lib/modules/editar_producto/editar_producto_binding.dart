import 'package:get/get.dart';

import 'editar_producto_controller.dart';

class EditarProductoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EditarProductoController());
  }
}
