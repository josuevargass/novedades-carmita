import 'dart:developer';
import 'dart:ffi';

import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class EditarProductoController extends GetxController {
  var contador = 1.obs;
  var idProducto;

  var codigoBarra;
  var titulo = "".obs;
  var descripcion = "".obs;
  var stock = 0.0.obs;
  var codigoInterno = "".obs;
  var photoUrl = "".obs;
  var precio = 0.0.obs;
  var tipoUnidad = "".obs;
  List<dynamic> carrito = [];
  List<dynamic> listaCarrito = [];
  List<dynamic> listaProductosByUnidad = [].obs;
  final storage = GetStorage();
  var ordenes = 0.obs;
  var cantidadOrdenes = [].obs;
  var carritos = [].obs;
  var indexCarrito = 0.obs;
  var cantidadMinima = 0.0.obs;
  var idTipoUnidad;
  var dataProducto;


  RxBool existeIndexCarrito = false.obs;
  RxDouble sumaCantidadPagarProducto = 0.0.obs;
  RxDouble sumaCantidadPagarCarrito = 0.0.obs;

  TextEditingController cant = TextEditingController();

  var sumaCantidadProducto = 0.obs;
  var argumentos;

  @override
  void onInit() {
    super.onInit();
    getProducto();
    traerCarritos();
  }

  Future getProducto() async {
    print("Argumento ${Get.arguments}");
    argumentos = Get.arguments;
    idProducto = argumentos["dataProducto"]["idProducto"];
    indexCarrito.value = argumentos["indexCarrito"];
    await API.instance.getProductoById(idProducto).then((value) {
      final data = value["PhotoUrl"];
      dataProducto = value;

      descripcion.value = value["Descripcion"];
      stock.value = value["Stock"];
      codigoInterno.value = value["CodigoInterno"];
      final a = data.toString().replaceAll("/Productos", "/Productos");
      final b = a.toString().replaceAll("http", "https");
      // print(b);
      photoUrl.value = b;
      tipoUnidad.value = argumentos["dataProducto"]["tipoUnidad"];
      cargarDatosUnidad();
      datosProductoByTipoUnidad(tipoUnidad.value);
      
    });
  }

  Future cargarDatosUnidad() async {
    print("id producto $idProducto");
    await API.instance.getProductoByIdByUnidad(idProducto).then((value) {
      // GUardamos en una lista todos los tipos de unidad x producto
      print("Value ${value}");
      listaProductosByUnidad = value;
      inspect(listaProductosByUnidad);
      // print(value[0]["Producto"]);
      // cantidadMinima.value = value[0]["CantidadMinima"];
      titulo.value = argumentos["dataProducto"]["titulo"];
      // precio.value = value[0]["Precio"];
      tipoUnidad.value = argumentos["dataProducto"]["tipoUnidad"];
      getOrdenes();
    });
  }

  Future datosProductoByTipoUnidad(tipoUnidad) async {
    await API.instance
        .getDatosProductoByTipoUnidad(idProducto, tipoUnidad)
        .then((value) {
      inspect(value);
      precio.value = value["Precio"];
      cantidadMinima.value = value["CantidadMinima"];
      idTipoUnidad = value["Id"];
      sumaCantidadPagarProducto.value =
          sumaCantidadProducto.value * precio.value;
    });
  }

  suma(posicion) {
    sumaCantidadProducto.value = 0;
    print("POSICION: $posicion");
    cantidadOrdenes[posicion]["pedido"]++;
    cantidadOrdenes.refresh();
    contador.value = contador.value + 1;
    print("SUMA: ${cantidadOrdenes}");

    for (var item in cantidadOrdenes) {
      print(item["pedido"]);
      sumaCantidadProducto.value = sumaCantidadProducto.value + item["pedido"];
    }

    print(
        "Total cantidad productos ${sumaCantidadProducto.value}"); // Total cantidad productos * precio

    sumaCantidadPagarProducto.value = sumaCantidadProducto.value * precio.value;
    // sumarCantidadCarritoPagar();
    sumaCantidadEditadaPagarCarrito(1, precio.value);

    var dataListaCarrito = storage.read("carritos");
    if (dataListaCarrito == null) {
      print("Lista carritos NULL");
      listaCarrito = [];
    } else {
      listaCarrito = storage.read("carritos");
      // print(listaCarrito);
    }
  }

  restar(posicion) {
    if (cantidadOrdenes[posicion]["pedido"] == 0) {
      cantidadOrdenes[posicion]["pedido"] = 0;
      cantidadOrdenes.refresh();
    } else {
      sumaCantidadProducto.value = 0;
      print("POSICION: $posicion");
      cantidadOrdenes[posicion]["pedido"]--;
      cantidadOrdenes.refresh();
      contador.value = contador.value - 1;
      print("Resta: ${cantidadOrdenes}");

      for (var item in cantidadOrdenes) {
        print(item["pedido"]);
        sumaCantidadProducto.value =
            sumaCantidadProducto.value + item["pedido"];
      }

      print(
          "Cantidad productos${sumaCantidadProducto.value}"); // Total cantidad productos * precio

      sumaCantidadPagarProducto.value =
          sumaCantidadProducto.value * precio.value;
      // sumarCantidadCarritoPagar();

      restaCantidadEditadaPagarCarrito(1, precio.value);

      var dataListaCarrito = storage.read("carritos");
      if (dataListaCarrito == null) {
        print("Lista carritos NULL");
        listaCarrito = [];
      } else {
        listaCarrito = storage.read("carritos");
        print(listaCarrito);
      }
    }
  }

  sumaCantidadEditadaPagarCarrito(cantidad, precio) {
    sumaCantidadPagarCarrito.value =
        sumaCantidadPagarCarrito.value + (cantidad * precio);
  }

  restaCantidadEditadaPagarCarrito(cantidad, precio) {
    sumaCantidadPagarCarrito.value =
        sumaCantidadPagarCarrito.value - (cantidad * precio);
  }

  sumarCantidadCarritoPagar() {
    // sumaCantidadPagarCarrito.value = sumaCantidadPagarProducto.value;
    print("Index carrito actual ${indexCarrito.value}");
    if (listaCarrito.length != 0) {
      print(listaCarrito[indexCarrito.value]);

      if (listaCarrito[indexCarrito.value].length != 0) {
        print("TIENE DATOS EL CARRITO");

        for (var item in listaCarrito[indexCarrito.value]) {
          var precio = item["precio"];
          print("Precio $precio");
          var ordenes = item["ordenes"];
          print("Ordenes $ordenes");
          for (var item in ordenes) {
            print("Pedidos cant ${item["pedido"]}");
            sumaCantidadPagarCarrito.value =
                sumaCantidadPagarCarrito.value + (item["pedido"] * precio);
          }
        }
        print("Suma Cant. Carrito ${sumaCantidadPagarCarrito.value}");
      } else {
        print("NO TIENE DATOS EL CARRITO");
      }
    } else {
      print("No TIENE DATOS EL CARRITO");
    }
  }

  getOrdenes() {
    ordenes.value = 0;
    sumaCantidadProducto.value = 0;
    print("index carrito ${indexCarrito.value}");

    if (indexCarrito.value == null) {
      indexCarrito.value = 0;
    }

    cantidadOrdenes.value = [];
    List dataOrdenes = argumentos["dataProducto"]["ordenes"];
    print("Todas las ordenes ${dataOrdenes}");
    dataOrdenes.forEach((data) {
      cant.text = data["pedido"].toString();
      final pedido = {"pedido": data["pedido"]};
      ordenes.value = data["pedido"];
      cantidadOrdenes.add(pedido);
      sumaCantidadProducto.value = sumaCantidadProducto.value + data["pedido"];
      // sumaCantidadProducto.value = ordenes.value;
      sumaCantidadPagarProducto.value =
          sumaCantidadProducto.value * precio.value;
      // sumaCantidadPagarCarrito.value = sumaCantidadPagarProducto.value;

      print("ORDENES ${cantidadOrdenes.value}");
    });

    sumarCantidadCarritoPagar();
  }

  editarProducto(index) {
    print("Posicion del carrito $index");

    if (index != null) {

      var cantidadProductos =
          double.parse(sumaCantidadProducto.value.toString());
      var codigoPadreProducto = dataProducto['ProductoPadre'];
      
      final producto = {
        "photoUrl": photoUrl.value,
        "titulo": titulo.value,
        "descripcion": descripcion.value,
        "codigoInterno": codigoInterno.value,
        "precio": precio.value,
        "cantidadTotal": sumaCantidadProducto.value,
        "ordenes": cantidadOrdenes.value,
        "idProducto": idProducto,
        "tipoUnidad": tipoUnidad.value,
        "IdTipoUnidad": idTipoUnidad,
        "codigoPadre": dataProducto["ProductoPadre"],
        "cantidadMinima": cantidadMinima.value
      };

      if (codigoPadreProducto != '' && codigoPadreProducto != null) {

        print("PRODUCTO en carrito ${index} + $producto");

          inspect(listaCarrito);
          var indexProducto = listaCarrito[index]
              .indexWhere((data) => data["idProducto"] == idProducto);
          print("EDITADO index Producto ${cantidadOrdenes.value}");
          listaCarrito[index][indexProducto] = producto;
          // listaCarrito[index].add(producto);
          inspect(listaCarrito);
          storage.remove("carritos");
          storage.write("carritos", listaCarrito);
          Get.offAllNamed(AppRoutes.TABS);
          exitoMensajeSnackBar("Exito", "Se edito el producto");

      } else {
        if (cantidadProductos >= cantidadMinima.value) {

          print("PRODUCTO en carrito ${index} + $producto");

          inspect(listaCarrito);
          var indexProducto = listaCarrito[index]
              .indexWhere((data) => data["idProducto"] == idProducto);
          print("EDITADO index Producto ${cantidadOrdenes.value}");
          listaCarrito[index][indexProducto] = producto;
          // listaCarrito[index].add(producto);
          inspect(listaCarrito);
          storage.remove("carritos");
          storage.write("carritos", listaCarrito);
          Get.offAllNamed(AppRoutes.TABS);
          exitoMensajeSnackBar("Exito", "Se edito el producto");
        } else {
          mensajeAlerta("No cumple la cantidad minima");
        }
      }
    } else {
      mensajeError("Ocurrio un error al agregar el producto");
    }
  }

  traerCarritos() {
    var dataListaCarrito = storage.read("carritos");
    if (dataListaCarrito == null) {
      print("Lista carritos NULL");
      listaCarrito = [];
    } else {
      listaCarrito = storage.read("carritos");
      if (listaCarrito.length == 1) {
        print("Solo tiene 1 carrito");
        // indexCarrito.value = 0;
      } else {
        print("Datos del carrito");
        // print("Index carrito ${indexCarrito.value}");
        inspect(listaCarrito);
      }
    }
  }
}
