import 'package:Novedades/modules/detalle_pedido_pago/detalle_pedido_pago_controller.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:pdf_viewer_jk/pdf_viewer_jk.dart';

import 'package:flutter/cupertino.dart';
import 'package:cupertino_stepper/cupertino_stepper.dart';

class DetallePedidoPago extends GetWidget<DetallePedidoPagoController> {
  var data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            if (controller.existePDF == true) {
              Get.offAllNamed(AppRoutes.TABS);
            } else {
              Get.back();
            }
          },
        ),
        title: Text('Pagos'),
        backgroundColor: HexColor("#B76AC8"),
        centerTitle: true,
      ),
      body: Obx(
        () => !controller.existePDF.value
            ? Theme(
                data: ThemeData(
                    accentColor: colorPrincipal,
                    primarySwatch: Colors.orange,
                    colorScheme: ColorScheme.light(primary: colorPrincipal)),
                child: GestureDetector(
                  onTap: () {
                    // cambio.value = true;
                    data = {"pago": "", "metodoPago": "", "idMetodoPago": ""};
                  },
                  child: Column(
                    children: [
                      Expanded(
                        child: Stepper(
                          type: controller.stepperType,
                          physics: ScrollPhysics(),
                          currentStep: controller.currentStep.value,
                          onStepTapped: (step) {
                            tapped(step);
                            print("Hola $step");
                            cambioStep(step);
                          },
                          onStepCancel: cancel,
                          onStepContinue: () {
                            continued();
                            cambioStep(controller.currentStep.value);
                          },
                          controlsBuilder: (BuildContext context,
                              {VoidCallback onStepContinue,
                              VoidCallback onStepCancel}) {
                            if (controller.currentStep.value == 2) {
                              print("Hola control");
                              return Row(
                                children: <Widget>[
                                  SizedBox(height: 70.0),
                                  Container(
                                    color: colorPrincipal,
                                    child: FlatButton(
                                      child: Text(
                                        "Enviar",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      onPressed: () {
                                        print("ENviando doc");
                                        controller.enviar();
                                      },
                                    ),
                                  ),
                                  FlatButton(
                                    child: Text("Atras"),
                                    onPressed: onStepCancel,
                                  )
                                ],
                              );
                            } else if (controller.currentStep.value == 1) {
                              return Row(
                                children: <Widget>[
                                  SizedBox(height: 50.0),
                                  Container(
                                    color: colorPrincipal,
                                    child: FlatButton(
                                      child: Text(
                                        "Siguiente",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      onPressed: () {
                                        var valorPagado = expresionRegular(
                                            controller.valorPagado.value);
                                        var valorPagar = expresionRegular(
                                            controller.valorPagar);

                                        if (controller.valorFormaPago.value ==
                                            1) {
                                          if (valorPagado == valorPagar) {
                                            if (controller
                                                    .valorFormaPago.value ==
                                                1) {
                                              onStepContinue();
                                            }
                                          } else {
                                            mensajeAlerta(
                                                "EL monto a pagar debe ser igual al monto total!");
                                          }
                                        } else {
                                          //Credito
                                          onStepContinue();
                                        }
                                      },
                                    ),
                                  ),
                                  FlatButton(
                                    child: Text("Atras"),
                                    onPressed: onStepCancel,
                                  )
                                ],
                              );
                            } else {
                              return Row(
                                children: <Widget>[
                                  SizedBox(height: 50.0),
                                  Container(
                                    color: colorPrincipal,
                                    child: FlatButton(
                                      child: Text(
                                        "Siguiente",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                      onPressed: onStepContinue,
                                    ),
                                  ),
                                  FlatButton(
                                    child: Text("Atras"),
                                    onPressed: onStepCancel,
                                  )
                                ],
                              );
                            }
                          },
                          steps: <Step>[
                            Step(
                              title: new Text('Forma de pago'),
                              content: Column(
                                children: [
                                  pago(),
                                  SizedBox(height: Get.height * 0.02),
                                  pagosDias(),
                                  SizedBox(height: Get.height * 0.02),
                                  valoresPagar(),
                                  SizedBox(height: Get.height * 0.02),
                                  pendienteEntrega(),
                                  SizedBox(height: Get.height * 0.02),
                                ],
                              ),
                              isActive: controller.currentStep.value >= 0,
                              state: controller.currentStep.value >= 0
                                  ? StepState.complete
                                  : StepState.disabled,
                            ),
                            Step(
                              title: new Text('Metodos de Pagos'),
                              content: Column(
                                children: [
                                  titulosAgregarTabla(),
                                  datosAgregarTabla(),
                                  detallePagosTabla(),
                                  totalesMetodoPagos()
                                ],
                              ),
                              isActive: controller.currentStep.value >= 0,
                              state: controller.currentStep.value >= 1
                                  ? StepState.complete
                                  : StepState.disabled,
                            ),
                            Step(
                              title: new Text('Cuotas'),
                              content: Column(
                                children: <Widget>[
                                  tabla(),
                                  // Padding(
                                  //   padding:
                                  //       const EdgeInsets.symmetric(horizontal: 30),
                                  //   child: MaterialButton(
                                  //     onPressed: () {
                                  //       controller.enviar();
                                  //       // print(controller.url);
                                  //       // controller.launchURL(controller.url);
                                  //     },
                                  //     child: Text("Continuar"),
                                  //     color: colorPrincipal,
                                  //     textColor: Colors.white,
                                  //   ),
                                  // ),
                                ],
                              ),
                              isActive: controller.currentStep.value >= 0,
                              state: controller.currentStep.value >= 2
                                  ? StepState.complete
                                  : StepState.disabled,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ))
            : pdf(),
      ),
      // floatingActionButton: FloatingActionButton(
      //   child: Icon(Icons.list),
      //   onPressed: switchStepsType,
      // ),
    );
  }

  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(
  //       backgroundColor: HexColor("#B76AC8"),
  //       title: Text("Detalle de pedido"),
  //     ),
  //     body: GestureDetector(
  //       onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
  //       child: Obx(() => !controller.existePDF.value
  //           ? ListView(
  //               shrinkWrap: true,
  //               children: [
  //                 SizedBox(height: Get.height * 0.04),
  //                 pago(),
  //                 SizedBox(height: Get.height * 0.02),
  //                 pagosDias(),
  //                 SizedBox(height: Get.height * 0.02),
  //                 valoresPagar(),
  //                 SizedBox(height: Get.height * 0.02),
  //                 pendienteEntrega(),
  //                 SizedBox(height: Get.height * 0.02),
  //                 Divider(
  //                   thickness: 5,
  //                   color: Colors.grey,
  //                 ),
  //                 // tituloGenerar(),
  //                 datosAgregarTabla(),
  //                 detallePagosTabla(),
  //                 // botonGenerarPagos(),
  //                 Divider(
  //                   thickness: 5,
  //                   color: Colors.grey,
  //                 ),
  //                 tabla(),
  //                 Padding(
  //                   padding: const EdgeInsets.symmetric(horizontal: 30),
  //                   child: MaterialButton(
  //                     onPressed: () {
  //                       controller.enviar();
  //                       // print(controller.url);
  //                       // controller.launchURL(controller.url);
  //                     },
  //                     child: Text("Continuar"),
  //                     color: colorPrincipal,
  //                     textColor: Colors.white,
  //                   ),
  //                 ),
  //               ],
  //             )
  //           : pdf()),
  //     ),
  //   );

  tapped(int step) {
    controller.currentStep.value = step;
  }

  continued() {
    controller.currentStep.value < 2 ? controller.currentStep.value += 1 : null;
  }

  cancel() {
    controller.currentStep.value > 0 ? controller.currentStep.value -= 1 : null;
  }

  Step _buildStep({
    Widget title,
    StepState state = StepState.indexed,
    bool isActive = false,
  }) {
    print(state);
    return Step(
      title: title,
      subtitle: Text('Subtitle'),
      state: state,
      isActive: isActive,
      content: LimitedBox(
        maxWidth: 300,
        maxHeight: 300,
        child: Container(color: CupertinoColors.systemGrey),
      ),
    );
  }
}

pdf() {
  var controller = Get.put(DetallePedidoPagoController());
  return PDFViewer(document: controller.doc);
}

tituloGenerar() {
  return Padding(
    padding: const EdgeInsets.all(18.0),
    child: Text(
      "Generar métodos de pago",
      style: TextStyle(fontWeight: FontWeight.bold),
    ),
  );
}

botonAgregar() {
  var controller = Get.put(DetallePedidoPagoController());
  return ElevatedButton(
    style:
        ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.green)),
    onPressed: () {
      controller.generarFormasPago();
    },
    child: const Text('Agregar'),
  );
}

cambioStep(step) {
  var controller = Get.put(DetallePedidoPagoController());

  print(step);
  switch (step) {
    case 0:
      break;
    case 1:
      if (controller.detallePagos.length != 0) {
        print("Tiene elemento");
      } else {
        controller.cambioFormaPago();
        // controller.generarFormasPago();
        // controller.valorPagado.value = controller.valorPagar;
      }
      break;
    case 2:
      controller.generarPagos();
      // controller.enviar();
      break;
  }
}

titulosAgregarTabla() {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 0.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Text(
          "M. Pago",
          style: titulosNegrita40,
        ),
        Text(
          "Monto",
          style: titulosNegrita40,
        ),
        Text(
          "Acción",
          style: titulosNegrita40,
        )
      ],
    ),
  );
}

datosAgregarTabla() {
  var controller = Get.put(DetallePedidoPagoController());

  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 0.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(width: Get.width * 0.25, child: mostrarTiposMetodoPago()),
        Container(
          width: Get.width * 0.20,
          child: CupertinoTextField(
            placeholder: "Monto",
            controller: controller.pago,
          ),
        ),
        botonAgregar()
      ],
    ),
  );
}

detallePagosTabla() {
  var controller = Get.put(DetallePedidoPagoController());

  return Obx(() => SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: DataTable(
          columns: const <DataColumn>[
            DataColumn(
              label: Text(
                'Acción',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                'Método Pago',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
            DataColumn(
              label: Text(
                'Monto',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ),
          ],
          rows: controller.detallePagos
              .map(
                (elemento) => DataRow(
                  cells: <DataCell>[
                    // DataCell(
                    //     Text(elemento["fechaVencimiento"].toString())),
                    DataCell(IconButton(
                        onPressed: () {
                          controller.eliminar(elemento);
                        },
                        icon: Icon(FontAwesomeIcons.trash, color: Colors.red))),
                    DataCell(Text(elemento["metodoPago"].toString())),
                    DataCell(Text(
                        expresionRegular(double.parse(elemento["pago"]))
                            .toString())),
                  ],
                ),
              )
              .toList(),
        ),
      ));
}

totalesMetodoPagos() {
  var controller = Get.put(DetallePedidoPagoController());

  return Column(
    children: [
      Container(
        color: HexColor("#DDDDDD"),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              "Valor a Pagar",
              style: TextStyle(
                  fontSize: Get.width * 0.04, fontWeight: FontWeight.bold),
            ),
            Text(
              "${expresionRegular(controller.valorPagado.value)}",
              style: TextStyle(
                  fontSize: Get.width * 0.04, fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
      Row(
        children: [],
      )
    ],
  );
}

Widget pago() {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 12.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: Get.width * 0.30,
          child: Text(
            "Tipo de compra: ",
            style: TextStyle(
                color: Colors.black54,
                fontSize: Get.width * 0.04,
                fontWeight: FontWeight.bold),
          ),
        ),
        Container(
          width: Get.width * 0.40,
          child: mostrarFormasPagos(),
        )
      ],
    ),
  );
}

mostrarFormasPagos() {
  var controller = Get.put(DetallePedidoPagoController());

  return Obx(() => Center(
        child: DropdownButton(
          isExpanded: true,
          hint: Text('Selecciona una tipo'),
          value: controller.valorFormaPago.value,
          items: controller.formaPagos.map((accountType) {
            return DropdownMenuItem(
              value: accountType["id"],
              child: Text(accountType["descripcion"]),
            );
          }).toList(),
          onChanged: (opt) {
            controller.valorFormaPago.value = opt;
            controller.cambioFormaPago();
            // controller.datosProductoByTipoUnidad(opt);
          },
        ),
      ));
}

Widget pagosDias() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      Container(
          width: Get.width * 0.20,
          child: Text(
            "# Pagos:",
            style: TextStyle(
                color: Colors.black54,
                fontSize: Get.width * 0.04,
                fontWeight: FontWeight.bold),
          )),
      Container(
        width: Get.width * 0.20,
        child: numeroPagos(),
      ),
      Text(
        "Dias: ",
        style: TextStyle(
            color: Colors.black54,
            fontSize: Get.width * 0.04,
            fontWeight: FontWeight.bold),
      ),
      Container(
        width: Get.width * 0.20,
        child: diasPagar(),
      ),
    ],
  );
}

numeroPagos() {
  var controller = Get.put(DetallePedidoPagoController());

  return Obx(() => controller.valorFormaPago.value == 1
      ? CupertinoTextFormFieldRow(
          initialValue: "1",
          keyboardType: TextInputType.number,
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'OpenSans',
          ),
          readOnly: true,
          placeholder: "0",
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        )
      : CupertinoTextField(
          keyboardType: TextInputType.number,
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'OpenSans',
          ),
          // readOnly: true,
          placeholder: "0",
          controller: controller.numeroPagos,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
        ));
}

diasPagar() {
  var controller = Get.put(DetallePedidoPagoController());

  return Obx(() => controller.valorFormaPago.value == 1
      ? CupertinoTextFormFieldRow(
          controller: controller.numeroDias,
          keyboardType: TextInputType.number,
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'OpenSans',
          ),
          readOnly: true,
          placeholder: "0",
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
        )
      : CupertinoTextField(
          controller: controller.numeroDias,
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'OpenSans',
          ),
          placeholder: "0",
        ));
}

Widget valoresPagar() {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            width: Get.width * 0.20,
            child: Text(
              "V. Pagar",
              style: TextStyle(
                  color: Colors.black54,
                  fontSize: Get.width * 0.04,
                  fontWeight: FontWeight.bold),
            )),
        Container(width: Get.width * 0.40, child: valorPagar()),
      ],
    ),
  );
}

valorPagar() {
  var controller = Get.put(DetallePedidoPagoController());

  return Container(
    width: Get.width * 0.80,
    child: CupertinoTextField(
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      readOnly: true,
      inputFormatters: [
        FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,2}')),
      ],
      style: TextStyle(
        color: Colors.black,
        fontFamily: 'OpenSans',
      ),
      placeholder: "0",
      controller: controller.valorPagarController,
    ),
  );
}

pendienteEntrega() {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 10),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
            width: Get.width * 0.30,
            child: Text(
              "Pendiente Entrega",
              style: TextStyle(
                  color: Colors.black54,
                  fontSize: Get.width * 0.04,
                  fontWeight: FontWeight.bold),
            )),
        Container(width: Get.width * 0.40, child: tipoEntrega()),
      ],
    ),
  );
}

tipoEntrega() {
  var controller = Get.put(DetallePedidoPagoController());

  return Obx(() => Center(
        child: DropdownButton(
          isExpanded: true,
          hint: Text('Selecciona una tipo'),
          value: controller.idEntrega.value,
          items: controller.entrega.map((accountType) {
            return DropdownMenuItem(
              value: accountType["id"],
              child: Text(accountType["descripcion"]),
            );
          }).toList(),
          onChanged: (opt) {
            controller.idEntrega.value = opt;
            controller.cambioFormaEntrega(opt);
            print(opt);
            // controller.datosProductoByTipoUnidad(opt);
          },
        ),
      ));
}

var cambio = true.obs;

mostrarTextoMonto(elemento, [vacioData]) {
  var controller = Get.put(DetallePedidoPagoController());
  var index =
      controller.detallePagos.indexWhere((element) => element == elemento);

  print(elemento);
  return Obx(() => controller.detallePagos[index]
      ? GestureDetector(
          onTap: () {
            print("object");
            // cambiarTextoMonto(elemento);
            controller.detallePagos.refresh();
            print(cambio.value);
          },
          child: Text(expresionRegular(
              double.parse(controller.detallePagos[index]["pago"]))))
      : cambiarTextoMonto(elemento));
}

cambiarTextoMonto(elemento) {
  var controller = Get.put(DetallePedidoPagoController());
  print("Input");
  return CupertinoTextField(
    placeholder: "Valor",
    onChanged: (event) {
      elemento["pago"] = event;
      controller.cambioValorPagado(event, elemento);
    },
  );

  // DropdownButton(
  //       isExpanded: true,
  //       hint: Text('M. Pago'),
  //       value: controller.metodoPago,
  //       items: controller.metodosPagos.map((accountType) {
  //         return DropdownMenuItem(
  //           value: accountType,
  //           child: Text(accountType.medioPago),
  //         );
  //       }).toList(),
  //       onChanged: (opt) {
  //         controller.detallePagos[index]["metodoPago"] = opt.medioPago;
  //         controller.detallePagos[index]["idMetodoPago"] = opt.id;
  //         print(opt.id);
  //         controller.detallePagos.refresh();
  //         ; // controller.datosProductoByTipoUnidad(opt);
  //       },
  //     ));
}

mostrarTiposMetodoPago() {
  var controller = Get.put(DetallePedidoPagoController());

  return Obx(() => controller.metodoPagoDescripcion.value != ""
      ? GestureDetector(
          onTap: () {
            controller.metodoPagoDescripcion.value = "";
            seleccionarTiposMetodoPago();
          },
          child: Text(controller.metodoPagoDescripcion.value))
      : seleccionarTiposMetodoPago());
}

seleccionarTiposMetodoPago() {
  var controller = Get.put(DetallePedidoPagoController());

  return Obx(() => DropdownButton(
        isExpanded: true,
        hint: Text('M. Pago'),
        // value: controller.metodoPago,
        items: controller.metodosPagos.map((accountType) {
          return DropdownMenuItem(
            value: accountType,
            child: Text(accountType.medioPago),
          );
        }).toList(),
        onChanged: (opt) {
          controller.metodoPagoDescripcion.value = opt.medioPago;
          controller.idMetodoPago.value = opt.id;
          print(opt.id);
          controller.detallePagos.refresh();
        },
      ));
}

Widget tabla() {
  var controller = Get.put(DetallePedidoPagoController());

  return Obx(() => Center(
        child: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: DataTable(
              columns: const <DataColumn>[
                DataColumn(
                  label: Text(
                    '# Pagos',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Fecha',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                // DataColumn(
                //   label: Text(
                //     'Metodo Pago',
                //     style: TextStyle(fontStyle: FontStyle.italic),
                //   ),
                // ),
                DataColumn(
                  label: Text(
                    'Cuota',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Pagado',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
                DataColumn(
                  label: Text(
                    'Saldo',
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ),
              ],
              rows: controller.pagos
                  .map(
                    (elemento) => DataRow(
                      cells: <DataCell>[
                        // DataCell(
                        //     Text(elemento["fechaVencimiento"].toString())),
                        DataCell(Text(elemento["numeroPagos"].toString())),
                        DataCell(Text(
                            controller.formatter.format(elemento["fecha"]))),
                        // DataCell(Text(elemento["MetodoPago"])),
                        DataCell(Text(
                            ('\$' + expresionRegular(elemento["valorPagar"])))),
                        DataCell(Text(('\$' +
                            expresionRegular(elemento["valorPagado"])))),
                        // DataCell(Text(('\$${elemento["valorPagado"]}'))),
                        DataCell(
                            Text('\$' + expresionRegular(elemento["saldo"]))),
                      ],
                    ),
                  )
                  .toList(),
            )),
      ));
}
