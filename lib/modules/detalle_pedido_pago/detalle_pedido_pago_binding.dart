import 'package:Novedades/modules/detalle_producto/detalle_producto_controller.dart';
import 'package:Novedades/modules/detalle_pedido_pago/detalle_pedido_pago_controller.dart';
// import 'package:Novedades/modules/navegacion/carrito/carrito_controller.dart';
import 'package:get/get.dart';

class DetallePedidoPagoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetallePedidoPagoController());
  }
}
