import 'dart:convert';
import 'dart:developer';

import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pdf_viewer_jk/pdf_viewer_jk.dart';

class DetallePedidoPagoController extends GetxController {
  var storage = GetStorage();
  @override
  RxList<MetodoPagos> metodosPagos = <MetodoPagos>[].obs;
  RxList<dynamic> formaPagos = <dynamic>[
    {"id": 1, "descripcion": "CONTADO"},
    {"id": 2, "descripcion": "CREDITO"},
  ].obs;

  RxList<dynamic> entrega = <dynamic>[
    {"id": 1, "descripcion": "REGULAR"},
    {"id": 2, "descripcion": "INMEDIATA"},
  ].obs;

  var estadoEntrega = "EN PROCESO";
  var idEntrega = 1.obs;
  var valorFormaPago = 1.obs;
  var valorPagado = 0.0.obs;

  RxMap<String, Object> metodoPago = {
    "Id": 1,
    "IdEmpresa": 1,
    "MedioPago": "EFECTIVO",
    "IpIngreso": "SERVER",
    "UsuarioIngreso": "ADMIN",
    "FechaIngreso": "2020-01-01T00:00:00",
    "IpModificacion": null,
    "UsuarioModificacion": null,
    "FechaModificacion": null,
    "IdEstado": 1
  }.obs;
  var metodoPagoDescripcion = "".obs;
  var idMetodoPago = 1.obs;
  var valorPagar;
  var fecha;

  final headers = {
    "Content-Type": "application/json",
    "app_token": "C68677DD00184D6280B7BB4DB30A8E81"
  };

  PDFDocument doc;

  var argumentos = Get.arguments;

  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  DateTime diaActual = DateTime.now();

  DateFormat fecha2 = DateFormat("dd-MM-yyyy");

  TextEditingController numeroPagos = new TextEditingController();
  TextEditingController numeroDias = new TextEditingController();
  TextEditingController valorPagarController = new TextEditingController();
  TextEditingController valorPagadoController = new TextEditingController();

  TextEditingController pago = new TextEditingController();

  var pagos = [].obs;
  var solicitudNotaPedido = [].obs;
  var pedidoDetalle = [].obs;
  Map datosPedido;

  var detallePagos = [].obs;
  var existePDF = false.obs;

  var generarPagosBool = false;
  var cumpleMontoPagos = false;
  var currentStep = 0.obs;
  var usuario;
  StepperType stepperType = StepperType.vertical;

  void onInit() {
    DateTime now = new DateTime.now();
    DateTime date = new DateTime(now.year, now.month, now.day);
    fecha = date;
    print("FECHA $date");
    super.onInit();
    cargarMetodosPagos();
    datosPedido = Get.arguments;
    valorPagar = datosPedido["valorTotalCarrito"];
    fecha = datosPedido["fecha"];

    valorPagarController.text = expresionRegular(valorPagar).toString();
    valorPagadoController.text = expresionRegular(valorPagar).toString();
    if (valorFormaPago.value == 1) {
      numeroDias.text = "0";
    }
    print("object ${valorPagadoController.text}");

    print(valorPagarController.text);
  }

  Future<MetodoPagos> cargarMetodosPagos() async {
    metodosPagos.value = await API.instance.getMetodosPagos();
    print("METODOS PAGOS ${metodosPagos.value}");
  }

  cambioValorPagado(event, elemento) {
    var sumaPagosMetodos = 0.0;
    valorPagado.value = 0.0;
    var index = detallePagos.indexWhere((element) => element == elemento);
    var valorPagarDouble = expresionRegular(valorPagar);

    print(event);

    print(" Posicion $index");

    var data = {
      "pago": event,
      "metodoPago": metodoPagoDescripcion.value = "EFECTIVO",
      "idMetodoPago": idMetodoPago.value = 1
    };

    detallePagos[index] = data;

    inspect(detallePagos.value);

    if (event == "") {
      print("Vacio");
      valorPagado.value = 0.0;
    } else {
      detallePagos.forEach((element) {
        sumaPagosMetodos = sumaPagosMetodos + double.parse(element["pago"]);

        print("Total $sumaPagosMetodos");

        if (sumaPagosMetodos < double.parse(valorPagarDouble) ||
            sumaPagosMetodos == double.parse(valorPagarDouble)) {
          print("Cumple");
          print(detallePagos);
          print("Pagado ${element["pago"]}");
          var pagado = element["pago"];
          valorPagado.value = valorPagado.value + double.parse(pagado);
        } else {
          print("No cumple");
          mensajeAlerta("EL monto sobrepasa el valor");
          detallePagos[index]["pago"] = "0.0";
        }
      });
      detallePagos.refresh();
    }
  }

  cambioFormaPago() {
    if (valorFormaPago.value == 2) {
      valorPagadoController.text = "0";
      detallePagos.value = [];
      valorPagado.value = 0;
      print("Credito");
    } else {
      numeroDias.text = "0";
      generarFormasPagoInicial();
      valorPagado.value = valorPagar;
      valorPagadoController.text = valorPagarController.text;
    }

    generarPagosBool = false;
  }

  cambioFormaEntrega(opt) {
    if (opt == 2) {
      estadoEntrega = "EN PROCESO";
    } else {
      estadoEntrega = "EN PROCESO";
    }
  }

  generarFormasPagoInicial() {
    print("Entro generar");
    var sumaPagosMetodos = 0.0;

    valorPagado.value = 0.0;

    if (valorFormaPago.value == 1) {
      var data = {
        "pago": valorPagar.toString(),
        "metodoPago": metodoPagoDescripcion.value = "EFECTIVO",
        "idMetodoPago": idMetodoPago.value = 1
      };
      // valorPagado.value = double.parse(pago.text);

      detallePagos.add(data);
      print(data);

      var valorPagarDouble = expresionRegular(valorPagar);

      detallePagos.forEach((var element) {
        sumaPagosMetodos = sumaPagosMetodos + double.parse(element["pago"]);

        print("Total ${expresionRegular(sumaPagosMetodos)}");

        print("Pagar $valorPagarDouble");

        if (sumaPagosMetodos < double.parse(valorPagarDouble) ||
            sumaPagosMetodos == double.parse(valorPagarDouble)) {
          print("Cumple");
          print(detallePagos);
          var pagado = element["pago"];
          valorPagado.value = valorPagado.value + double.parse(pagado);
        } else {
          print("No cumple");
          var index = detallePagos.indexWhere((element) => element == data);
          detallePagos[index]["pago"] = "0.0";
          mensajeAlerta("EL monto sobrepasa el valor a Pagar");
        }
      });
    }
  }

  generarFormasPago() {
    print("Entro generar");
    inspect(detallePagos.value);
    var sumaPagosMetodos = 0.0;

    // var data = pago.text.toString() = valorPagar.toString();

    // expresionRegular()

    if (detallePagos.length == 0) {
      valorPagado.value = 0.0;
      var data = {
        "pago": pago.text,
        "metodoPago": metodoPagoDescripcion.value,
        "idMetodoPago": idMetodoPago.value
      };
      // valorPagado.value = double.parse(pago.text);

      detallePagos.add(data);
      print(data);

      var valorPagarDouble = expresionRegular(valorPagar);

      detallePagos.forEach((var element) {
        sumaPagosMetodos = sumaPagosMetodos + double.parse(element["pago"]);

        print("Total ${expresionRegular(sumaPagosMetodos)}");

        print("Pagar $valorPagarDouble");

        if (sumaPagosMetodos < double.parse(valorPagarDouble) ||
            sumaPagosMetodos == double.parse(valorPagarDouble)) {
          print("Cumple");
          print(detallePagos);
          var pagado = element["pago"];
          valorPagado.value = valorPagado.value + double.parse(pagado);
        } else {
          print("No cumple");
          var index = detallePagos.indexWhere((element) => element == data);
          detallePagos[index]["pago"] = "0.0";
          mensajeAlerta("EL monto sobrepasa el valor a Pagar");
        }
      });
    } else {
      if (pago.text != "" && metodoPagoDescripcion.value != "") {
        valorPagado.value = 0.0;
        var data = {
          "pago": pago.text,
          "metodoPago": metodoPagoDescripcion.value,
          "idMetodoPago": idMetodoPago.value
        };
        // // valorPagado.value = double.parse(pago.text);

        detallePagos.add(data);
        print("Entro agregar neuvo metodo");

        var valorPagarDouble = expresionRegular(valorPagar);

        detallePagos.forEach((var element) {
          sumaPagosMetodos = sumaPagosMetodos + double.parse(element["pago"]);

          print("Total ${expresionRegular(sumaPagosMetodos)}");

          print("Pagar $valorPagarDouble");

          if (sumaPagosMetodos < double.parse(valorPagarDouble) ||
              sumaPagosMetodos == double.parse(valorPagarDouble)) {
            print("Cumple");
            print(detallePagos);
            var pagado = element["pago"];
            valorPagado.value = valorPagado.value + double.parse(pagado);
          } else {
            print("No cumple");
            var index = detallePagos.indexWhere((element) => element == data);
            detallePagos.removeAt(index);
            // detallePagos[index]["pago"] = "0.0";
            mensajeAlerta("EL monto sobrepasa el valor a Pagar");
          }
        });
      } else {
        mensajeAlerta("Agrege un monto o metodo de pago");
      }
    }

    generarPagosBool = false;
  }

  eliminar(elemento) {
    print(elemento);

    if (detallePagos.length == 1) {
      // print("Solo tinee 1 mas $detallePagos");
      // mensajeAlerta("Se requiere al menos una forma de pago!");
      // var data = {
      //   "pago": 0.toString(),
      //   "metodoPago": detallePagos[0]["metodoPago"].toString(),
      //   "idMetodoPago": detallePagos[0]["idMetodoPago"].toString()
      // };
      // valorPagado.value = 0.0;

      // detallePagos[0] = data;
      // detallePagos.refresh();
      detallePagos.value = [];
      valorPagado.value = 0.0;
    } else {
      valorPagado.value = 0.0;
      print("Tienes mas $detallePagos");
      detallePagos.remove(elemento);
      print(detallePagos);
      detallePagos.refresh();
      generarPagosBool = false;

      detallePagos.forEach((element) {
        print("Pagado ${element["pago"]}");
        var pagado = element["pago"];
        valorPagado.value = valorPagado.value + double.parse(pagado);
        detallePagos.refresh();
      });

      detallePagos.refresh();
    }

    // detallePagos.remove(elemento);
    // print(detallePagos);
    // detallePagos.refresh();
    // generarPagosBool = false;
  }

  generarPagos() {
    // Confirma si tiene detalles creditos
    if (detallePagos.length != 0) {
      var sumaPagosMetodos = 0.0;

      detallePagos.forEach((var element) {
        sumaPagosMetodos = sumaPagosMetodos + double.parse(element["pago"]);
      });

      DateTime now = new DateTime.now();
      DateTime date = new DateTime(now.year, now.month, now.day);

      // print(numeroPagos.text);
      // print(fecha);

      pagos.clear();
      solicitudNotaPedido.clear();

      if (numeroPagos.text == "") {
        numeroPagos.text = 1.toString();
      }

      print("Arreglo metodos pagos $detallePagos");

      // var cuota = double.parse(valorPagadoController.text);

      // var valorPagado = double.parse(valorPagadoController.text);

      // var saldoTotal;

      var diasPagos = date;

      var cantidadDias = numeroDias.text;

      var contador = int.parse(numeroDias.text);

      // saldoTotal = cuota - valorPagado;

      print("Suma metodos pagos ${sumaPagosMetodos}");

      var total = expresionRegular(valorPagar);
      var pagado = expresionRegular(valorPagado.value);
      var saldo = double.parse(total) - double.parse(pagado);

      if (detallePagos.length != 0) {
        // Agrega lo metodos de pagos escogidos

        cantidadDias = (int.parse(numeroPagos.text)).toString();

        print(expresionRegular(valorPagado.value));
        print(expresionRegular(valorPagar));

        // SIN SALDO
        if (expresionRegular(valorPagado.value) ==
            expresionRegular(valorPagar)) {
          print("Sin saldo pendiente");
          List detallesPagosEnviar = [];

          for (var i = 0; i < 1; i++) {
            var data = {
              "numeroPagos": i,
              "fecha": date,
              "numeroDias": numeroDias.text,
              // "MetodoPago": detallePagos[i]["metodoPago"],
              "valorPagar": double.parse(total),
              "valorPagado": double.parse(pagado),
              "saldo": saldo
            };

            pagos.add(data);
            pagos.refresh();
          }

          // Detalle metodos pagos
          for (var i = 0; i < detallePagos.length; i++) {
            var valor = double.parse(detallePagos[i]["pago"]);

            var data = {
              "IdMedioPago": detallePagos[i]["idMetodoPago"],
              "Valor": expresionRegular(valor)
            };
            print(data);
            detallesPagosEnviar.add(data);
          }

          // CUENTAS X COBRAR
          for (var i = 0; i < 1; i++) {
            var dataSolicutud = {
              "Numero": i,
              "FechaVencimiento": date.toString(),
              "MetodosPago": detallesPagosEnviar,
              "Valor": double.parse(total),
              "valorPagado": double.parse(pagado),
              "Saldo": saldo,
              "IdEstado": 1,
              "Usuario": "admindev"
            };

            solicitudNotaPedido.add(dataSolicutud);
            solicitudNotaPedido.refresh();
          }

          print("Sin saldo");
          inspect(solicitudNotaPedido);
        } else {
          // CON SALDO
          print("Total $total");
          print("Pagado $pagado");

          mensajeAlerta("saldo pendiente de \$${expresionRegular(saldo)}");

          if (formaPagos[1]["id"] == valorFormaPago.value) {
            //CREDITO

            print("NUMERO DIAS ${numeroDias.text}");
            print("NUMERO pagos ${numeroPagos.text}");

            List detallesPagosEnviar = [];

            diasPagos = date;
            contador = 0;

            var cuota = (double.parse(total) - double.parse(pagado)) /
                double.parse(numeroPagos.text);

            //Abono
            for (var i = 0; i < 1; i++) {
              var data = {
                "numeroPagos": i,
                "fecha": date,
                "numeroDias": numeroDias.text,
                // "MetodoPago": 'EFECTIVO',
                "valorPagar": double.parse(pagado),
                "valorPagado": double.parse(pagado),
                "saldo": 0.0
              };

              pagos.add(data);
              pagos.refresh();
            }

            //cuotas
            for (var i = 0; i < int.parse(numeroPagos.text); i++) {
              print("Dias ${numeroDias.text}");

              contador = contador + int.parse(numeroDias.text);
              diasPagos = DateTime(date.year, date.month, date.day + contador);

              var data = {
                "numeroPagos": i + 1,
                "fecha": diasPagos,
                "numeroDias": numeroDias.text,
                // "MetodoPago": 'EFECTIVO',
                "valorPagar": cuota,
                "valorPagado": 0.0,
                "saldo": cuota
              };

              pagos.add(data);
              pagos.refresh();
            }

            // Detalles metodos de pagos
            for (var i = 0; i < detallePagos.length; i++) {
              var valor = double.parse(detallePagos[i]["pago"]);

              var data = {
                "IdMedioPago": detallePagos[i]["idMetodoPago"],
                "Valor": valor
              };
              print(data);
              detallesPagosEnviar.add(data);
            }

            //JSON para enviar
            for (var i = 0; i < pagos.length; i++) {
              var dataSolicutud = {
                "Numero": i,
                "FechaVencimiento": pagos[i]["fecha"].toString(),
                "MetodosPago": detallesPagosEnviar,
                "Valor": pagos[i]["valorPagar"],
                "ValorPagado": pagos[i]["valorPagado"],
                "Saldo": pagos[i]["saldo"],
                "IdEstado": 1,
                "Usuario": "admindev"
              };

              solicitudNotaPedido.add(dataSolicutud);
              solicitudNotaPedido.refresh();
            }
          } else {
            //CONTADO
            print("Tabla de cuotas de contado");

            var cuota = double.parse(pagado);
            var conSaldoValorPagado = double.parse(pagado);
            var conSaldoValorSaldo = cuota - conSaldoValorPagado;
            List detallesPagosEnviar = [];

            for (var i = 0; i < 2; i++) {
              var data = {
                "numeroPagos": i,
                "fecha": diasPagos,
                "numeroDias": 0,
                "valorPagar": cuota,
                "valorPagado": conSaldoValorPagado,
                "saldo": conSaldoValorSaldo
              };

              var dataSolicutud = {
                "Numero": i,
                "FechaVencimiento": date.toString(),
                "MetodosPago": detallesPagosEnviar,
                "Valor": cuota,
                "valorPagado": conSaldoValorPagado,
                "Saldo": conSaldoValorSaldo,
                "IdEstado": 1,
                "Usuario": "admindev"
              };

              cuota = saldo;
              conSaldoValorPagado = 0;
              conSaldoValorSaldo = cuota;

              solicitudNotaPedido.add(dataSolicutud);
              solicitudNotaPedido.refresh();

              pagos.add(data);
              pagos.refresh();
            }

            // Detalles metodos de pagos
            for (var i = 0; i < detallePagos.length; i++) {
              var valor = double.parse(detallePagos[i]["pago"]);

              var data = {
                "IdMedioPago": detallePagos[i]["idMetodoPago"],
                "Valor": valor
              };
              print(data);
              detallesPagosEnviar.add(data);
            }

            for (var i = 0; i < 2; i++) {}

            print("Sin saldo");
            inspect(solicitudNotaPedido);
          }
        }
        generarPagosBool = true;
      } else {
        mensajeAlerta("Genere metodos de pagos");
      }
    } else {
      mensajeAlerta("Debe ingresar metodos de pagos");
    }
  }

  // void launchURL(url) async => await canLaunch(url)
  //     ? await launch(url,
  //         forceSafariVC: true, forceWebView: true, headers: headers)
  //     : throw 'No existe la página';

  enviar() async {
    // Load from URL

    var idLocal = storage.read("idLocal");
    usuario = storage.read("usuario");

    if (generarPagosBool == true) {
      // print(existePDF);
      // print(headers);
      pedidoDetalle.clear();
      DateTime now = new DateTime.now();
      DateTime date = new DateTime(now.year, now.month, now.day);

      print(date.toString());
      var idLocal = storage.read("local");

      var dataCarrito = argumentos["carrito"];

      dataCarrito.forEach((item) {
        // inspect(item);
        // inspect(dataCarrito);

        var dataPedido = {
          "TipoTransaccion": "NP",
          "IdProducto": item["carrito"]["idProducto"],
          "IdLocal": int.parse(idLocal),
          "Cantidad": item["orden"]["pedido"],
          "IdTipoUnidad": item["carrito"]["IdTipoUnidad"],
          "Precio": item["carrito"]["precio"],
          "Subtotal": item["carrito"]["precio"] * item["orden"]["pedido"],
          "CodigoInterno": item["carrito"]["codigoInterno"],
          "NombreProducto": item["carrito"]["descripcion"],
          "UnidadMedida": "UNIDAD",
          "Total": item["carrito"]["precio"] * item["orden"]["pedido"],
        };

        pedidoDetalle.add(dataPedido);
      });

      // print("Data $dataCarrito");

      var subTotal12 = double.parse(expresionRegular(valorPagar));
      var total = double.parse(expresionRegular(valorPagar));

      var body = {
        "IdVendedor": usuario,
        "IpIngreso": "",
        "FechaHoraEntregaPropuesta": date.toString(),
        "IdLocal": int.parse(idLocal),
        "Fecha": date.toString(),
        "TipoTransaccion": "NP",
        "IdCliente": argumentos["idCliente"],
        "Subtotal0": 0,
        "Subtotal12": total,
        "Total": total,
        "Saldo": 0,
        "Plazo": 0,
        "Cuotas": 0,
        "ValorAdicional": 0,
        "EstadoActual": estadoEntrega,
        "FechaHoraEntrega": date.toString(),
        "IdFormaPago": valorFormaPago.value,
        "UsuarioIngreso": usuario,
        "FechaIngreso": date.toString(),
        "DetalleNotaPedido": pedidoDetalle.value,
        "CuentasPorCobrar": solicitudNotaPedido.value,
      };

      // inspect(argumentos);
      inspect(body);

      var dataEnviar = json.encode(body);
      print(dataEnviar);

      await API.instance.postPedidoCompra(dataEnviar).then((value) async {
        print(" DATOS $value");
        inspect(value);

        if (value["Id"] != "") {
          print("Tiene Id");
          doc = await PDFDocument.fromURL(
              "https://api.binasystem.com/tendaGO/output/${value["Id"]}/download?format=PDF",
              headers: headers);
          existePDF.value = true;
        } else {
          print("No tiene id");
        }
      });
    } else {
      mensajeAlerta("Debe generar pagos");
    }
  }
}
