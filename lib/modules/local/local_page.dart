import 'package:Novedades/global_widgets/fondo_login.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../../utils.dart';
import 'local_controller.dart';

class LocalPage extends GetWidget<LocalController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[FondoLogin(), _loginForm()],
          ),
        ),
      ),
    );
  }

  Widget _loginForm() {
    // final bloc = Provider.of(context);
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
              child: Container(
            height: 100.0,
          )),
          Container(
            width: Get.width * 1,
            height: Get.height * 1,
            padding: EdgeInsets.symmetric(vertical: 50.0),
            margin: EdgeInsets.symmetric(vertical: 30.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(55.0)),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.grey,
                      blurRadius: 1.0,
                      offset: Offset(0.0, 1.0),
                      spreadRadius: 1.0)
                ]),
            child: Column(
              children: [
                Text(
                  "SELECCIONA UN LOCAL",
                  style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: Get.height * 0.02),
                ),
                SizedBox(
                  height: 30.0,
                ),
                locales()
              ],
            ),
          )
        ],
      ),
    );
  }
}

Widget locales() {
  final controller = Get.put(LocalController());
  return Obx(() => ListView.separated(
        shrinkWrap: true,
        separatorBuilder: (context, index) => Divider(
          color: Colors.black,
        ),
        itemBuilder: (context, index) {
          return GestureDetector(
              onTap: () {
                controller.guardarLocal(controller.locales[index].id);
              },
              child: Center(
                child: Text(
                  controller.locales[index].local,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: Get.height * 0.02),
                ),
              ));
        },
        itemCount: controller.locales.length,
      ));
}
