import 'dart:async';
import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalController extends GetxController {
  RxList<Local> locales = <Local>[].obs;

  @override
  void onInit() {
    super.onInit();
    cargarLocales();
  }

  cargarLocales() async {
    locales.value = await API.instance.getLocales();
  }

  guardarLocal(idLocal) async {
    final storage = GetStorage();
    storage.write("local", idLocal.toString());
    Get.offAllNamed(AppRoutes.TABS);
  }
}
