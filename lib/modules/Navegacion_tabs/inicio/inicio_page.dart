import 'package:Novedades/global_widgets/search/search.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/state_manager.dart';
import 'package:hexcolor/hexcolor.dart';
import 'inicio_controller.dart';
import 'package:get/get.dart';

class InicioPage extends GetWidget<InicioController> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: HexColor("#B76AC8"),
          // leading: Row(
          //   children: [],
          // ),
          actions: [
            // IconButton(
            //     icon: Icon(Icons.list_alt),
            //     iconSize: Get.width * 0.07,
            //     onPressed: () {
            //       agregarCantidad();
            //     }),
            // IconButton(
            //     icon: Icon(Icons.search),
            //     iconSize: Get.width * 0.07,
            //     onPressed: () {
            //       showSearch(context: context, delegate: ProductoSearch());
            //     }),
            IconButton(
                iconSize: Get.width * 0.07,
                icon: Icon(Icons.qr_code_scanner_sharp),
                onPressed: () {
                  controller.escanear();
                }),
            IconButton(
                icon: Icon(Icons.logout),
                iconSize: Get.width * 0.07,
                onPressed: () {
                  controller.alertaCerrarSesion();
                }),
          ],
          title: GestureDetector(
            onTap: () {
              Get.bottomSheet(verlocales());
            },
            child: Text(
              "Novedades",
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: Get.width * 0.06),
            ),
          ),
        ),
        body: Center(child: Image.asset("assets/img/novedades_carmita.jpg")));
  }

  verlocales() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        width: Get.width * 0.80,
        height: 500,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "Novedades Carmita",
                style: TextStyle(color: Colors.black54),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ListView.separated(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (_, int posicion) {
                  return ListTile(
                    title: Center(
                      child: Text(
                        controller.locales[posicion].local,
                        style: TextStyle(color: Colors.blueAccent),
                      ),
                    ),
                    onTap: () {
                      controller.guardarLocal(controller.locales[posicion].id);
                    },
                  );
                },
                separatorBuilder: (_, index) => Divider(
                      color: Colors.black12,
                    ),
                itemCount: controller.locales.length)
          ],
        ),
      ),
    );
  }

  buscador() {
    return Row(
      children: [Column(), Icon(Icons.scanner_rounded)],
    );
  }

  agregarCantidad() {
    return Get.defaultDialog(
      backgroundColor: colorPrincipal,
      // title: "",
      titleStyle: TextStyle(color: Colors.white),
      confirm: FlatButton.icon(
          onPressed: () {
            controller.guardarNumeroOrdenes();
          },
          icon: Icon(
            Icons.check,
            color: colorPrincipal,
          ),
          label: Text(
            "Aceptar",
            style: TextStyle(color: Colors.white),
          )),
      cancel: FlatButton.icon(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.cancel,
            color: colorPrincipal,
          ),
          label: Text(
            "Cancelar",
            style: TextStyle(color: Colors.white),
          )),
      content: Column(
        children: [
          // Image.asset(
          //   'assets/img/logo.png',
          //   height: 120,
          // ),
          Center(
            child: Text(
              "Ingrese el número de ordenes a generar",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextFormField(
              controller: controller.cantidadController,
              validator: (value) {
                if (value.isEmpty) {
                  return "Nombre es requerido";
                }
                return null;
              },
              // controller: controller.cedulaText,
              keyboardType: TextInputType.number,
              style: TextStyle(
                color: Colors.grey,
                fontFamily: 'OpenSans',
              ),

              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                contentPadding: EdgeInsets.only(top: 14.0),
                hintText: 'Nº de órdenes',
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
