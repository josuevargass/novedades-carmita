import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class InicioController extends GetxController {
  RxList<Productos> productos = <Productos>[].obs;
  RxList<Local> locales = <Local>[].obs;

  var idLocal = "".obs;

  TextEditingController cantidadController = new TextEditingController();
  final storage = GetStorage();

  @override
  void onInit() {
    super.onInit();
    cargarProductosByLocal();
    cargarLocales();
  }

  Future<void> escanear() async {
    String barcode;
    try {
      barcode = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.BARCODE);
      print("CODIGO BARRA $barcode");
      if (barcode != "") {
        storage.write('codigoBarra', barcode);
        Get.toNamed(AppRoutes.DETALLEPRODUCTO);
      }
    } catch (e) {
      print(e);
    }
  }

  alertaCerrarSesion() {
    return Get.defaultDialog(
      backgroundColor: Colors.black,
      // title: "",
      titleStyle: TextStyle(color: Colors.white),
      content: Column(
        children: [
          BounceInDown(
              child: Image.asset(
            'assets/img/logo.png',
            height: 120,
          )),
          Text(
            "¿Estás seguro de cerrar sesión?",
            style: TextStyle(color: Colors.white),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: Text(
                    "No",
                    style: TextStyle(color: colorSecundario),
                  )),
              FlatButton(
                  onPressed: () {
                    storage.remove("token");
                    Get.offAllNamed(AppRoutes.LOGIN);
                  },
                  child: Text(
                    "Si",
                    style: TextStyle(color: colorSecundario),
                  )),
            ],
          )
        ],
      ),
    );
  }

  Future<void> cargarProductosByLocal() async {
    idLocal.value = storage.read("local");
    if (idLocal.value == null) {
      idLocal.value = "1";
    }
    // storage.listenKey('carrito', (value) {
    //   carrito.value = value;
    //   carrito.refresh();
    //   print("CARRITO: ${carrito.value}");
    //   print("LISTA GUARDADA: $value");
    // });
    print("Hola ${idLocal.value}");
    // storage.listenKey("local", (value) {
    //   print(value);
    // });

    print("ID LOCAL: $idLocal");
    productos.value = await API.instance.getProductosByIdLocal(idLocal);
    // print(productos);
  }

  guardarNumeroOrdenes() async {
    final ordenes = cantidadController.text;
    storage.write("ordenes", ordenes);
    // print(ordenes);
    Get.back();
  }

  guardarLocal(idLocal) async {
    final storage = GetStorage();
    storage.write("local", idLocal.toString());
    print("LocalId $idLocal");
    cargarProductosByLocal();
    Get.back();
  }

  cantidadOrdenes() {
    return Get.defaultDialog(
      backgroundColor: Colors.black,
      // title: "",
      titleStyle: TextStyle(color: Colors.white),
      content: Column(
        children: [
          // BounceInDown(
          //     child: Image.asset(
          //   'assets/img/logo.png',
          //   height: 120,
          // )),
          Text(
            "Ingrese el numero de ordenes a generar.",
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: TextField(
              keyboardType: TextInputType.number,
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'OpenSans',
              ),
              decoration: InputDecoration(
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.red),
                ),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                prefixIcon: Icon(
                  Icons.person,
                  color: colorSecundario,
                ),
                contentPadding: EdgeInsets.only(top: 14.0),
                hintText: 'Nº de ordenes',
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'OpenSans',
                ),
              ),
              // controller: controller.emailController,
            ),
          ),
        ],
      ),
    );
  }

  Future<void> cargarLocales() async {
    locales.value = await API.instance.getLocales();
  }
}
