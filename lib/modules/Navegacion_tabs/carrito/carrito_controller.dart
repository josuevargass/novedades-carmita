import 'dart:developer';
import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/global_widgets/global_controller.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class CarritoController extends GetxController
    with SingleGetTickerProviderMixin {
  var globalController = Get.put(GlobalController());

  final storage = GetStorage();

  // VARIABLES
  RxInt indexCarrito = 0.obs;
  RxBool existeIndex = true.obs;
  RxString nombreLocal = "".obs;
  String observacion = "";

  // LISTAS
  RxDouble sumaCantidadPagarCarrito = 0.0.obs;
  RxList<Productos> productos = <Productos>[].obs;
  RxList listaCarrito = [].obs;
  RxList dataCarrito = [].obs;
  RxList ordenesCarritos = [].obs;
  List carritoEnvio = [];

  // VARIABLES ESPECIALES
  TabController tabController;
  TextEditingController cantidadController = new TextEditingController();
  TextEditingController observacionController = new TextEditingController();

  @override
  void onInit() {
    super.onInit();
    getCarrito();
    getLocal();
    cantidadController.text = "1";
  }

  // TRAER LOS DATOS DEL CARRITO
  getCarrito() {
    var listaCarritoStorage = storage.read("carritos");
    var indexCarritoStorage = storage.read("indexCarrito");
    var listaOrdenes = storage.read("ordenesCarrito");

    if (dataCarrito.length != 0) {
      dataCarrito.refresh();
      sumarCantidadCarritoPagar();
    }

    if (listaCarritoStorage != null) {
      if (listaCarritoStorage.length != 0) {
        print("Datos carritos $listaCarrito");
        listaCarrito.value = listaCarritoStorage;

        if (indexCarritoStorage != null) {
          indexCarrito.value = indexCarritoStorage;
          dataCarrito.value = listaCarrito[indexCarrito.value];
        }

        ordenesCarritos.value = listaOrdenes;
        print("Index ${indexCarrito.value}");
        listaCarrito.refresh();
        mostrarCarritoByPosicion(indexCarrito.value);
      } else {
        print("No existen carritos creados");
        ordenesCarritos.value = [];
        listaCarrito.value = [];
        indexCarrito.value = 0;
        // nuevoCarrito();
      }
    } else {
      print("No existen carritos");
      listaCarrito.value = [];
    }
  }

  // TRAER LOCAL
  getLocal() {
    var local = storage.read("local");
    print("ID LOCAL: $local");
    if (local != null) {
      globalController.getLocalById(local);
    } else {
      Get.offAllNamed(AppRoutes.LOCAL);
    }
  }

  // DETALLE DE LAS ORDENES
  detalleOrdenes(posicion) {

    var sumaCantidad = 0;
    var cantidad = 0;
    var cumple = true;
    List arrCodigosPadres = [];
    List codigosPadres = [];
 
    carritoEnvio.clear();
    inspect(dataCarrito.value);
    dataCarrito.forEach((element) {
      // Validacion si esque tiene productoPadre
      if (element["codigoPadre"] != null) {
        var data = {
          "producto": element,
          "codigoPadre": element["codigoPadre"],
          "cantidad": element["ordenes"][posicion]["pedido"],
          "cantidadMinima": element["cantidadMinima"]
        };

        codigosPadres.add(data);

        var existe = arrCodigosPadres.firstWhere((item) => item == element["codigoPadre"], orElse: () => '');

        if (existe == '') {
          arrCodigosPadres.add(element["codigoPadre"]);
        } 
      }
      // Envio de datos hacia detalles pedido
      var data = element["ordenes"][posicion];
      var body = {
        "observacion": observacionController.text,
        "carrito": element,
        "orden": data
      };
      carritoEnvio.add(body);
    });

    //Cumple o no la validacion
    for (var i = 0; i < arrCodigosPadres.length; i++) {
      var productoTemp = {};
      var suma = 0;
      var cantidadMinima = 0.0;
      codigosPadres.forEach((item) { 
        if (arrCodigosPadres[i] == item["codigoPadre"]) {
          productoTemp = item["producto"];
          cantidadMinima = item["cantidadMinima"];
          // cantidadMinima = int.parse(item["cantidadMinima"].toString());
          suma = suma + item["cantidad"];
        }
      });

      print("Suma ${double.parse(suma.toString())} y cant Minima $cantidadMinima");
      if (double.parse(suma.toString())  >= cantidadMinima) {
        print("Cumple");
      } else {
        print("No cumple validacion codigo padre");
        mensajeAlerta("No cumple la validacion ${productoTemp["codigoInterno"]}");
        cumple = false;
      }
    }

    inspect(carritoEnvio);
    inspect(codigosPadres);
    print("Arreglo Codigos $arrCodigosPadres");


    if (cumple) {
      // CUMPLE LA CONDICION CON PRODUCTOPADRE O NO

      print("Suma total cantidad $cantidad");
      inspect(carritoEnvio);
      Get.toNamed(AppRoutes.DETALLEPEDIDO, arguments: carritoEnvio);
    }

  }

  // ESCANEAR CODIGO QR Y CODIGO DE BARRA
  Future<void> escanear() async {
    String barcode;
    try {
      barcode = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.BARCODE);
      print("CODIGO BARRA $barcode");
      if (barcode != "") {
        storage.write('codigoBarra', barcode);
        Get.toNamed(AppRoutes.DETALLEPRODUCTO);
      }
      if (barcode == "-1") {
        Get.back();
      }
    } catch (e) {
      print(e);
    }
  }

  nuevoCarrito() {
    dataCarrito.value = [];

    listaCarrito.add(dataCarrito.toList());

    storage.remove("carritos");
    storage.write("carritos", listaCarrito.value);
    inspect(listaCarrito.value);

    final data1 = storage.read("ordenesCarrito");
    print("DATOS ORDENES $data1");

    indexCarrito.value = listaCarrito.length - 1;

    sumarCantidadCarritoPagar();

    var ordenesCarrito = {"ordenes": "1"};

    print("Nuevo carrito e index $ordenesCarrito");

    ordenesCarritos.value.add(ordenesCarrito);

    storage.write("ordenesCarrito", ordenesCarritos.value);
  }

  guardarNumeroOrdenes() async {
    final ordenes = cantidadController.text;
    print(
        "Arreglo ${ordenesCarritos.value} IndexCarrito ${indexCarrito.value}");

    ordenesCarritos[indexCarrito.value] = {"ordenes": ordenes};

    print("Arreglo $ordenesCarritos $indexCarrito");

    storage.write("ordenesCarrito", ordenesCarritos.value);

    ordenesCarritos.refresh();

    Get.back();
  }

  borrarCarrito(index) {
    print(index);
    print("Si tiene coincidencia ELIMINAR $ordenesCarritos");
    var ordenes = storage.read("ordenesCarrito");
    var data = storage.read("carritos");

    print("ORDENES $ordenes");
    print("CARRITOS $data");

    ordenesCarritos.removeAt(index);
    storage.remove("ordenesCarrito");
    storage.write("ordenesCarrito", ordenesCarritos);
    var orde = storage.read("ordenesCarrito");
    print("NUEVO ORDENES ${orde}");
    listaCarrito.removeAt(index);
    storage.remove("carritos");
    storage.write("carritos", listaCarrito.value);
    print("NUEVO carritos ${listaCarrito.value}");

    listaCarrito.refresh();
    ordenesCarritos.refresh();

    if (index != 0) {
      indexCarrito.value = indexCarrito.value - 1;
      mostrarCarritoByPosicion(indexCarrito.value);
    }

    if (ordenesCarritos.length == 0) {
      print("ARREGLO TAMANO A 0");
      indexCarrito.value = 0;
      nuevoCarrito();
    } else {
      try {
        var existeIndexOrden = ordenesCarritos[indexCarrito.value];
        print("si Existe index orden $existeIndexOrden");
        mostrarCarritoByPosicion(indexCarrito.value);
      } catch (e) {
        print("NO Existe index orden");
        indexCarrito.value = 0;
        mostrarCarritoByPosicion(indexCarrito.value);
      }
    }

    Get.back();
  }

  mostrarCarritoByPosicion(index) {
    print("Data ordenes ${ordenesCarritos}");
    print("Index carrito ${indexCarrito.value}");
    indexCarrito.value = index;
    inspect(listaCarrito.value);
    storage.write("indexCarrito", index);
    dataCarrito.value = listaCarrito[index];
    sumarCantidadCarritoPagar();
    existeIndex.value = true;
  }

  // TOTAL CANTIDAD PRODUCTOS A LLEVAR
  sumarCantidadCarritoPagar() {
    sumaCantidadPagarCarrito.value = 0.0;
    print("Index carrito actual ${indexCarrito.value}");
    if (dataCarrito.value.length != 0) {
      for (var item in dataCarrito.value) {
        var precio = item["precio"];
        var ordenes = item["ordenes"];
        for (var orden in ordenes) {
          sumaCantidadPagarCarrito.value =
              sumaCantidadPagarCarrito.value + (orden["pedido"] * precio);
        }
      }
      print("Suma Cant. Carrito ${sumaCantidadPagarCarrito.value}");
    }
  }

  // ELIMINAR PRODUCTO
  eliminarProducto(codigoInterno) {
    var codigosPadres = storage.read("codigosPadres");

    inspect(codigosPadres);

    var eliminarElemento = dataCarrito
        .firstWhere((element) => element["codigoInterno"] == codigoInterno);
    dataCarrito.remove(eliminarElemento);
    print("ELIMADO ES: $eliminarElemento");
    print("CARRITO: ${dataCarrito.value}");

    listaCarrito.value[indexCarrito.value] = dataCarrito.value;
    dataCarrito.refresh();
    storage.write("carritos", listaCarrito.value);
    sumarCantidadCarritoPagar();
    inspect(listaCarrito.value);

    Get.back();
    fallaOEliminacionMensajeSnackBar(
        "Eliminar", "Se elimino el producto del carrito");
  }
}
