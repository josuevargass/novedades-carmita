import 'dart:developer';

import 'package:Novedades/global_widgets/global_controller.dart';
import 'package:Novedades/global_widgets/lista_locales.dart';
import 'package:Novedades/global_widgets/search/search.dart';
import 'package:Novedades/modules/Navegacion_tabs/carrito/carrito_controller.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:Novedades/utils/limitar_cantordenes.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

class CarritoPage extends GetWidget<CarritoController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: colorSecundario,
          title: GestureDetector(
            onTap: () {
              Get.offAllNamed(AppRoutes.LOCAL);
            },
            child: Obx(() => Text(
                  controller.globalController.nombreLocal.value != ""
                      ? controller.globalController.nombreLocal.value
                      : "Novedades",
                  textAlign: TextAlign.start,
                  style: TextStyle(fontSize: Get.width * 0.06),
                )),
          ),
          actions: [
            IconButton(
                iconSize: Get.width * 0.07,
                icon: Icon(Icons.add_shopping_cart),
                onPressed: () {
                  agregarCantidadCarritoNuevo();
                }),
            IconButton(
                icon: Icon(Icons.qr_code_scanner_sharp),
                iconSize: Get.width * 0.07,
                onPressed: () {
                  controller.escanear();
                }),
            IconButton(
                icon: Icon(Icons.delete),
                iconSize: Get.width * 0.07,
                onPressed: () {
                  mostrarAlertaEliminarCarrito(controller.indexCarrito.value);
                  // controller.borrarCarrito(controller.indexCarrito.value);
                }),
          ],
        ),
        body: ListView(
          shrinkWrap: true,
          children: [
            segmento(),
            SizedBox(
              height: 30,
            ),
            titulo(),
            SizedBox(
              height: 30,
            ),
            Obx(() => controller.dataCarrito.length != 0
                ? carroCompras()
                : Column(
                    children: [
                      Text(
                        "No hay productos en este carrito",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: Get.width * 0.04),
                      ),
                    ],
                  ))
          ],
        ),
        // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        bottomNavigationBar: Obx(() => controller.listaCarrito.length != 0
            ? cargarDatosFlotantes()
            : Text(
                "",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: Get.width * 0.04),
              )));
  }

  segmento() {
    return Container(
        height: 100,
        child: Obx(() => ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: controller.listaCarrito.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                  onTap: (() {
                    controller.mostrarCarritoByPosicion(index);
                    carroCompras();
                  }),
                  child: Column(
                    children: [
                      Obx(() => controller.indexCarrito.value == index
                          ? Container(
                              height: 100,
                              width: 200,
                              color: colorPrincipal,
                              child: Stack(
                                children: [
                                  Center(
                                    child: Container(
                                      margin: EdgeInsets.only(
                                          right: Get.width * 0.02,
                                          bottom: Get.width * 0.07),
                                      width: Get.width * 0.05,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: Colors.red),
                                      child: Text(
                                        (controller.dataCarrito.value.length)
                                            .toString(),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(Icons.shopping_cart_sharp,
                                          color: Colors.white),
                                      Center(
                                          child: Text(
                                        "${index + 1}",
                                        style: TextStyle(
                                            fontSize: 18, color: Colors.white),
                                      ))
                                    ],
                                  ),
                                  Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Obx(() =>
                                        controller.ordenesCarritos.length != 0
                                            ? GestureDetector(
                                                onTap: () => agregarCantidad(),
                                                child: Text(
                                                  "ÓRDENES: " +
                                                      (controller.ordenesCarritos[
                                                                  controller
                                                                      .indexCarrito
                                                                      .value]
                                                              ["ordenes"])
                                                          .toString(),
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              )
                                            : GestureDetector(
                                                onTap: () => agregarCantidad(),
                                                child: Text(
                                                  "ÓRDENES: " + "0",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              )),
                                  )
                                ],
                              ))
                          : Container(
                              height: 100,
                              width: 200,
                              color: colorSecundario,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.shopping_cart_outlined,
                                      color: Colors.white),
                                  Center(
                                      child: Text(
                                    "${index + 1}",
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  )),
                                ],
                              )))
                    ],
                  ));
            })));
  }

  titulo() {
    return Center(
        child: Obx(
      () => Text(
        "Carrito ${controller.indexCarrito.value + 1}",
        style:
            TextStyle(fontWeight: FontWeight.bold, fontSize: Get.width * 0.06),
      ),
    ));
  }

  carroCompras() {
    return Obx(() => ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: [
            ListView.builder(
              // physics: NeverScrollableScrollPhysics(), // new line
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemCount: controller.dataCarrito.length,
              itemBuilder: (BuildContext context, int index) {
                return Slidable(
                  actionPane: SlidableDrawerActionPane(),
                  actionExtentRatio: 0.25,
                  child: Container(
                      color: Colors.white,
                      child: SingleChildScrollView(
                        child: ListTile(
                          leading: Container(
                            height: 80,
                            width: 80,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage(controller
                                        .dataCarrito[index]["photoUrl"]))),
                          ),
                          title: Text(
                            controller.dataCarrito[index]["codigoInterno"],
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          subtitle: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                controller.dataCarrito[index]["titulo"],
                              ),
                              // Text("Tipo Unidad: " +
                              //   controller.dataCarrito[index]["tipoUnidad"],
                              // ),
                              Text(
                                "Precio: " +
                                    "\$" +
                                    expresionRegular(controller
                                        .dataCarrito[index]["precio"]),
                              ),
                              Text(
                                "Cantidad: " +
                                    controller.dataCarrito[index]
                                            ["cantidadTotal"]
                                        .toString(),
                              ),
                              // Text(
                              //   "Total: " +
                              //       controller.dataCarrito[index]["ordenes"].length.toString(),
                              // )
                            ],
                          ),
                          onTap: () {
                            var data = {
                              "dataProducto": controller.dataCarrito[index],
                              "indexCarrito": controller.indexCarrito.value
                            };
                            Get.toNamed(AppRoutes.EDITARPRODUCTO,
                                arguments: data);
                            // ProductoSearch(controller.dataCarrito[index]);
                            // inspect(controller.dataCarrito[index]);
                          },
                        ),
                      )),
                  secondaryActions: <Widget>[
                    IconSlideAction(
                        caption: 'Eliminar',
                        color: Colors.red,
                        icon: Icons.delete,
                        onTap: () {
                          mostrarAlertaEliminarItem(
                              controller.dataCarrito[index]["codigoInterno"]);
                        }),
                  ],
                );
              },
            ),
            SizedBox(
              height: 30,
            ),
          ],
        ));
  }

  mostrarAlertaEliminarItem(codigoInterno) {
    Get.defaultDialog(
        backgroundColor: Colors.black,
        // title: "",
        titleStyle: TextStyle(color: Colors.white),
        content: Column(
          children: [
            Image.asset(
              'assets/img/logo.png',
              height: 120,
            ),
            Text(
              "¿Estás seguro de eliminar este producto?",
              textAlign: TextAlign.center,
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            )
          ],
        ),
        actions: [
          FlatButton(
            onPressed: () => Get.back(),
            child: Text("No"),
            textColor: colorSecundario,
          ),
          FlatButton(
              onPressed: () async {
                controller.eliminarProducto(codigoInterno);
              },
              child: Text("Si"),
              textColor: colorPrincipal),
        ]);
  }

  mostrarAlertaEliminarCarrito(index) {
    Get.defaultDialog(
        backgroundColor: Colors.black,
        // title: "",
        titleStyle: TextStyle(color: Colors.white),
        content: Column(
          children: [
            Image.asset(
              'assets/img/logo.png',
              height: 120,
            ),
            Text(
              "¿Estás seguro de eliminar este carrito?",
              textAlign: TextAlign.center,
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            )
          ],
        ),
        actions: [
          FlatButton(
            onPressed: () => Get.back(),
            child: Text("No"),
            textColor: colorSecundario,
          ),
          FlatButton(
              onPressed: () async {
                controller.borrarCarrito(index);
              },
              child: Text("Si"),
              textColor: colorPrincipal),
        ]);
  }

  agregarCantidad() {
    return Get.defaultDialog(
      backgroundColor: colorSecundario,
      // title: "",
      titleStyle: TextStyle(color: Colors.white),
      confirm: FlatButton.icon(
          onPressed: () {
            controller.guardarNumeroOrdenes();
          },
          icon: Icon(
            Icons.check,
            color: Colors.black,
          ),
          label: Text(
            "Aceptar",
            style: TextStyle(color: Colors.white),
          )),
      cancel: FlatButton.icon(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.cancel,
            color: Colors.black,
          ),
          label: Text(
            "Cancelar",
            style: TextStyle(color: Colors.white),
          )),
      content: Column(
        children: [
          // Image.asset(
          //   'assets/img/logo.png',
          //   height: 120,
          // ),
          Center(
            child: Text(
              "Ingrese el número de ordenes a generar",
              textAlign: TextAlign.center,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextFormField(
              controller: controller.cantidadController,
              validator: (value) {
                if (value.isEmpty) {
                  return "Nombre es requerido";
                }
                return null;
              },
              // controller: controller.cedulaText,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly,
                // LimitRangeTextInputFormatter(1, 100),
              ],
              style: TextStyle(
                color: Colors.grey,
                fontFamily: 'OpenSans',
              ),

              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                contentPadding: EdgeInsets.only(top: 14.0),
                hintText: 'Nº de órdenes',
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  observacionOrden() {
    return Get.defaultDialog(
      middleTextStyle: TextStyle(color: Colors.white),
      backgroundColor: colorSecundario,
      // title: "",
      titleStyle: TextStyle(color: Colors.white),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.bottomLeft,
            child: Icon(Icons.close, color: Colors.white),
          ),
          Center(
            child: Text(
              "Ingrese la observación",
              textAlign: TextAlign.center,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextFormField(
              maxLines: 5,
              controller: controller.observacionController,
              onChanged: (observacion) {
                controller.observacion = observacion;
              },
              validator: (value) {
                if (value.isEmpty) {
                  return "Observación es requerido";
                }
                return null;
              },
              // controller: controller.cedulaText,
              keyboardType: TextInputType.text,
              style: TextStyle(
                color: Colors.grey,
                fontFamily: 'OpenSans',
              ),

              decoration: InputDecoration.collapsed(
                filled: true,
                fillColor: Colors.white,
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                hintText: 'Observación',
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  agregarCantidadCarritoNuevo() {
    return Get.defaultDialog(
      backgroundColor: Colors.black,
      // title: "",
      titleStyle: TextStyle(color: Colors.white),
      confirm: FlatButton.icon(
          onPressed: () {
            controller.nuevoCarrito();
            controller.guardarNumeroOrdenes();
          },
          icon: Icon(
            Icons.check,
            color: colorPrincipal,
          ),
          label: Text(
            "Aceptar",
            style: TextStyle(color: Colors.white),
          )),
      cancel: FlatButton.icon(
          onPressed: () {
            Get.back();
          },
          icon: Icon(
            Icons.cancel,
            color: colorPrincipal,
          ),
          label: Text(
            "Cancelar",
            style: TextStyle(color: Colors.white),
          )),
      content: Column(
        children: [
          Image.asset(
            'assets/img/logo.png',
            height: 120,
          ),
          Center(
            child: Text(
              "Ingrese el número de ordenes a generar",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: TextFormField(
              controller: controller.cantidadController,
              validator: (value) {
                if (value.isEmpty) {
                  return "Nombre es requerido";
                }
                return null;
              },
              // controller: controller.cedulaText,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.digitsOnly
              ],
              style: TextStyle(
                color: Colors.grey,
                fontFamily: 'OpenSans',
              ),

              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.white,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white),
                ),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                contentPadding: EdgeInsets.only(top: 14.0),
                hintText: 'Nº de órdenes',
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _botones() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [botonAgregarProducto(), botonEnviar()],
      ),
    );
  }

  Widget botonAgregarProducto() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        RaisedButton(
          color: colorPrincipal,
          onPressed: () {
            showSearch(
                context: Get.context,
                delegate: ProductoSearch(controller.indexCarrito.value));
          },
          child: SizedBox(
            width: Get.width * 0.4,
            child: Text(
              "AGREGAR",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  Widget botonEnviar() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        RaisedButton(
          color: colorPrincipal,
          onPressed: () {
            Get.bottomSheet(verListadoNumeroOrdenes());

            // Get.toNamed(AppRoutes.DETALLEPEDIDO,
            //     arguments: controller.sumaCantidadPagarCarrito.value);
          },
          child: SizedBox(
            width: Get.width * 0.4,
            child: Text(
              "GUARDAR",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
          ),
        )
      ],
    );
  }

  verListadoNumeroOrdenes() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.white,
        ),
        width: Get.width * 0.80,
        height: 400,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                "Número de órdenes",
                style: TextStyle(
                    color: Colors.black54, fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Obx(() => ListView.separated(
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (_, int posicion) {
                  return Center(
                    child: GestureDetector(
                      onTap: () {
                        controller.detalleOrdenes(posicion);
                      },
                      child: FadeInRight(
                          // duration: Duration(microseconds: 300),
                          child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.list_alt_sharp),
                          Text(
                            "Orden " + (posicion + 1).toString(),
                            style: TextStyle(fontSize: Get.width * 0.05),
                          ),
                        ],
                      )),
                    ),
                  );
                },
                separatorBuilder: (_, index) => Divider(
                      color: Colors.black12,
                    ),
                itemCount: int.parse(
                    controller.ordenesCarritos[controller.indexCarrito.value]
                        ["ordenes"])))
          ],
        ),
      ),
    );
  }

  totalPagarCarrito() {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Obx(
              () => Text(
                "Total: \$" +
                    expresionRegular(controller.sumaCantidadPagarCarrito.value),
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: Get.width * 0.06),
              ),
            ),
            botonComentario()
          ],
        ));
  }

  cargarDatosFlotantes() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [totalPagarCarrito(), _botones()],
    );
  }

  botonComentario() {
    return FloatingActionButton(
      splashColor: Colors.red,
      backgroundColor: colorSecundario,
      onPressed: () {
        observacionOrden();
      },
      child: Icon(Icons.comment),
    );
  }
}
