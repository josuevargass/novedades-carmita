import 'package:Novedades/modules/Navegacion_tabs/carrito/carrito_controller.dart';
import 'package:Novedades/modules/Navegacion_tabs/carrito/carrito_pages.dart';
import 'package:Novedades/modules/Navegacion_tabs/cliente/cliente_page.dart';
import 'package:Novedades/modules/Navegacion_tabs/historial/historial_page..dart';
import 'package:Novedades/modules/Navegacion_tabs/inicio/inicio_page.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hexcolor/hexcolor.dart';

import 'tabs_controller.dart';

class TabsPage extends GetWidget<TabsController> {
  final storage = GetStorage();

  GlobalKey _bottomNavigationKey = GlobalKey();
  final carritoController = Get.put(CarritoController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Get.width > 500
          ? CurvedNavigationBar(
              key: _bottomNavigationKey,
              index: 0,
              height: 70,
              items: [
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Icon(
                    Icons.store,
                    size: 50,
                    color: Colors.white,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Icon(
                    Icons.shopping_cart_outlined,
                    size: 50,
                    color: Colors.white,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Icon(
                    Icons.history,
                    size: 50,
                    color: Colors.white,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Icon(
                    Icons.person_add,
                    size: 50,
                    color: Colors.white,
                  ),
                ),
              ],
              // color: Color.fromRGBO(245, 142, 255, 0.9),
              color: HexColor("#B76AC8"),
              buttonBackgroundColor: HexColor("#C880D2"),
              backgroundColor: HexColor("#C880D2"),
              animationCurve: Curves.easeInOut,
              animationDuration: Duration(milliseconds: 600),
              onTap: (index) => {
                print(controller.selectedIndex.value),
                controller.selectedIndex.value = index
              },
              letIndexChange: (index) => true,
            )
          : CurvedNavigationBar(
              key: _bottomNavigationKey,
              index: 0,
              height: 50,
              items: [
                Icon(
                  Icons.shopping_cart_outlined,
                  size: 30,
                  color: Colors.white,
                ),
                // Icon(
                //   Icons.store,
                //   size: 30,
                //   color: Colors.white,
                // ),
                Icon(
                  Icons.person_add,
                  size: 30,
                  color: Colors.white,
                ),
                Icon(
                  Icons.history,
                  size: 30,
                  color: Colors.white,
                ),
              ],
              // color: Color.fromRGBO(245, 142, 255, 0.9),
              color: colorSecundario,
              buttonBackgroundColor: colorPrincipal,
              backgroundColor: colorPrincipal,
              animationCurve: Curves.easeInOut,
              animationDuration: Duration(milliseconds: 600),
              onTap: (index) async {
                print(controller.selectedIndex.value);
                controller.selectedIndex.value = index;
                if (controller.selectedIndex.value == 0) {
                  carritoController.onInit();
                  carritoController.sumaCantidadPagarCarrito();
                  // carritoController.arrOrdenesCarrito.value =
                  //     await storage.read("ordenesCarrito");
                }
              },
              letIndexChange: (index) => true,
            ),
      body: Obx(() => IndexedStack(
            index: controller.selectedIndex.value,
            children: [
              CarritoPage(),
              // InicioPage(),
              ClientePage(),
              HistorialPage(),
            ],
          )),
    );
  }
}
