import 'package:Novedades/modules/Navegacion_tabs/carrito/carrito_controller.dart';
import 'package:Novedades/modules/Navegacion_tabs/cliente/cliente_controller.dart';
import 'package:Novedades/modules/Navegacion_tabs/historial/historial_controller.dart';
import 'package:Novedades/modules/Navegacion_tabs/inicio/inicio_controller.dart';
import 'package:get/get.dart';

import 'tabs_controller.dart';

class TabsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TabsController>(() => TabsController());
    Get.lazyPut<CarritoController>(() => CarritoController());
    Get.lazyPut<HistorialController>(() => HistorialController());
    Get.lazyPut<ClienteController>(() => ClienteController());
    // Get.lazyPut(() => PromocionController());
  }
}
