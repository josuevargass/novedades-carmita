import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';
import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';

class ClienteController extends GetxController {
  final picker = ImagePicker();
  var pickedFile;
  final storage = GetStorage();
  var opcionPais = 1.obs;
  var opcionProvincia = 9.obs;
  var opcionCiudad = 75.obs;
  var opcionTipoIdentificacion = "C".obs;
  var opcionSector = 1.obs;
  Uint8List imagen;
  File file;
  RxList<Pais> paises = <Pais>[].obs;
  RxList<Provincia> provincias = <Provincia>[].obs;
  RxList<Ciudad> ciudades = <Ciudad>[].obs;
  RxList<Sector> sectores = <Sector>[].obs;
  RxList<dynamic> tiposIdentificacion = <dynamic>[
    {"key": "C", "valor": "Cédula"},
    {"key": "R", "valor": "Ruc"}
  ].obs;

  TextEditingController razonsocialController = new TextEditingController();
  TextEditingController correoController = new TextEditingController();
  TextEditingController direccionController = new TextEditingController();
  TextEditingController telefonoController = new TextEditingController();
  TextEditingController celularController = new TextEditingController();
  TextEditingController identificacionController = new TextEditingController();
  TextEditingController fechaNacimientoController = new TextEditingController();

  final DateFormat formatter = DateFormat('yyyy-MM-dd');
  DateTime diaActual = DateTime.now();
  DateTime diaSeleccionado = DateTime(2021);

  String imagen64 = "";

  @override
  void onInit() {
    super.onInit();
    cargarPaises();
    cargarProvincias();
    cargarCiudades();
    cargarSectores();
  }

  cargarPaises() async {
    paises.value = await API.instance.getPaises();
  }

  cargarProvincias() async {
    provincias.value =
        await API.instance.getProvinciasByIdPais(opcionPais.value);
  }

  cargarCiudades() async {
    ciudades.value =
        await API.instance.getCiudadByIdProvincia(opcionProvincia.value);
  }

  cargarSectores() async {
    sectores.value = await API.instance.getSectores();
  }

  alertaCerrarSesion() {
    return Get.defaultDialog(
      backgroundColor: Colors.black,
      // title: "",
      titleStyle: TextStyle(color: Colors.white),
      content: Column(
        children: [
          BounceInDown(
              child: Image.asset(
            'assets/img/logo.png',
            height: 120,
          )),
          Text(
            "¿Estás seguro de cerrar sesión?",
            style: TextStyle(color: Colors.white),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: Text(
                    "No",
                    style: TextStyle(color: colorSecundario),
                  )),
              FlatButton(
                  onPressed: () {
                    storage.remove("token");
                    Get.offAllNamed(AppRoutes.LOGIN);
                  },
                  child: Text(
                    "Si",
                    style: TextStyle(color: colorSecundario),
                  )),
            ],
          )
        ],
      ),
    );
  }

  tomarFoto() async {
    var status = await Permission.camera.status;
    print(status);
    if (status.isDenied) {
      print("if");
      Permission.camera.request();
      // We didn't ask for permission yet or the permission has been denied before but not permanently.
    }
    print("entro $status");
    if (status.isGranted) {
      pickedFile =
          await picker.getImage(source: ImageSource.camera, imageQuality: 10);
      print("pick: $pickedFile");
      if (pickedFile != null) {
        file = File(pickedFile.path);
        imagen = file.readAsBytesSync();
        imagen64 = base64Encode(imagen);
        mensajeSastifactorio("Se ha cargado la imagen");
      } else {
        mensajeAlerta("No has tomado foto");
      }
    }
  }

  tomarUbicacion() async {
    Get.toNamed(AppRoutes.MAPA);
  }

  crearCliente() async {
    var idLocal = storage.read("local");
    var latitud = storage.read("latitud");
    var longitud = storage.read("longitud");
    print("LATITUD: $latitud, LONGITUD: $longitud");
    print(razonsocialController.text);
    var body = json.encode({
      // "Pais": "string",
      "IdEmpresa": idLocal,
      "TipoEntidad": "string",
      "RazonSocial": razonsocialController.text,
      "TipoIdentificacion": opcionTipoIdentificacion.value,
      "Identificacion": identificacionController.text,
      "Telefono": telefonoController.text,
      "Direccion": direccionController.text,
      "Celular": celularController.text,
      "Correo": correoController.text,
      "IdPais": opcionPais.value,
      "IdProvincia": opcionProvincia.value,
      "IdCiudad": opcionCiudad.value,
      // "Sector": "",
      "IdSector": opcionSector.value,
      "Observaciones": "string",
      "IpIngreso": "string",
      "UsuarioIngreso": "string",
      "FechaIngreso": diaActual.toIso8601String(),
      "IpModificacion": "string",
      "UsuarioModificacion": "string",
      "FechaModificacion": diaActual.toIso8601String(),
      "IdEstado": 1,
      "EsCliente": true,
      "EsProveedor": true,
      "Latitud": latitud,
      "Longitud": longitud,
      "Foto": imagen64 != null ? imagen64 : ""
    });
    inspect(body);
    if (razonsocialController.text != null &&
        identificacionController.text != null &&
        correoController.text != null &&
        telefonoController.text != null &&
        direccionController.text != null &&
        celularController.text != null) {
      await API.instance.postCrearCliente(body).then((value) {
        print(value["ErrorCode"]);
        if (value["ErrorCode"] == "404") {
          mensajeAlerta(value["UserMessage"]);
        } else {
          razonsocialController.text = "";
          identificacionController.text = "";
          telefonoController.text = "";
          direccionController.text = "";
          celularController.text = "";
          correoController.text = "";
          mensajeSastifactorio("El cliente se ha creado");
        }
      });
    } else {
      mensajeAlerta("Debes completar los campos");
    }
  }
}
