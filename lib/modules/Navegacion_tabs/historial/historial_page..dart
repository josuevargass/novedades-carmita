import 'package:Novedades/global_widgets/search/search_clientes.dart';
import 'package:Novedades/global_widgets/search/search_historial.dart';
import 'package:Novedades/modules/Navegacion_tabs/historial/historial_controller.dart';
import 'package:Novedades/utils.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class HistorialPage extends GetWidget<HistorialController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("#B76AC8"),
        title: Text(
          "Novedades",
          textAlign: TextAlign.start,
          style: TextStyle(fontSize: Get.width * 0.06),
        ),
        actions: [
          IconButton(
              icon: Icon(Icons.logout),
              iconSize: Get.width * 0.07,
              onPressed: () {
                controller.alertaCerrarSesion();
              }),
          Text("")
        ],
      ),
      body: ListView(
        children: [buscar(), fechas(), botonFiltrar()],
      ),
    );
  }

  Widget buscarCliente() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        new Flexible(
          child: Padding(
              padding: const EdgeInsets.all(10.0), child: tituloBuscar()),
        ),
        new Flexible(
          child: Padding(padding: const EdgeInsets.all(10.0), child: buscar()),
        ),
      ],
    );
  }

  Widget tituloBuscar() {
    return Text(
      "Cliente: ",
      style: TextStyle(fontWeight: FontWeight.bold),
    );
  }

  Widget buscar() {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: TextField(
          controller: controller.razonSocialController,
          onTap: () async {
            final data = await showSearch(
                context: Get.context, delegate: ClienteSearch());
            print(data);
            if (data != null) {
              controller.idCliente.value = data;
              controller.cargarClienteById();
            } else {
              controller.idCliente.value = 0;
            }
          },
          readOnly: true,
          decoration: InputDecoration(
              hintText: "Buscar cliente",
              prefixIcon: Icon(Icons.search, color: colorSecundario),
              border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.blueAccent, width: 10.0),
                  borderRadius: BorderRadius.circular(25.0)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: 10.0),
                  borderRadius: BorderRadius.circular(25.0)))),
    );
  }

  Widget fechas() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        new Flexible(
          child: Padding(
              padding: const EdgeInsets.all(20.0), child: fechaInicio()),
        ),
        new Flexible(
          child:
              Padding(padding: const EdgeInsets.all(20.0), child: fechaFin()),
        ),
      ],
    );
  }

  fechaInicio() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: CupertinoTextField(
        readOnly: true,
        placeholder: "Fecha Inicio",
        controller: controller.fechaInicio,
        onTap: () {
          FocusScope.of(Get.context).requestFocus(new FocusNode());
          _seleccionarFechaInicio();
        },
      ),
    );
  }

  _seleccionarFechaInicio() async {
    DateTime picked = await showDatePicker(
        context: Get.context,
        locale: const Locale('es', 'ES'),
        initialDate: new DateTime(2021, 5),
        firstDate: new DateTime(2021),
        lastDate: new DateTime(2030));

    if (picked != null) {
      controller.selectedDate = picked;
      final String formatted = controller.formatter.format(picked);
      controller.fechaInicio.text = formatted;
    }
  }

  fechaFin() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: CupertinoTextField(
        readOnly: true,
        placeholder: "Fecha Fin",
        controller: controller.fechaFin,
        onTap: () {
          FocusScope.of(Get.context).requestFocus(new FocusNode());
          _seleccionarFechaFin();
        },
      ),
    );
  }

  _seleccionarFechaFin() async {
    DateTime picked = await showDatePicker(
        context: Get.context,
        locale: const Locale('es', 'ES'),
        initialDate: new DateTime(2021, 5),
        firstDate: new DateTime(2021),
        lastDate: new DateTime(2025));

    if (picked != null) {
      controller.selectedDate = picked;
      final String formatted = controller.formatter.format(picked);
      controller.fechaFin.text = formatted;
    }
  }

  Widget botonFiltrar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: MaterialButton(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
            child: Text('Filtrar'),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          elevation: 0.0,
          color: colorPrincipal,
          textColor: Colors.white,
          onPressed: () {
            controller.bandera = true;
            controller.filtrar();
          }),
    );
  }

  Widget modal() {
    showCupertinoModalBottomSheet(
      context: Get.context,
      builder: (context) => Container(),
    );
  }
}
