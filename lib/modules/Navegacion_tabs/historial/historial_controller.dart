import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/global_widgets/lista_locales.dart';
import 'package:Novedades/global_widgets/search/search_historial.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class HistorialController extends GetxController {
  var storage = GetStorage();
  var bandera = false;
  var estados = [
    "enProceso",
    "Aprobada",
    "Revisada",
    "Empaquetada",
    "Entregada",
    "Anulada",
    "Facturada"
  ];
  var tiposTransaccion = [
    "NotaPedido",
    "Compra",
    "SalidaBodega",
    "EntradaBodega",
    "Merma"
  ];
  RxList<dynamic> listadoHistorial = <dynamic>[].obs;

  var idCliente = 0.obs;

  RxList<dynamic> historial = <dynamic>[].obs;

  DateFormat fecha = DateFormat("dd-MM-yyyy");

  DateTime selectedDate = DateTime(2021);
  final DateTime diaActual = DateTime.now();

  final DateFormat formatter = DateFormat('yyyy-MM-dd');

  TextEditingController fechaInicio = new TextEditingController();
  TextEditingController fechaFin = new TextEditingController();

  TextEditingController razonSocialController = new TextEditingController();

  @override
  void onInit() {
    super.onInit();
  }

  cargarClienteById() async {
    if (idCliente.value != 0) {
      await API.instance.getCLientesById(idCliente.value).then((value) {
        print(value);
        razonSocialController.text = value["RazonSocial"];
        print(razonSocialController.text);
      });
    }
  }

  filtrar() async {
    var idLocal = storage.read("local");
    if (idLocal != null || idLocal != "") {
      print(
          "${idLocal}, ${idCliente.value}, ${fechaInicio.text}, ${fechaFin.text}");

      listadoHistorial.value = await API.instance.getHistorial(
          idLocal, idCliente.value, fechaInicio.text, fechaFin.text);
      showSearch(
          context: Get.context, delegate: HistorialSearch(listadoHistorial));
    } else {
      fallaOEliminacionMensajeSnackBar(
          "Error", "Por favor seleccionar un local");
      Get.offAllNamed(AppRoutes.LOCAL);
    }
  }

  alertaCerrarSesion() {
    return Get.defaultDialog(
      backgroundColor: Colors.black,
      // title: "",
      titleStyle: TextStyle(color: Colors.white),
      content: Column(
        children: [
          BounceInDown(
              child: Image.asset(
            'assets/img/logo.png',
            height: 120,
          )),
          Text(
            "¿Estás seguro de cerrar sesión?",
            style: TextStyle(color: Colors.white),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton(
                  onPressed: () {
                    Get.back();
                  },
                  child: Text(
                    "No",
                    style: TextStyle(color: colorSecundario),
                  )),
              FlatButton(
                  onPressed: () {
                    storage.remove("token");
                    Get.offAllNamed(AppRoutes.LOGIN);
                  },
                  child: Text(
                    "Si",
                    style: TextStyle(color: colorSecundario),
                  )),
            ],
          )
        ],
      ),
    );
  }
}
