import 'package:flutter/material.dart';
import 'package:get/instance_manager.dart';
import 'package:get/state_manager.dart';
import 'historial_controller.dart';

class HistorialBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HistorialController());
  }
}
