import 'dart:async';
import 'package:Novedades/data/models/model.dart';
import 'package:Novedades/data/services/api.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:get_storage/get_storage.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MapaController extends GetxController {
  RxList<Marker> markers = List<Marker>().obs;
  var storage = GetStorage();
  MarkerId markerId = MarkerId("1");
  Location location = new Location();
  LocationData locationData;
  PermissionStatus _permissionGranted;
  @override
  void onInit() {
    super.onInit();
    permisos();
  }

  permisos() async {
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    locationData = await location.getLocation();
    actualizarPosicion();
  }

  actualizarPosicion() {
    markers.add(Marker(
        draggable: true,
        onDragEnd: (LatLng position) {
          print('Drag Ended $position');
          storage.write("latitud", position.latitude);
          storage.write("longitud", position.longitude);
        },
        markerId: markerId,
        position: LatLng(locationData.latitude, locationData.longitude)));
  }
}
