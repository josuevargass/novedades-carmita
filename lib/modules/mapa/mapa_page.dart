import 'dart:convert';

import 'package:Novedades/modules/mapa/mapa_controller.dart';
import 'package:Novedades/utils/uber_map_theme.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapaPage extends GetWidget<MapaController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: colorPrincipal,
        title: Text("Selecciona la ubicación"),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: controller.permisos(),
        builder: (_, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child:
                  CircularProgressIndicator(backgroundColor: colorSecundario),
            );
          } else {
            return mapa();
          }
        },
      ),
    );
  }

  Widget mapa() {
    return Obx(() => GoogleMap(
          myLocationButtonEnabled: false,
          zoomControlsEnabled: false,
          mapType: MapType.normal,
          zoomGesturesEnabled: true,
          onMapCreated: (GoogleMapController controller) {
            controller.setMapStyle(jsonEncode(uberMapTheme));
          },
          markers: Set<Marker>.of(controller.markers),
          initialCameraPosition: CameraPosition(
              target: LatLng(controller.locationData.latitude,
                  controller.locationData.longitude),
              zoom: 17),
          // onCameraMove: _.mover_camara,
        ));
  }
}
