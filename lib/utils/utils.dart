import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';

final colorPrincipal = HexColor("#B76AC8");
final colorSecundario = HexColor("#C880D2");

final titulosNegrita60 =
    TextStyle(fontWeight: FontWeight.bold, fontSize: Get.width * 0.06);

final titulosNegrita50 =
    TextStyle(fontWeight: FontWeight.bold, fontSize: Get.width * 0.05);

final titulosNegrita40 =
    TextStyle(fontWeight: FontWeight.bold, fontSize: Get.width * 0.04);

final titulos60 = TextStyle(fontSize: Get.width * 0.06);

final titulos50 = TextStyle(fontSize: Get.width * 0.05);

final subTitulos45 = TextStyle(fontSize: Get.width * 0.04);
