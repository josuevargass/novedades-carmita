import 'dart:convert';

import 'package:Novedades/global_widgets/global_controller.dart';
import 'package:Novedades/routes/app_routes.dart';
import 'package:Novedades/utils/uber_map_theme.dart';
import 'package:Novedades/utils/utils.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:intl/intl.dart';

final colorAmarillo = HexColor("#FCB338");
final colorNeutralNegro = HexColor("#231F20");

final kHintTextStyle = TextStyle(
  color: HexColor("FFCB00"),
  fontFamily: 'OpenSans',
);

final kLabelStyle = TextStyle(
  color: HexColor("FFCB00"),
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

final kBoxDecorationStyle = BoxDecoration(
  color: Colors.white12,
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.white30,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

final kBoxDecorationStyle2 = BoxDecoration(
  color: Colors.white,
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);

alerta(titulo, icono) {
  return Get.defaultDialog(
    backgroundColor: colorNeutralNegro,
    // title: "",
    titleStyle: TextStyle(color: Colors.white),
    content: Column(
      children: [
        Image.asset(
          'assets/images/logo-vertical.png',
          height: 120,
        ),
        Text(
          "¿Estás seguro de cerrar sesión?",
          style: TextStyle(color: Colors.white),
        ),
        SizedBox(
          height: 15,
        ),
        Center(
            child: Icon(
          icono,
          color: colorAmarillo,
          size: 30,
        ))
      ],
    ),
  );
}

alertaCerrarSesion() {
  return Get.defaultDialog(
    backgroundColor: colorNeutralNegro,
    // title: "",
    titleStyle: TextStyle(color: Colors.white),
    content: Column(
      children: [
        Image.asset(
          'assets/images/logo-vertical.png',
          height: 120,
        ),
        Text(
          "¿Estás seguro de cerrar sesión?",
          style: TextStyle(color: Colors.white),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FlatButton(
                onPressed: () {
                  // cerrarSesion();
                },
                child: Text(
                  "Si",
                  style: TextStyle(color: colorAmarillo),
                )),
            FlatButton(
                onPressed: () {
                  Get.back();
                },
                child: Text(
                  "No",
                  style: TextStyle(color: colorAmarillo),
                )),
          ],
        )
      ],
    ),
  );
}

alertaCliente(idCliente, razonSocial, identificacion, correo,
    [posicion, imagen]) {
  print("POSICION: $posicion");
  return Get.defaultDialog(
    backgroundColor: colorSecundario,
    title: "¿Qué deseas hacer?",
    titleStyle: TextStyle(color: Colors.white),
    confirm: FlatButton.icon(
        onPressed: () {
          Get.toNamed(AppRoutes.EDITARCLIENTE, arguments: idCliente);
        },
        icon: Icon(FontAwesomeIcons.pencilAlt, color: Colors.white),
        label: Text(
          "Editar",
          style: TextStyle(color: Colors.white),
        )),
    cancel: posicion != null
        ? FlatButton.icon(
            onPressed: () {
              Get.back();
              detalle(razonSocial, identificacion, correo, posicion, imagen);
            },
            icon: Icon(
              FontAwesomeIcons.eye,
              color: Colors.white,
            ),
            label: Text(
              "Ver detalle",
              style: TextStyle(color: Colors.white),
            ))
        : FlatButton.icon(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.cancel_outlined,
              color: Colors.white,
            ),
            label: Text(
              "Cancelar",
              style: TextStyle(color: Colors.white),
            )),
    content: Column(
      children: [
        // Image.asset(
        //   'assets/img/logo.png',
        //   height: 120,
        // ),
      ],
    ),
  );
}

detalle(nombres, cedula, correo, posicion, imagen) {
  Get.bottomSheet(
    Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.white,
      ),
      width: Get.width * 0.80,
      height: Get.height * 0.70,
      child: ListView(
        shrinkWrap: true,
        children: [
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text(
              "Detalle del cliente",
              textAlign: TextAlign.center,
              style:
                  TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Nombres: ", style: TextStyle(fontWeight: FontWeight.bold)),
              Text(nombres)
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Cédula: ", style: TextStyle(fontWeight: FontWeight.bold)),
              Text(cedula)
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Correo: ", style: TextStyle(fontWeight: FontWeight.bold)),
              Text(correo)
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Container(
              child: mapa(posicion),
              height: 180,
              width: Get.width * 0.80,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20))),
            ),
          ),
          imagen != null || imagen != ""
              ? Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: FadeInImage.assetNetwork(
                      height: Get.height * 0.60,
                      width: Get.width * 0.80,
                      placeholder: 'assets/img/loading.gif',
                      image: imagen),
                )
              : Container(),
        ],
      ),
    ),
    ignoreSafeArea: true,
  );
}

Widget mapa(posicion) {
  List<Marker> marcador = [];

  Marker marker = Marker(
    markerId: MarkerId("ID"),
    position: posicion,
  );
  print(posicion);
  marcador.add(marker);
  return GoogleMap(
    // myLocationEnabled: true,
    myLocationButtonEnabled: false,
    zoomControlsEnabled: false,
    mapType: MapType.normal,
    zoomGesturesEnabled: true,
    onMapCreated: (GoogleMapController controller) {
      controller.setMapStyle(jsonEncode(uberMapTheme));
    },
    markers: Set.from(marcador),

    initialCameraPosition: CameraPosition(target: posicion, zoom: 17),
    // onCameraMove: _.mover_camara,
  );
}

// MENSAJE
exitoMensajeSnackBar(titulo, descripcion) {
  return Get.snackbar(titulo, descripcion,
      backgroundColor: Colors.green,
      colorText: Colors.white,
      snackPosition: SnackPosition.BOTTOM);
}

fallaOEliminacionMensajeSnackBar(titulo, descripcion) {
  return Get.snackbar(titulo, descripcion,
      backgroundColor: Colors.red,
      colorText: Colors.white,
      snackPosition: SnackPosition.BOTTOM);
}

enConstruccion(titulo, descripcion) {
  return Get.snackbar(titulo, descripcion,
      backgroundColor: Colors.blue,
      colorText: Colors.white,
      snackPosition: SnackPosition.BOTTOM);
}

String expresionRegular(double numero) {
  NumberFormat f = new NumberFormat("#,###.00#", "es_US");
  String result = f.format(numero);
  return result;
}

mensajeSastifactorio(mensaje) {
  var show = CoolAlert.show(
    title: "Éxito",
    context: Get.context,
    type: CoolAlertType.success,
    text: mensaje,
    onConfirmBtnTap: () {
      Get.back();
    },
  );
}

mensajeError(mensaje) {
  var show = CoolAlert.show(
    title: "Error",
    context: Get.context,
    type: CoolAlertType.error,
    text: mensaje,
    // autoCloseDuration: Duration(seconds: 4),
  );
}

mensajeAlerta(mensaje) {
  var show = CoolAlert.show(
    title: "Alerta",
    context: Get.context,
    type: CoolAlertType.info,
    text: mensaje,
    onConfirmBtnTap: () {
      Get.back();
    },
  );
}
